# SMT Server
 * Application SMT ( Server Monitoring Toolkit ) Server  
 * Monitoring Device Of Server Like , HDD, Memory, Network, Load Average , Service , CPU Info . 
 * With Notice Integrate with cahnnel ready use , Instagram, Email , etc.  
 * Author - Kawani Development Team
 * (C) 2016 - 2021 (PDS) Peranti Digital Solusindo,PT.
 *
 
# SMT Installation Requirmment 
 * OS Unix Centos 5.x More Then 
 * PHP Version 5.1 More Then   
 * PHP Process Supported posix 
 * Strace for debug trace console 
 
# Running script local 
 * php -q /opt/kawani/bin/simon/smt.c --cfg=/opt/kawani/bin/simon/config/smt.conf --pid=/var/run/smt/smt.pid --vvv=1

# Path PID location service for SMT 
 * /var/run/smt/${pid}

# Path daemon started binary launch on  
 * /usr/sbin/simon
 
# Path service config start up / down  
 * /etc/init.d/simon
 
# Run config Install 
 * ${PATH}/kernel/smt_app_config
 
# for interactive user comand & devel 

* simon -devel launch smt simon on foreground , Note simon Process must on stop service
* simon -rx "comand" for user utilize process 




 