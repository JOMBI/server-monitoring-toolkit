<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 if (!defined('MAX_SIZE_MEM')) define( 'MAX_SIZE_MEM', 85); 
 
/*! \retval smtAppAlertMemory */ 
 function smtAppAlertMemory($smt= NULL)
{
	/*! \retval Write PID Process from this section */	
	if ($pid = getmypid())
	{
		smt_log_info(sprintf("Start Alerts Thread.AlertMemory(%d)", $pid), 
		__FILE__, __LINE__);
	}
	/*! \remark Create Allocation(s)  */
	app_func_struct($out);
	
	
	/*! \remark add collapse */
	/*! if warning is true */
	app_func_copy($out->mem_max_size, MAX_SIZE_MEM);
	app_func_copy($out->vm_hardisk, NULL);
	app_func_copy($out->warning, 0);  
	
	/*! \remark add New Wait Allocation */
	app_func_alloc($out->waits);
	while(1)
	{
		/*! \remark if Not found Kill Process */
		usleep(2 * 1000000); 
		
		app_func_copy($out->buf, NULL);
		if (!app_fork_pid($smt->AppProgPid))
		{
			app_fork_kill($pid, SIGKILL);
		}
		
		$out->storage = $smt->CfgPathStg ."/storage.json";
		if( false === ( $out->stg = @file_get_contents($out->storage) ))
		{
			app_func_free($out->buf);
			continue;
		}
		
		/*! \retval false process read file */
		if (false === ($out->stg = json_decode($out->stg, true)))
		{
			app_func_free($out->buf);
			continue;
		}
		
		/*! \remark for Header Process */
		app_func_alloc($out->buf);
		app_func_copy($out->buf, 'event', 'Memory');
		app_func_copy($out->buf, 'message', '');
		
		
		/*! \retval Create 'Header' Of Message for Service */
		
		app_func_free($s);
		if (app_func_typdef($out->stg, $s))
		{
			/*! convert Of Time */
			app_func_copy($out->buf,'vmid',$s->smt_vm_id);
			if ($s->smt_vm_update){
				app_func_copy($s->smt_vm_update, 
					date('d M Y H:i:s', $s->smt_vm_update)
				);
			}
		
			/*! next sub process */
			app_func_free($out->message);
			app_func_output($out->message, "\n");
			app_func_output($out->message, sprintf("SMT %s\n\n"   , 'Alert for:'));
			app_func_output($out->message, sprintf("Type   : %s\n", 'Memory'));
			app_func_output($out->message, sprintf("Date   : %s\n", $s->smt_vm_update));
			app_func_output($out->message, sprintf("ID     : %s\n", $s->smt_vm_id));
			app_func_output($out->message, sprintf("Name   : %s\n", $s->smt_vm_name));
			app_func_output($out->message, sprintf("Domain : %s\n", $s->smt_vm_hname));	
			app_func_output($out->message, sprintf("Host   : %s\n", $s->smt_vm_haddr));
			app_func_output($out->message, sprintf("Kernel : %s\n", $s->smt_vm_kernel)); 
		
			/*! app_func_free(*) */
			app_func_free($s);
		}
		
		
		/*! Next sub Process */
		app_func_copy($out->sent, 0);
		app_func_output($out->body, NULL);
		app_func_output($out->body, "\n");
		
		/*! \remark  for Load Average */
		app_func_free($out->vm_memory);
		@app_func_copy($out->vm_memory, 
			$out->stg['vm_data']['smt_vm_memory']
		);
		
		if (is_array($out->vm_memory)) 
		foreach ($out->vm_memory as $dev => $r)
		{
			app_func_free($s);
			if (app_func_typdef($r, $s)) 
			{	
				if ($s->percent > $out->mem_max_size)
				{
					app_func_output( $out->body, 
						sprintf("Memory <%s> Usage More than %s %% \n", $dev, $s->percent)
					); 
					app_func_next($out->sent);
				}				
			}	
			app_func_free($r);
		} 
		 
		/*! \retval check Arguments */
		if ($out->sent && (app_func_output($out->message, $out->body)))
		{
			app_func_output($out->message, "\n");
			app_func_free($out->body);
			
		}
		
		/*! sent message to channel */
		if ( app_func_copy($out->buf, 'message', $out->message) )
		{
			app_func_free($out->message);
		}
		
		/*! \remark smtAppChanMemory */
		if (!$out->sent) 
		   app_func_copy($out->warning, 0);	
		else if ($out->sent)
		{
			if ( !app_func_event( 'smtAppChanMemory', array(&$smt, &$out->buf, &$out), __FILE__, __LINE__ ))
			{
				app_func_free($out->buf);
			} 
		}
		
		app_func_free($out->buf);
		app_func_free($out->body);
	}
	 
 }
 
/*! \retval smtAppChanMemory */ 
  function smtAppChanMemory(&$smt= NULL, &$buf = NULL, &$out = NULL)
{
	if (!function_exists('smtAppEventAlerting'))
	{
		return 0;
	}
	
	app_func_free($configHandler);
	if (!app_cfg_stream('AppAlerting', 'memory', $configHandler))
	{
		smt_log_app(sprintf("AppAlerting() Fail.", $pid));
		app_func_free($buf);
		return 0;
	}
	
	foreach ($configHandler as $event => $eventHandler)
	{
		/*! \remark <Parsing Event Handler> */
		app_func_free($Handler);
		if (app_cfg_alerting($eventHandler, $Handler))
		{
			/*! \remark dump channel events */
			if (!isset($out->waits[$event])) 
			{
				$out->waits[$event]['freq'] = $Handler[0];
				$out->waits[$event]['wait'] = time();
			}
			
			/*! \remark <must be implmented> */
			if ((time() - $out->waits[$event]['wait']) >= $out->waits[$event]['freq'])
			{
				$out->waits[$event]['wait'] = time();
				
				if (!$out->warning)
				{
					app_func_next($out->warning);
					smtAppCreateThread($smt, "smtAppEventAlerting", 
						array(&$smt, $buf, array($Handler[1] => $Handler[2]) )
					);
				}
			} 
		}
	} 
	return 0;	
 } 