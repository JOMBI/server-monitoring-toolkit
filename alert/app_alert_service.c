<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 function smtAppAlertService($smt= NULL)
{
	/*! \retval Write PID Process from this section */	
	if ($pid = getmypid())
	{
		smt_log_info(sprintf("Start Alerts Thread.AlertService(%d)", $pid), 
		__FILE__, __LINE__);
	}
	
	/*! \remark Create Allocation(s)  */
	app_func_struct($out);
	app_func_copy($out->vm_service, 0);  /*! if service is true */
	
	
	/*! \remark add collapse */
	app_func_copy($out->warning, 0);  /*! if warning is true */
	
	/*! \remark add New Wait Allocation */
	app_func_alloc($out->waits); 
	while ($smt)
	{
		/*! \remark if Not found Kill Process */
		usleep(5 * 1000000); 
		
		app_func_copy($out->buf, NULL);
		if (!app_fork_pid($smt->AppProgPid))
		{
			app_fork_kill($pid, SIGKILL);
		}
		
		$out->storage = $smt->CfgPathStg ."/storage.json";
		if( false === ( $out->stg = @file_get_contents($out->storage) ))
		{
			app_func_free($out->buf);
			continue;
		}
		
		/*! \retval false process read file */
		if (false === ($out->stg = json_decode($out->stg, true)))
		{
			app_func_free($out->buf);
			continue;
		}
		
		/*! \remark for Header Process */
		app_func_alloc($out->buf);
		app_func_copy($out->buf, 'event', 'Service');
		app_func_copy($out->buf, 'message', '');
		
		
		/*! \retval Create 'Header' Of Message for Service */
		
		app_func_free($s);
		if (app_func_typdef($out->stg, $s))
		{
			/*! convert Of Time */
			app_func_copy($out->buf,'vmid',$s->smt_vm_id);
			if ($s->smt_vm_update){
				app_func_copy($s->smt_vm_update, 
					date('d M Y H:i:s', $s->smt_vm_update)
				);
			}
		
			/*! next sub process */
			app_func_free($out->message);
			app_func_output($out->message, "\n");
			app_func_output($out->message, sprintf("SMT %s\n\n"   , 'Alert for:'));
			app_func_output($out->message, sprintf("Type   : %s\n", 'Service'));
			app_func_output($out->message, sprintf("Date   : %s\n", $s->smt_vm_update));
			app_func_output($out->message, sprintf("ID     : %s\n", $s->smt_vm_id));
			app_func_output($out->message, sprintf("Name   : %s\n", $s->smt_vm_name));
			app_func_output($out->message, sprintf("Domain : %s\n", $s->smt_vm_hname));	
			app_func_output($out->message, sprintf("Host   : %s\n", $s->smt_vm_haddr));
			app_func_output($out->message, sprintf("Kernel : %s\n", $s->smt_vm_kernel)); 
			
			/*! app_func_free(*) */
			app_func_free($s);
		}
		
		/*! \retval Next sub Process */
		app_func_copy($out->sent, 0);
		
		
		/*! \retval created Body Mail */
		app_func_output($out->body, NULL);
		app_func_output($out->body, "\n");
		
		/*! \remark  for Load Average */
		app_func_free($out->vm_service);
		@app_func_copy($out->vm_service, 
			$out->stg['vm_data']['smt_vm_service']
		);
		
		
		/*! \retval default warning is  == zero */ 
		if (is_array($out->vm_service)) foreach ($out->vm_service as $name => $r)
		{
			app_func_free($s);
			if (app_func_typdef($r, $s)) 
			{
				if (!$s->state ) 
				{
					app_func_output($out->body, sprintf("Service <%s> Status Down\n", $s->name)); 
					app_func_next($out->sent);
				}
			}
		} 
		 
		/*! \retval check Arguments */
		if ($out->sent && (app_func_output($out->message, $out->body)))
		{
			app_func_output($out->message, "\n");
			app_func_free($out->body);
			
		}
		
		/*! sent message to channel */
		if ( app_func_copy($out->buf, 'message', $out->message) )
		{
			app_func_free($out->message);
		}
		
		/*! \remark if No have Problem reset Warning to zero field  == false */
		/*! \remark smtAppChanService */
		if (!$out->sent) 
		   app_func_copy($out->warning, 0);	
		else if( $out->sent )
		{
			if ( !app_func_event( 'smtAppChanService', array(&$smt, &$out->buf, &$out), __FILE__, __LINE__ ))
			{
				app_func_free($out->buf);
			} 
		}
		
		app_func_free($out->buf);
		app_func_free($out->body);
	}
	
	return 0;
 }
 
 
 /*! \retval smtAppChanService */ 
  function smtAppChanService(&$smt= NULL, &$buf = NULL, &$out = NULL)
{
	if (!function_exists('smtAppEventAlerting'))
	{
		return 0;
	}
	
	app_func_free($configHandler);
	if (!app_cfg_stream('AppAlerting', 'service', $configHandler))
	{
		smt_log_app(sprintf("AppAlerting() Fail.", $pid));
		app_func_free($buf);
		return 0;
	}
	
	foreach ($configHandler as $event => $eventHandler)
	{
		/*! \remark <Parsing Event Handler> */
		app_func_free($Handler);
		if (app_cfg_alerting($eventHandler, $Handler))
		{
			/*! \remark dump channel events */
			if (!isset($out->waits[$event])) 
			{
				$out->waits[$event]['freq'] = $Handler[0];
				$out->waits[$event]['wait'] = time();
			}
			
			/*! \remark <must be implmented> */
			if ((time() - $out->waits[$event]['wait']) >= $out->waits[$event]['freq'])
			{
				$out->waits[$event]['wait'] = time(); 
				if (!$out->warning)
				{ 
					app_func_next($out->warning); /*! \retval add +n */
					smtAppCreateThread($smt, "smtAppEventAlerting", 
						array(&$smt, $buf, array($Handler[1] => $Handler[2]) )
					);
				}
			} 
		}
	} 
	return 0;	
 }
 