#!/usr/bin/php
<?php if(!defined('SMT_APP')) define('SMT_APP', rtrim(rtrim(dirname( __FILE__ ),"console"), "/"));
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT Toolkit >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 // https://lms.onnocenter.or.id/wiki/index.php/Asterisk_Video_Call 
 /*! running
	php -q /opt/kawani/bin/simon/console/cli.c reload
 */
 
 
 /*! create Global Object */
 $smt = new stdClass();
 
 /*! debug Track Error */
 if (function_exists('error_reporting')){
  ini_set("display_errors",1);
  error_reporting(E_ALL);	
}
 
 /*! create signal fork handler */ 
 if (function_exists( 'pcntl_async_signals')) 
	pcntl_async_signals(true);
 else {
	declare(ticks=1);
 }
 
/*! Unlimited process */
 if ( function_exists('set_time_limit') ){
   set_time_limit(0);
   ob_implicit_flush();
   ignore_user_abort(0);
}
 
/*! declare All php Ini Modul */
 if (function_exists('date_default_timezone_set')){
	date_default_timezone_set( 'Asia/Jakarta' );
 }

 
/*! 
 * include Application Header 
 *
 */
 include SMT_APP .'/socket/app_sock_tcp.c';
 include SMT_APP .'/socket/app_sock_udp.c';
 include SMT_APP .'/libraries/app_lib_func.c';
 include SMT_APP .'/libraries/app_lib_config.c';
 
/*!
 * ToolkitMainApplictaion
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function ToolkitMainConsole()
{
	Global $smt, $argc, $argv;  
	
	/*! \retval "smt_app_safe -rx 'core reload' " */ 
	/*! \retval check arguments if false exit(0); */ 
	
	if ( $argc<3 )
	{
		exit("invalid args(0)\n\n");
	}
	
	/*! \retval check arg 'rx' */
	
	if ( $argv[1] != '-rx' )
	{
		exit("invalid args(1)\n\n");
	}
	
	/*! \retval make Object data */	
	
	app_func_struct($s); 
	app_func_struct($s->udp); 
	
	
	/*! \retval open configuration */
	
	app_func_copy($smt->cfg, SMT_APP ."/config/smt.conf");
	if (app_cfg_parser($smt->cfg, $smt->AppProgVar))
	{
		app_func_copy($s->udp, 'host', app_cfg_param("smt.udp", "host"));
		app_func_copy($s->udp, 'port', app_cfg_param("smt.udp", "port"));
		app_func_copy($s->udp, 'sign', app_cfg_param("smt.udp", "host")); 
		app_func_copy($s->udp, 'sock', false);
		app_func_copy($s->udp, 'recv', "");
		
		/*! \remark trap of opt "user"  */
		
		app_func_copy($s->useropt, $argv[2]);
		app_func_copy($s->useropt, app_func_trim(explode( " ", $s->useropt)));
		 
		/*! \retval make connection console to UDP socket */
		
		if (smtSockUdpClientCreate($s->udp->sock) < 0)
		{
			smt_log_app("Create 'smtSockUdpClientCreate()' Fail.");
			if (smtSockUdpClose($s->udp->sock)) 
			{
			    app_func_copy($s->udp->sock,0);
			}
		}
		
		/*! \retval	passing to socket stream */
		
		if (!app_func_sock($s->udp->sock))
		{
			exit("NOK");
		}
		
		/*! \remark user trap 'option = reload.core' */ 
		
		if ( isset($s->useropt[1]) && !strcmp($s->useropt[1], 'core') )
		{
			/*! \retval passing arguments */ 
			
			app_func_stream($s->udp->buf, sprintf("Action:%s", "Reload"));
			app_func_stream($s->udp->buf, sprintf("Data:%s",$s->useropt[1]));
			
			if (app_func_enter($s->udp->buf))
			{
				smtSockUdpWrite($s->udp->sock, $s->udp->buf, $s->udp->host, $s->udp->port);
				if (smtSockUdpRecv($s->udp->sock, $s->udp->recv))
				{
					smtSockUdpClose($s->udp->sock);
					if (app_func_color($s->udp->recv))
					{
						exit( fprintf( STDERR, "\n\t%s", $s->udp->recv));
					}
				} 
			} 
		}
		
		
		/*! \remark user trap 'option = reload.alerting' */ 
		
		if ( isset($s->useropt[1]) && !strcmp($s->useropt[1], 'alerting') )
		{
			/*! \retval passing arguments */ 
			
			app_func_stream($s->udp->buf, sprintf("Action:%s", "Reload"));
			app_func_stream($s->udp->buf, sprintf("Data:%s", $s->useropt[1]));
			
			if (app_func_enter($s->udp->buf))
			{
				smtSockUdpWrite($s->udp->sock, $s->udp->buf, $s->udp->host, $s->udp->port);
				if (smtSockUdpRecv($s->udp->sock, $s->udp->recv))
				{
					smtSockUdpClose($s->udp->sock);
					if (app_func_color($s->udp->recv))
					{
						exit( fprintf( STDERR, "\n\t%s", $s->udp->recv));
					}
				}
			}
			
		}
		
		/*! \remark user trap 'option = reload.worker' */ 
		
		if ( isset($s->useropt[1]) && !strcmp($s->useropt[1], 'worker') )
		{
			
			/*! \retval passing arguments */ 
			app_func_free($s->udp->buf);
			app_func_stream($s->udp->buf, sprintf("Action:%s", "Reload"));
			app_func_stream($s->udp->buf, sprintf("Data:%s",$s->useropt[1]));
			
			if (app_func_enter($s->udp->buf))
			{
				smtSockUdpWrite($s->udp->sock, $s->udp->buf, $s->udp->host, $s->udp->port);
				if (smtSockUdpRecv($s->udp->sock, $s->udp->recv))
				{
					smtSockUdpClose($s->udp->sock);
					if (app_func_color($s->udp->recv))
					{
						exit( fprintf( STDERR, "\n\t%s", $s->udp->recv));
					}
				}
			}
			
			/*! \retval dump response */
			app_func_copy($cli_output, NULL);
			app_func_output($cli_output, "\nReload <Worker> OK\n"); 
			if ( app_func_color($cli_output) ){
				exit($cli_output);	
			}
		} 
		
		/*! \retval if false Arguments */
		else 
		{
			app_func_stream($s->udp->buf, sprintf("Action:%s", "Reload"));
			app_func_stream($s->udp->buf, sprintf("Data:%s",'helper'));
			
			if (app_func_enter($s->udp->buf)) 
			{
				smtSockUdpWrite($s->udp->sock, $s->udp->buf, $s->udp->host, $s->udp->port);
				if (smtSockUdpRecv($s->udp->sock, $s->udp->recv))
				{
					smtSockUdpClose($s->udp->sock);  
					if ( app_func_color($s->udp->recv) )
					{
						exit( fprintf( STDERR, "\r\n". "\t%s", $s->udp->recv) );
					}
				} 
			} 
		}
	}
	
	exit(0);
}
ToolkitMainConsole();


 