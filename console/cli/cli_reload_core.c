<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function cli_reload_core(&$smt = NULL, $s = NULL, $c = NULL)
{
	/*! \retval Get Config Channel for All */ 
	/*! \remark Open All Configuration */
	app_func_copy($s->output, NULL);
	app_func_free($app->cfg);
	if (!app_cfg_parser($smt->AppProgCfg, $app->cfg))
	{
		return 0; 
	}
	
	/*! \remark copy stream to Global ? <SMT> */
	app_func_free($smt->AppProgVar);
	app_func_free($smt->AppService);
	if (!app_func_copy($smt->AppProgVar, $app->cfg))
	{
		return 0;
	}
	
	/*! \retval notice */
	smt_log_info('Reloading Config Vendor', __FILE__, __LINE__ );
	app_func_output($s->output, "\tReloading Config 'Vendor'\n");
	
	
	/*! \retval reload Vendor */
	app_func_copy($smt->VendorName, 0);
	app_func_copy($smt->VendorHost, 0);
	app_func_copy($smt->VendorId  , 0);
	
	if (function_exists('smtAppMainVendor')) {
		if (smtAppMainVendor($smt)) {
			stg_create_stream($smt);
		}
	}
	
	
	/*! \retval loader service */ 
	smt_log_info('Reloading Config Service', __FILE__, __LINE__ );
	app_func_output($s->output, "\tReloading Config 'Service'\n");
	
	app_func_alloc($smt->AppService);
	if (app_func_sizeof($smt->AppService)<1)
	{
		smtAppInitService($smt);
	}
	
	
	/*! \retval reload config channels */
	smt_log_info('Reloading Config Channel', __FILE__, __LINE__ );
	app_func_output($s->output, "\tReloading Config 'Channel'\n");
	
	app_func_free($smt->CfgChannel);
	if (app_cfg_param( 'smt.cfg', 'evt', $smt->CfgChannel)) 
	{
		smtAppInitChannel($smt);
	}
	
	
	/*! \retval reload config Alert Or Notification */
	smt_log_info('Reloading Config Alert', __FILE__, __LINE__ );
	app_func_output($s->output, "\tReloading Config 'Alert'\n");
	
	app_func_free($smt->CfgAlerting);
	if (app_cfg_param( 'smt.cfg', 'alt', $smt->CfgAlerting)) 
	{
		smtAppInitAlerting($smt);
	}
	
	foreach ($smt->AppWorker as $pid => $worker ) 
	{
		if (app_fork_pid($pid)) 
		{
			smt_log_info(sprintf("Reloading Worker '%s'", $worker['event'] ), __FILE__, __LINE__);
			app_func_output($s->output, sprintf("\tReloading Worker '%s'\n", $worker['event']));
			
			app_fork_kill($pid, SIGKILL);
		}
	}
	
	foreach ($smt->AppAlert as $pid => $alert ) 
	{
		if (app_fork_pid($pid)) 
		{
			smt_log_info(sprintf("Reloading Alert '%s'\n", $alert['event']), __FILE__, __LINE__);
			
			app_func_output($s->output, sprintf("\tReloading Alert  '%s'\n", $alert['event']));
			app_fork_kill($pid, SIGKILL);
		}
	}
	
	/*! \retval sent output */
	app_func_output($s->output, "\n");
	if ( app_func_enter($s->output) )
	{
		if (smtSockUdpWrite($c->sock, $s->output, $c->host, $c->port) )
		{
			app_func_free($s->output); 
		}
	}
	
	return $c->sock;
}