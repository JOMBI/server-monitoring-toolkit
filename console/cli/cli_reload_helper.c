<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function cli_reload_helper(&$smt = NULL, $s = NULL, $c = NULL)
{
	/*! \retval sent text to user interface */
	
	app_func_copy($s->output, NULL);
	app_func_output($s->output, sprintf("\t%s Version %s\n", $smt->AppProgTitle, $smt->AppProgVer)); 
	app_func_output($s->output, sprintf("\tCopyright %s.\n", $smt->AppProgCopy)); 
	app_func_output($s->output, "\t\n");
	app_func_output($s->output, sprintf("\t%s < jombi.php@gmail.com >\n", $smt->AppProgAuth)); 
	app_func_output($s->output, sprintf("\tSee < %s > for more information about\n", $smt->AppProgLink));
	app_func_output($s->output, "\tthe SMT project. Please do not directly contact\n");
	app_func_output($s->output, "\tany of the maintainers of this project for assistance\n");
	app_func_output($s->output, "\tthe project provides a web site, mailing lists and IRC\n");
	app_func_output($s->output, "\tchannels for your use.\n");
	app_func_output($s->output, "\t\n");
	app_func_output($s->output, "\tThis program is free software, distributed under the terms of\n");
	app_func_output($s->output, "\tthe GNU General Public License Version 2. See the LICENSE file\n");
	app_func_output($s->output, "\tat the top of the source tree.\n"); 
	app_func_output($s->output, "\r\n"); 
	app_func_output($s->output, "\n\tUsage Command ( -rx \"reload option\" ) \n");
	app_func_output($s->output, "\r\n");
	app_func_output($s->output, "\tcore ....... : Reload Core Process\n");
	app_func_output($s->output, "\tworker ..... : Reload Service Worker\n");
	app_func_output($s->output, "\talerting ... : Reload Alert Or Notification\n"); 
	app_func_output($s->output, "\r\n");
	
	if ( app_func_enter($s->output) )
	{
		if (smtSockUdpWrite($c->sock, $s->output, $c->host, $c->port) )
		{
			app_func_free($s->output); 
		}
	}
	
	return $c->sock;
}