<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function udp_event_cpuinfo(&$smt = NULL, $s = NULL, $c = NULL)
{
	/*! \retval make Alloc */
	app_func_struct($out);
	app_func_copy($out->n, 0);
	app_func_copy($out->r, 0);
	
	/*! \retval If Not Object return 0 */
	if (!is_object($s)){
		return 0;
	}
	
	/*! validation this */
	if( !isset($s->Data->power_management) )
	{
		$s->Data->power_management = NULL;
	}
	
	/*! \retval make Alloc(s) */
	app_func_struct($out->s);
	app_func_copy($out->s,'vendor_id',$s->Data->vendor_id);
	app_func_copy($out->s,'cpu_family',$s->Data->cpu_family);
	app_func_copy($out->s,'model',$s->Data->model);
	app_func_copy($out->s,'model_name',$s->Data->model_name);
	app_func_copy($out->s,'stepping',$s->Data->stepping);
	app_func_copy($out->s,'cpu_mhz',$s->Data->cpu_mhz);
	app_func_copy($out->s,'cache_size',$s->Data->cache_size);
	app_func_copy($out->s,'fpu',$s->Data->fpu);
	app_func_copy($out->s,'fpu_exception',$s->Data->fpu_exception);
	app_func_copy($out->s,'cpuid_level',$s->Data->cpuid_level);
	app_func_copy($out->s,'wp',$s->Data->wp);
	app_func_copy($out->s,'bogomips',$s->Data->bogomips);
	app_func_copy($out->s,'clflush_size',$s->Data->clflush_size);
	app_func_copy($out->s,'cache_alignment',$s->Data->cache_alignment);
	app_func_copy($out->s,'address_sizes',$s->Data->address_sizes);
	app_func_copy($out->s,'power_management',$s->Data->power_management);
	
	if (stg_update_cpuinfo($out->s,$out->r))
	{
		app_func_free($out->r); 
		app_func_next($out->n); 
	}
	
	/*! \retval clean Memory usage */
	app_func_free($out->s);
	return $out->n;	
 } 