<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function udp_event_memory(&$smt = NULL, $s = NULL, $c = NULL)
{
	
	/*! \retval make Alloc */
	app_func_struct($out);
	app_func_copy($out->n, 0);
	app_func_copy($out->r, 0);
	
	
	/*! \retval If Not Object return 0 */ 
	if ( !is_object($s) ) 
		return 0;
	else if ( !isset($s->Data) )
		return 0;
	else 
	{
		 foreach ($s->Data as $h => $r)
		{
			/*! \def heapcheck */
			app_func_copy( $out->h, $h ); 
			
			
			/*! \retval output for <\ ram> */
			if ( !strcmp($out->h,'ram') )
			{
				app_func_struct($out->s);
				app_func_copy($out->s,'total',$r->total);
				app_func_copy($out->s,'t_free',$r->t_free);
				app_func_copy($out->s,'buffers',$r->buffers);
				app_func_copy($out->s,'cached',$r->cached);
				app_func_copy($out->s,'t_used',$r->t_used);
				app_func_copy($out->s,'percent',$r->percent);
				app_func_copy($out->s,'p_free',$r->p_free);
				app_func_copy($out->s,'app', $r->app);
				app_func_copy($out->s,'app_percent',$r->app_percent);
				app_func_copy($out->s,'buffers_percent',$r->buffers_percent);
				app_func_copy($out->s,'cached_percent',$r->cached_percent);
				
				if (stg_update_memory($out->h, $out->s, $out->r))
				{
					app_func_free($out->r); 
					app_func_next($out->n); 
				}
			}
			
			/*! \retval output for <\ swap> */
			
			if ( !strcmp($out->h,'swap') )
			{
				app_func_struct($out->s);
				app_func_copy($out->s,'total',$r->total);
				app_func_copy($out->s,'free',$r->free);
				app_func_copy($out->s,'used',$r->used);
				app_func_copy($out->s,'percent',$r->percent); 
				
				if (stg_update_memory($out->h, $out->s, $out->r))
				{
					app_func_free($out->r); 
					app_func_next($out->n); 
				}
			} 	

			/*! \retval output for <\ devswap> */
			
			if ( !strcmp($out->h,'devswap') )
			{
				app_func_struct($out->s);
				app_func_copy($out->s,'total',0);
				app_func_copy($out->s,'free',0);
				app_func_copy($out->s,'used',0);
				app_func_copy($out->s,'percent',0); 
				
				if (stg_update_memory($out->h, $out->s, $out->r))
				{
					app_func_free($out->r); 
					app_func_next($out->n); 
				}
			}	 
			app_func_free($out->s); 
		}
	}  
	
	return $out->n;	
 } 