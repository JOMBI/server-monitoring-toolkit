<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function udp_event_network(&$smt = NULL, $s = NULL, $c = NULL)
{
	/*! \retval make Alloc */
	app_func_struct($out);
	app_func_copy($out->n, 0); 
	app_func_copy($out->h, 0);
	app_func_copy($out->r, 0);	 
	app_func_copy($out->s, 0);
	 
	
	/*! \retval If Not Object return 0 */ 
	if (!is_object($s)) 
		return 0;
	else if ( !isset($s->Data) )
		return 0;
	else 
	{
		foreach ($s->Data as $h => $r)
	   {	
			app_func_free($out->s);
			if (app_func_copy($out->h, $h))
			{
				app_func_copy($out->s, $r);
				if (stg_update_network($out->h, $out->s, $out->r))
				{
					app_func_free($out->r);
					app_func_next($out->n); 
				}
			}
		}
	}  
	return $out->n;	
 } 