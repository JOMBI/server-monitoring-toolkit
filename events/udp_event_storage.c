<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function udp_event_storage(&$smt = NULL, $s = NULL, $c = NULL)
{
	/*! \retval If Not Object return 0 */
	if (!is_object($s)){
		return 0;
	}  
	
	/*! \retval get Interupted Time */
	if (isset($s->Data->time))
	{
		if ( app_func_copy($smt->AppStorage, 'smt_vm_update', $s->Data->time) )
		{
			app_func_event('smtAppMainStorage', array(&$smt), __FILE__, __LINE__ );  
		}
	}
	
	app_func_free($s);
	return 0;
		 
 } 