<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 
/*!
 *
 *\brief Constructor 
 * 
 *\param array  A reference to an array to use for storage. This will be populated with key/value pairs that store the time/value, respectively.
 * Because it is passed by reference, it can be stored externally in a session or database, allowing persistant use of this object
 * across page loads.
 *
 *\param  int  The maximum age of values to store, in seconds
 *
 */
class SMTAppAverage 
{
  var $_max_age;
  var $_values;

  function __construct(&$storage_array, $max_age) 
  {
	  
	$this->_max_age = $max_age;
    if (!is_array($storage_array)) {
		$storage_array = array();
    }
    $this->_values =& $storage_array;
  }
  /** Adds a value to the array
   * @param  float        The value to add
   * @param  int  The timestamp to use for this value, defaults to now
   */
  function add($value, $timestamp=NULL) 
  {
	if (!$timestamp)
		$timestamp = time();
	  
	$this->_values[$timestamp] = $value;
  }
	/** Calculate the average per second value
   * @return  The average value, as a rate per second
   */
  function average() 
  {
	
	$this->_clean();

    $avgs = array();
    $last_time = false;
    $last_val = false;
	foreach ($this->_values as $time=>$val) 
	{
     
		if (!$last_time){
			$avgs[] = ($val - $last_val) / ($time - $last_time);
		}
	  
      $last_time = $time;
      $last_val = $val;
    }
	
    // return the average of all our averages
	if ($count = count($avgs)) {
			return array_sum($avgs) / $count;
    } else {
      return 'unknown';
    }
  }
  /** Clean old values out of the array
   */
  function _clean() 
  {
    $too_old = time() - $this->_max_age;

    foreach (array_keys($this->_values) as $key) 
	{
			if ($key < $too_old) {
      	unset($this->_values[$key]);
      }
    }
  }
}