<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 
/*! 
 * \brief  app_cfg_parser 
 *
 * \param [0] => /opt/kawani/iot/smt.c
 * \param [1] => --cfg=
 * \param [2] => /opt/kawani/iot/config/smt.conf
 * \param [3] => --pid=
 * \param [4] => /var/run/smt/smt.pid
 *
 * \retval 
 *
 */
 
 function app_cfg_main(&$smt = NULL)
{
	Global $argc, $argv;  
	
	
	/*! \remark if Arguments Not Valid Then 
	Sent Signal Break */ 
	if ( $argc < 3)
	{
		smt_log_error("Invalid Arguments Configuration.", __FILE__, __LINE__ );
		exit(0);
	}
	
	/*! \remark Create Allocation 
	Size Memory */
	
	app_func_struct($out);
	app_func_copy($out->app, $argv[0]);
	app_func_copy($out->cfg, $argv[1]);
	app_func_copy($out->run, $argv[2]);
	app_func_copy($out->scr, $argv[3]);
	
	/*! \remark for scren hide by default show */
	app_func_copy($smt->AppProgScr, 1); 
	if ( isset($argv[3]) )
	{	
		/*! \remark < scr :  0 is hide console 1:show console > */
		app_func_copy($smt->AppProgScr, 
			(int)substr($out->scr, strpos($out->scr, chr(61))+1, strlen($out->scr)) 
		);
	}
	
	// check data for validation 
	if ($out->cfg) {
		app_func_copy($out->cfg, 
			substr($out->cfg, strpos($out->cfg, chr(61))+1, strlen($out->cfg)) 
		);
	}
	
	// check data for validation 
	if ($out->run) {
		app_func_copy( $out->run, 
			substr($out->run, strpos($out->run, chr(61))+1, strlen($out->run))
		);
	}
	 
	// copy Main Program 
	if ( !file_exists($out->cfg) )
	{
		smt_log_error( "Failed Configuration.", __FILE__, __LINE__ );
		exit(0);
	}
	
	// next Iterator process  
	if( !$smt->AppProgCfg )
	{
		app_func_copy($smt->AppProgCfg, $out->cfg);
	}
	
	// running config 
	if ( !$smt->AppProgLck )
	{
		app_func_copy($smt->AppProgLck, $out->run);
	} 
	
	// free file config 
	app_func_free($out);
	
	// grep All Config File 
	return $smt;
}


/**!
 * \brief 	app_cfg_parser
 * \note 	[description]
 * \retval  [description]
 *
 */
 function app_cfg_parser($buf = NULL, &$ret= 0)
{
	// default retvals 
	app_func_struct($cfg);
	app_func_copy($ret, 0);
	
	if (!$buf)
	{
		return 0;	
	}
	
	/*! \retval if read config fail is false */
	if (false === ($cfg->sof = file_get_contents($buf)) )
	{
		return 0;
	}		
	
	/*! \retval if read config fail is false */
	if( false == ($cfg->buf = array_map('trim', explode("\n", $cfg->sof)) ))
	{
		return 0;
	}
	
	/*! \retval if Next Iterator successfuly */ 
	app_func_copy( $cfg->size, count($cfg->buf));
	app_func_copy( $cfg->next, 0);
	
	/*! \remark Create Allocation Array Populated */ 
	
	app_func_alloc($cfg->app); 
	while ( $cfg->next < $cfg->size )
	{
		/*! \remark copy link from app */
		app_func_copy($cfg->ptr, 
			trim($cfg->buf[$cfg->next])
		);
		
		/*! \remark validation length */
		if (!strlen($cfg->ptr))
		{
			app_func_next($cfg->next);
			continue;
		}   
		
		/*! \remark check istring not valid */
		if (substr($cfg->ptr, 0, 1) == chr(59))
		{
			app_func_next($cfg->next);
			continue;
		}
		
		/*! \remark validation of data process */
		if (false == $cfg->ptr){
			app_func_next($cfg->next);
			continue;
		}
		
		/*! \remark validation of data process */
		app_func_copy($cfg->has, stristr($cfg->ptr, chr(91)));
		
		if (false != $cfg->has)
		{
			app_func_copy($cfg->hex, strpos($cfg->ptr, chr(91)));
			
			if ($cfg->idx = substr($cfg->ptr, $cfg->hex+1, strlen($cfg->ptr)-2))
			{
				app_func_alloc($cfg->app[$cfg->idx]); 
			}
		}
		
		/*! \remark for body context here then will exclude */
		
		if (false == $cfg->has)
		{
			if (list($i, $j) = array_map( 'trim', explode( chr(61), $cfg->ptr )))
			{
				$cfg->app[$cfg->idx][$i] = $j; 
			}
		} 
		
		/*! \def next loop process */
		app_func_next($cfg->next);
	}
	
	/*! \retval if successfuly then clear Allocation Size */  
	if (app_func_copy($ret, $cfg->app))
	{
		app_func_free($cfg);
	}  
	
	return $ret;
} 

/*!
 * \brief 	app_cfg_param
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_param($cfg = NULL, $key = NULL, &$ret = '')
{
	Global $smt;
	
	/*! jika di temukan keynya */
	if( !isset($smt->AppProgVar[$cfg]))
	{
		return '';
	}
	
	/*! jika di temukan keynya */
	if ( !isset($smt->AppProgVar[$cfg][$key]))
	{
		return '';
	}
	
	/*! copy process on program */
	app_func_copy( $ret, 
		$smt->AppProgVar[$cfg][$key]
	);
	
	/*! \retval final is OK */
	return $ret; 
}

/*!
 * \brief 	app_cfg_header
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_header($cfg = NULL, &$ret = '')
{
	Global $smt;
	
	/*! jika di temukan keynya */
	if( !isset($smt->AppProgVar[$cfg]))
	{
		return '';
	}
	
	/*! copy process on program */
	app_func_copy( $ret, 
		$smt->AppProgVar[$cfg]
	);
	
	/*! \retval final is OK */
	return $ret; 
}

/*!
 * \brief 	app_cfg_alerting
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_alerting($str = NULL, &$ret = false )
{
	if ( !strstr($str, 'EventAlerting') )
	{
		return 0;
	}
	
	if ( !strpos($str, chr(44)))
	{
		return 0;
	}
	
	$str = str_replace( array('EventAlerting', chr(40), chr(41)),
		array('', '', ''),  $str);
		
	$ret = array_map('trim', explode(chr(44), $str));
	return $ret;
}

/*!
 * \brief 	app_cfg_handler
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_handler($str = NULL, &$ret = false )
{
	if ( !strstr($str, 'EventHandler') )
	{
		return 0;
	}
	
	if ( !strpos($str, chr(44)))
	{
		return 0;
	}
	
	$str = str_replace( array('EventHandler', chr(40), chr(41)),
		array('', '', ''),  $str);
		
	$ret = array_map('trim', explode(chr(44), $str));
	return $ret;
}

/*!
 * \brief 	app_cfg_handler
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_stream($cfg = NULL, $key = NULL, &$ret = '')
{
	Global $smt;
	
	if (!isset($smt->$cfg))
	{
		return 0;
	}
	
	/*! copy process on program */
	app_func_copy($ret, $smt->$cfg); 
	if ( !is_null($key) )
	{
		$ret = ( isset($ret[$key]) ? $ret[$key] : false);
	}
	
	return $ret;
}