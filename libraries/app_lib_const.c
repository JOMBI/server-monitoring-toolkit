<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \def Sistem Monitoring 'Symon' */

class Simon 
{
	/*! \retval Static Proc System */
	private static $SMTProc = array
	(
		'version'	=> '/proc/version',
		'cpuinfo'	=> '/proc/cpuinfo',
		'device'	=> '/proc/net/dev',
		'uptime'	=> '/proc/uptime',
		'stat'  	=> '/proc/stat',
		'load'  	=> '/proc/loadavg',
		'memory'  	=> '/proc/meminfo',
		'swap'  	=> '/proc/swaps' 
	); 
	
	/*! \retval Static Exec System */
	
	
	private static $SMTExec = array
	( 
		'CMD01'	=> 'df -k 2>&1',
		'CMD02'	=> 'df -h 2>&1',
		'CMD03'	=> 'fre -h 2>&1',
		'CMD04'	=> 'fre -m 2>&1' 
	);
	
   /**
	* \brief  	public Static Proc 
	*
	* \param 	sig type data is *str 
	* \retval 	self . array populate 
	*
	*/
	public static function Proc($sig = '')
	{
		return (array_key_exists($sig, self::$SMTProc) ?
			self::$SMTProc[$sig] : false);
	}
	
	/**
	* \brief  	public Static Exec 
	*
	* \param 	sig type data is *str 
	* \retval 	self . array populate 
	*
	*/
	public static function Exec($sig = '')
	{
		return (array_key_exists($sig, self::$SMTExec) ?
			self::$SMTExec[$sig] : false);
	}
}