<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 if( !defined('SMT_LOCK_RUN') ) define( 'SMT_LOCK_RUN', '/var/run/smt');
 
/*! 
 * \brief app_fork_run 
 *
 * \notes check pid still alive Or not 
 *
 * \param {pid} description 
 * \param {ret} description 
 *
 * \retval integer 
 *
 */    
 function app_fork_run(&$smt)
{
	/*! \remark check assign Log run */
	if (!$smt->AppProgLck) 
	{
		app_func_copy($smt->AppProgLck, SMT_LOCK_RUN);
	}
	
	/*! \retval get base of dir */
	app_func_copy($smt->AppProgLckDir, dirname($smt->AppProgLck));
	app_func_copy($smt->AppProgLckPid, basename($smt->AppProgLck));
	
	if (!is_dir($smt->AppProgLckDir))
	{
		system(sprintf( "mkdir -p %s && chmod 0777 %s", $smt->AppProgLckDir, $smt->AppProgLckDir));
		if (!is_dir($smt->AppProgLckDir) )
		{
			smt_log_error(sprintf("error 'app_fork_run(0)' failed created dir '%s' .", $smt->AppProgLckDir),
			__FILE__, __LINE__);
			exit(0);
		}
	}
	
	/*! \remark check base pid */
	if (!stristr($smt->AppProgLckPid, '.pid'))
	{
		smt_log_error(sprintf("error 'app_fork_run(1)' failed created pid '%s' .", $smt->AppProgLck),
		__FILE__, __LINE__);
		exit(0);
	} 
	
	/*! \remark next created dump of PID */
	if (!$smt->AppProgPid)
	{
		smt_log_error(sprintf("error 'app_fork_run(2)' failed created pid '%s' .", $smt->AppProgLck),
		__FILE__, __LINE__);
		exit(0);
	}
	
	/*! \remark check if false process */
	if(false === @file_put_contents($smt->AppProgLck, $smt->AppProgPid))
	{
		smt_log_error(sprintf("error 'app_fork_run(3)' failed created pid '%s' .", $smt->AppProgLck),
		__FILE__, __LINE__);
		exit(0);
	}
	
	return 0;
}  
 
/*! 
 * \brief app_fork_pid 
 *
 * \notes check pid still alive Or not 
 *
 * \param {pid} description 
 * \param {ret} description 
 *
 * \retval integer 
 *
 */  
 function app_fork_kill($pid = 0, $sig = SIGKILL )
{
	/*! \retval check function 'posix_kill' */
	
	if ( !function_exists('posix_kill') )
	{
		return 0;	
	}
	
	/*! \retval Ok Retval Sign Kill */
	if ( $pid && posix_kill($pid, $sig) )
	{
		return $pid;
	}
	
	return 0;
} 
 
/*! 
 * \brief app_fork_pid 
 *
 * \notes check pid still alive Or not 
 *
 * \param {pid} description 
 * \param {ret} description 
 *
 * \retval integer 
 *
 */  
 function app_fork_pid($pid = NULL, &$ret = 0)
{
	app_func_struct($obj);
	app_func_copy($obj->pid, (int)$pid);
	app_func_copy($obj->alive, 0); 
	
	/*! \remark Iterasi Untuk process */
	for (;;) { 
		if ( !$obj->pid OR ($obj->pid == 1)) 
		{
			app_func_copy($obj->alive,0);
			app_func_copy($ret, $obj->alive);
			app_func_free($obj);
			return $ret;
		} 
		
		// validation of function 
		if (!function_exists( 'posix_getsid'))
		{
			app_func_copy($obj->alive,0);
			app_func_copy($ret, $obj->alive);
			app_func_free($obj);
			return $ret;
		}
		
		// *find process by *ID
		if ($obj->pid > 1) {
			app_func_copy($obj, 'process', 
				posix_getsid($obj->pid)
			);
			
			if (false === $obj->process )
			{
				app_func_copy($obj->alive,0);
				app_func_copy($ret, $obj->alive);
				app_func_free($obj);
				return $ret;
			}
			
			if ($obj->process){
				$obj->alive++;
				app_func_copy($ret, $obj->pid);
			}
		}
		break;
	} 
	
	// copy into ret;
	app_func_free($obj);
	return $ret;	
}