<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
if(!defined( 'SMT_VAR_LOG' )) define( 'SMT_VAR_LOG', '/var/log/smt');

/*!
 * \brief 	smt_log_main
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_main($app = NULL, $str = NULL)
{
	Global $smt; 
	
	/*! \retval make void Time for application */
	
	app_func_struct($log); 
	app_func_copy($log->app, $app);
	app_func_copy($log->str, $str); 
	
	/*! \retval if succes proces then sent signal break */
	if (app_func_copy( $log->time, date('H:i:s')))
	{
		/*! \remark add color for style console screen */
		$log->out = sprintf("%s %s[%s]: %s", $log->time, $log->app, $smt->AppProgPid, $log->str);
		
		smt_log_stream($smt, $log->out); 
		if ($smt->AppProgScr &&(app_func_color($log->out, 32)))
		{
			printf("%s\n", $log->out);
		}
	}
	
	/*! \retval clean up memory usage */
	app_func_free($log);
	return 0;
}

/*!
 * \brief 	smt_log_main
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_stream(&$smt = NULL, $str = "", &$ret = 0)
{
	/*! \remark one touch process */
	app_func_copy($smt->LogIsDir , 0);
	app_func_copy($smt->LogIsFile, 0);
	
	/*! \retval check basedir of log */
	if (!isset($smt->LogPathCfg) )
	{
		app_func_copy($smt->LogPathCfg, SMT_VAR_LOG);
	}
	
	/*! \retval create base of Log */
	if ($smt->AppProgUid &&(!isset($smt->LogPathUid)))
	{
		app_func_copy($smt->LogPathUid, 
			strtolower($smt->AppProgUid)
		);	
	} 
	
	/*! \remark dir one check dont check every time 
	because exhause of CPU consume */ 
	
	if (!$smt->LogIsDir && !is_dir($smt->LogPathCfg))
	{
		system (sprintf("mkdir -p %s && chmod 0777", $smt->LogPathCfg) );
		if (!is_dir($smt->LogPathCfg))
		{
			printf("error 'smt_log_stream(0)' failed created log directory '%s' .\n", $smt->LogPathCfg);
			return 0;
		}
		
		/*! \retval add new isdir */
		app_func_next($smt->LogIsDir);
	} 
	
	
	/*! \retval will rotate every day */
	
	$smt->LogPathNow = date('Ymd');
	if (!strcmp($smt->LogPathNow, $smt->LogPathDts))
	{
		app_func_copy($smt->LogPathRoot, 
			sprintf( "%s/%s-%s.log", $smt->LogPathCfg, $smt->LogPathUid, $smt->LogPathNow)
		);
		
		if (!is_resource($smt->LogPathLink))
		{
			$smt->LogPathLink = fopen($smt->LogPathRoot, 'a');
			if (!$smt->LogPathLink)
			{
				printf("error 'smt_log_stream(3)' failed created log stream '%s' .\n", $smt->LogPathRoot);
				return 0;
			}
			
			/*! \retval input file to this process */
			system(sprintf("chmod 0777 %s", $smt->LogPathRoot));
			app_func_copy($ret, $smt->LogPathLink);
		}
	}
	
	/*! \retval diffrent day */
	else if (strcmp($smt->LogPathNow, $smt->LogPathDts))
	{
		/*! \remark close stream file */
		@fclose($smt->LogPathLink);
		
		/*! \remark update config Rotate to Now */
		app_func_copy($smt->LogPathLink, -1);
		app_func_copy($smt->LogPathDts , $smt->LogPathNow);
		app_func_copy($smt->LogPathRoot, 
			sprintf( "%s/%s-%s.log", $smt->LogPathCfg, $smt->LogPathUid, $smt->LogPathNow)
		);
		
		if (!is_resource($smt->LogPathLink))
		{
			$smt->LogPathLink = fopen($smt->LogPathRoot, 'a');
			if (!$smt->LogPathLink)
			{
				printf("error 'smt_log_stream(3)' failed created log stream '%s' .\n", $smt->LogPathRoot);
				return 0;
			}
			
			/*! \retval input file to this process */
			system(sprintf("chmod 0777 %s", $smt->LogPathRoot));
			app_func_copy($ret, $smt->LogPathLink);
		}
	}
	

	/*! \retval If Succcess then write log */	
	if (is_resource($smt->LogPathLink))
	{
		@fwrite( $smt->LogPathLink, 
			sprintf("%s\n", $str)
		);
	}
	
	return $ret;
	
 }

/*!
 * \brief 	smt_log_error
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_error($str = NULL, $file= NULL, $line = 0)
{
	Global $smt;  
	
	/*! make void Time */
	if ( function_exists('smt_log_main') )
	{
		if (file_exists($file))
		{ 
			smt_log_main('SMT', sprintf("-- WARNING: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	smt_log_info
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_info($str = NULL, $file= NULL, $line = 0)
{
	Global $smt;  
	
	/*! make void Time */
	if ( function_exists('smt_log_main') )
	{
		if (file_exists($file))
		{ 
			smt_log_main('SMT', sprintf("-- INFO: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	smt_log_notice
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_notice($str = NULL, $file= NULL, $line = 0)
{
	Global $smt;  
	
	/*! make void Time */
	if ( function_exists('smt_log_main') )
	{
		if (file_exists($file))
		{ 
			smt_log_main('SMT', sprintf("-- NOTICE: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	smt_log_app
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_app($str = NULL)
{
	Global $smt;  
	
	/*! make void Time */
	if ( function_exists('smt_log_main') )
	{
		smt_log_main('SMT', $str);
	} 
	return 0;
}

/*!
 * \brief 	smt_log_core
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_core( $name = "", $log = NULL )
{
	Global $smt;  
	if ( $name == '' )
	{
		smt_log_error(" dump 'smt_log_core(1)' Fail.", __FILE__, __LINE__);
		return 0;
	}
	
	if ( !file_put_contents( SMT_APP ."/$name.core", print_r($log, true)) )
	{
		smt_log_error(" dump 'smt_log_core(2)' Fail.", __FILE__, __LINE__);
		return 0;
	} 
	
	return 0;
}

/*!
 * \brief 	smt_log_baner
 * \note 	[description]
 * \retval  [description]
 *
 */
 function smt_log_scr(&$smt = NULL, $log = "" )
{
	/*! \retval sent to log stream */ 
	smt_log_stream($smt, $log); 
	
	/*! \retval set color log for screen */
	if ($smt->AppProgScr &&(app_func_color($log, 32))) 
	{
		printf("%s\n", $log);
	} 
	
	app_func_free($log);
	return 0;
 }

