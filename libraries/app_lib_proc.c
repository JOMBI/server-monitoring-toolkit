<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! 
https://www.kernel.org/doc/Documentation/filesystems/proc.txt
$.smt->AppProc->loadavg(); 
$.smt->AppProc->uptime(); 
$.smt->AppProc->network(); 
$.smt->AppProc->memory(); 
$.smt->AppProc->disk(); 
$.smt->AppProc->service(); 
*/

class SMTAppProc 
{
	
   /**
	* \brief Properties 
	*
	* \note  for access from Thread Process 
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	
	 private static $instance = NULL;
	 public static function &get_instance() 
    {
		if (is_null(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}
    
	/**
	* \brief vendor
	*
	* \note  for access from Thread Process 
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	
	 function vendor(&$ret = false)
	{
		Global $smt;
		
		/*! \remark get Contents */
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		/*! \remark retval on here */
		app_func_copy($out->res,'event','Vendor');
		app_func_copy($out->res,'time',strtotime('now'));
		app_func_copy($out->res,'data', array());
		
		
		/*! \remark retval on here */
		app_func_copy($out->ret,'v_id',app_vendor_id()); 
		app_func_copy($out->ret,'v_name',app_vendor_name());
		app_func_copy($out->ret,'v_hostip',app_vendor_host());
		app_func_copy($out->ret,'v_hostname',app_vendor_hostname());
		app_func_copy($out->ret,'v_kernel',app_vendor_kernel());
		 
	    /*! \retval Process Of "Vendor" */
		app_func_copy($out->res, 'data', $out->ret);
		if ( app_func_copy($ret, $out->res) )
		{
			app_func_free($out);
		}
		
		return $ret;
	}
   /**
	* \brief hostname
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function hostname(&$ret = false)
	{
		exec( "hostname", $retval);
		if ( !is_array($retval) )
		{
			return 0;	
		}
		
		/*! \retval copy intens */
		if (app_func_copy($ret, reset($retval)))
		{
			app_func_free($retval);			
		}
		
		return $ret;
	}
	
	   /**
	* \brief kernel
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function kernel(&$ret = false)
	{
		exec( "uname -r", $retval);
		if ( !is_array($retval) )
		{
			return 0;	
		}
		
		/*! \retval copy intens */
		if (app_func_copy($ret, reset($retval)))
		{
			app_func_free($retval);			
		}
		
		return $ret;
	}
	

   /**
	* \brief CPU Info
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function cpuinfo(&$ret =0)
	{
		/*! \remark get Contents */
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		/*! \remark retval on here */
		app_func_copy($out->res, 'event','Cpuinfo');
		app_func_copy($out->res, 'vmid' , app_vendor_id()); 
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		/*! \retval if false 'file_get_contents' return 0 */
		if ( false === ($out->buf = @file_get_contents(Simon::Proc('cpuinfo')))) {
			app_func_free($out);
			return 0;	
		}
		
		/*! \retval if false 'explode' return 0 */
		app_func_trim($out->buf);  
		if ( false === ($out->buf = explode( "\n", $out->buf))) {
			app_func_free($out);
			return 0;
		}
		
		
		/*! \def  Next process iteration  */
		foreach ( $out->buf as $i => $r) 
		{
			/*! \remark clean All characters process */ 
			if ( !app_func_trim($r) ){
				continue;
			}
			
			/*! \remark false process then next iterator */ 
			if (false === (list($out->p, $out->q)= array_map('trim', preg_split( '/:/', $r )))){
				continue;
			}
			
			/*! \remark Set to Key After clean process */
			 if($out->p = preg_replace('/\s+/', '_', $out->p))
			{
				$out->p = strtolower($out->p);
				if ( app_func_trim($out->q) ){ 
					$out->ret[$out->p] = $out->q;
				}
			}
		}
		
	   /**
		* \retval Process Of "cpuinfo"
		* After sed data to out object, then will copy new section 'ret' 
		* And clean All Session Allocation Memory Usage 
		*
		*/
		$out->res['data'] = $out->ret;
		if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		}
		
		return $ret;
	}
	
	
   /**
	* \brief Version 
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function version(&$ret= 0)
	{
		/*! \remark get Contents */
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		/*! new file */
		app_func_copy($out->res, 'event', 'Version');
		app_func_copy($out->res, 'vmid' , app_vendor_id()); 
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		/*! \remark false process then next iterator */ 
		if (false === ($out->buf = @file_get_contents(Simon::Proc('version')))) 
		{
			app_func_free($out);
			return 0;
		}
		
		/*! \remark Next Process to singgle Retval */
		if (app_func_trim($out->buf)) 
		{ 
		   /*! \def put list objects on here */
			if (false === (list( $osi, $vrk, $vrv, $org, $na, $na, $gcc, $rev) = preg_split('/\s/', $out->buf))) {
				app_func_free($out);
				return 0;
			}
			
			/*! \retval process for this Application */
			app_func_copy($out->ret, 'osi', $osi);
			app_func_copy($out->ret, 'ver', $vrv);
			app_func_copy($out->ret, 'org', $org);
			app_func_copy($out->ret, 'gcc', $gcc);
			app_func_copy($out->ret, 'rev', $rev);
		} 
		
	   /*!
		* \retval Process Of "version"
		* After sed data to out object, then will copy new section 'ret' 
		* And clean All Session Allocation Memory Usage 
		*
		*/
		app_func_copy($out->res, 'data', $out->ret);
		if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		} 
		
		return $ret; 
	}
	
   /**
	* \brief Load Average
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	
	 function average(&$ret= 0) 
    {	
		/*! \remark </proc/loadavg> */ 
		app_func_alloc($res);
		app_func_copy($res,'event', 'Average');
		app_func_copy($res,'vmid', app_vendor_id()); 
		app_func_copy($res,'time', strtotime('now'));
		app_func_copy($res,'data', array());
		
		app_func_struct($out);
		app_func_alloc($out->ret);
		
		$buf = @file_get_contents( Simon::Proc('load') );
		if ($buf == "ERROR")
			$out->ret['avg'] = array('N.A.', 'N.A.', 'N.A.');
		else if ($buf != "ERROR") 
		{
			if ($bufs = preg_split("/\s/", $buf, 4))
			{
				unset($bufs[3]);
				$out->ret['avg']['vol1'] = $bufs[0];
				$out->ret['avg']['vol2'] = $bufs[1];
				$out->ret['avg']['vol3'] = $bufs[2];
			}
			
			// $out->ret['avg']
			
			//unset($out->ret['avg'][3]);        // don't need the extra values, only first three
		}
		
		/*! \remark </proc/stat> */ 
		
		$buf = @file_get_contents(Simon::Proc('stat'));
		if ( $buf != "ERROR" )  
		{
			sscanf($buf, "%*s %f %f %f %f", $ab, $ac, $ad, $ae);
			
			// Find out the CPU load
			// user + sys = load
			// total = total
			  
			$load = $ab + $ac + $ad;        	// cpu.user + cpu.sys
			$total = $ab + $ac + $ad + $ae; 	// cpu.total
			
			// we need a second value, wait 1 second befor getting (< 1 second no good value will occour)
			sleep(1);
			$buf = @file_get_contents(Simon::Proc('stat'));
			
			sscanf($buf, "%*s %f %f %f %f", $ab, $ac, $ad, $ae);
			
			$load2 = $ab + $ac + $ad;
			$total2 = $ab + $ac + $ad + $ae;
			$out->ret['cpu']['percent'] = ($total2 != $total)?((100*($load2 - $load)) / ($total2 - $total)):0;
		}
		
		$res['data'] = $out->ret;
		
		if ( app_func_copy($ret, $res) )
		{
			app_func_free($out);
		}
		return $ret;
	}
  
   /**
	* \brief Uptime
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function uptime(&$ret= 0)
	{
		/*! \remark </proc/uptime> */  
		app_func_alloc($uptime);
		app_func_alloc($retval);
		app_func_copy($retval, 'event', 'Uptime');
		app_func_copy($retval, 'vmid' , app_vendor_id());
		app_func_copy($retval, 'time' , strtotime('now'));
		app_func_copy($retval, 'data' , array());
		
		
		$buf = @file_get_contents(Simon::Proc('uptime'));
		$ar_buf = preg_split( '/\s/', $buf );
		$timestamp = trim( $ar_buf[0] );
		 
		
		$min = $timestamp / 60;
		$hours = $min / 60;
		$days = floor($hours / 24);
		$hours = floor($hours - ($days * 24));
		$min = floor($min - ($days * 60 * 24) - ($hours * 60));
		$sec = date('s', $timestamp);
		
		$uptime['day'] = 0;
		$uptime['hour']= $hours;
		$uptime['min']= $min;
		$uptime['sec']= intval($sec);
		if ($days != 0) {
			$uptime['day']= $days;
		}

		if ($hours != 0) {
			$uptime['hour']= $hours;
		}

		$retval['data'] = $uptime;
		if (is_array($retval))
		{
			if ( app_func_copy($ret, $retval))
			{
				app_func_free($uptime);
				app_func_free($retval);
			}
			
		}
		
		return $ret;
	}
	
   /**
	* \brief network
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function network(&$ret = 0)  
	{
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		
		/*! \retval Memory data */  
		app_func_copy($out->res, 'event', 'Network');
		app_func_copy($out->res, 'vmid' , app_vendor_id());
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		
		/*! \remark open file to process on '/proc/net/dev' */
		app_func_copy( $out->get, 
			@file_get_contents(Simon::Proc('device'))
		); 
		
		if ( $out->get == "ERROR" ) 
			return $out->ret; /*! \retval break stop if ERROR process */
			
		$out->pck = explode("\n", $out->get); 
		foreach ($out->pck as $buf) 
		{
			if (preg_match('/:/', $buf))
			{
				$out->buf = $buf;
				
			   /*! \remark for tested only developer debug */
				$test = preg_split('/:/', $out->buf, 2);
				
			   /*!
			    * \remark list($i, $j) 
				* \param  {i} name of device eth or network device  
				* \param  {j} static Of device 
				*
				*/
				
				if ( false === (list($i, $j) = preg_split('/:/', $out->buf, 2)) )
				{
					continue;
				}
				
			    /*! \remark for list data from every Network Device Or eth */
				$out->stats = preg_split('/\s+/', app_func_trim($j));
				if (!is_array($out->stats))
				{
					continue;
				}
				
				/*! \remark write device Name by Index */
				app_func_trim($i);
				if ( !isset($out->ret[$i]) )
				{
					app_func_alloc($out->ret[$i]);
				}
				
				/*! \remark add all variable to array content */
				
				$out->ret[$i] = array();
				$out->ret[$i]['rx_bytes'] 	= $out->stats[0];
				$out->ret[$i]['rx_packets'] = $out->stats[1];
				$out->ret[$i]['rx_errs'] 	= $out->stats[2];
				$out->ret[$i]['rx_drop'] 	= $out->stats[3]; 
				$out->ret[$i]['tx_bytes'] 	= $out->stats[8];
				$out->ret[$i]['tx_packets']	= $out->stats[9];
				$out->ret[$i]['tx_errs'] 	= $out->stats[10];
				$out->ret[$i]['tx_drop'] 	= $out->stats[11];
				$out->ret[$i]['errs'] 		= $out->stats[2] + $out->stats[10];
				$out->ret[$i]['drop'] 		= $out->stats[3] + $out->stats[11];
			}
		}
		
		/*! \retval finaly return is <array> */	
		$out->res['data'] = $out->ret;
		
		if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		}
		
		return $ret;
	}

   /**
	* \brief memory
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function memory(&$ret = 0) 
	{
		/*! \retval app_func_struct */  
		
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		/*! \retval Memory data */  
		app_func_copy($out->res, 'event', 'Memory');
		app_func_copy($out->res, 'vmid' , app_vendor_id());
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		
		// next 	
		$out->ret['ram'] = array();
		$out->ret['swap'] = array();
		$out->ret['devswap'] = array();
		
		
	    /*! \remark </proc/meminfo> */
		
		$out->bufr = @file_get_contents( Simon::Proc('memory') );
		if ($out->bufr == 'ERROR' ){
			return 0;
		}
		
		$out->bufe = explode( "\n", $out->bufr);
		foreach ($out->bufe as $buf) 
		{
			if (preg_match('/^MemTotal:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['ram']['total'] = trim($ar_buf[1]);
			} else if (preg_match('/^MemFree:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['ram']['t_free'] = trim($ar_buf[1]);
			} else if (preg_match('/^Cached:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['ram']['cached'] = trim($ar_buf[1]);
			} else if (preg_match('/^Buffers:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['ram']['buffers'] = trim($ar_buf[1]);
			} else if (preg_match('/^SwapTotal:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['swap']['total'] = trim($ar_buf[1]);
			} else if (preg_match('/^SwapFree:\s+(.*)\s*kB/i', $buf, $ar_buf)) {
				$out->ret['swap']['free'] = trim($ar_buf[1]);
			}
		}
		
		$out->ret['ram']['t_used'] 	= $out->ret['ram']['total'] - $out->ret['ram']['t_free'];
		$out->ret['ram']['percent'] = $out->ret['ram']['total'] ? (round( (($out->ret['ram']['t_used'] / $out->ret['ram']['total']) *100),2)) : 0;
		$out->ret['ram']['p_free'] = $out->ret['ram']['total'] ? (round( (($out->ret['ram']['t_free'] / $out->ret['ram']['total']) *100),2)) : 0;
		$out->ret['swap']['used'] 	= $out->ret['swap']['total'] - $out->ret['swap']['free'];
		
		/*! \remark If no swap, avoid divide by 0 */
		/*! \remark If no swap, avoid divide by 0 */
		
		if (trim($out->ret['swap']['total'])) {
			$out->ret['swap']['percent'] = round((($out->ret['swap']['used']/$out->ret['swap']['total']) *100),2);
		} else {
			$out->ret['swap']['percent'] = 0;
		}
		
		/*! \remark values for splitting memory usage */
		/*! \remark values for splitting memory usage */
		if (isset($out->ret['ram']['cached']) && isset($out->ret['ram']['buffers'])) 
		{
			$out->ret['ram']['app'] = $out->ret['ram']['t_used'] - $out->ret['ram']['cached'] - $out->ret['ram']['buffers'];
			$out->ret['ram']['app_percent'] = round(($out->ret['ram']['app'] * 100) / $out->ret['ram']['total'], 2);
			$out->ret['ram']['buffers_percent'] = round( ($out->ret['ram']['buffers'] * 100) / $out->ret['ram']['total'], 2);
			$out->ret['ram']['cached_percent'] = round( ($out->ret['ram']['cached'] * 100) / $out->ret['ram']['total'], 2);
		}
		
		/*! \remark </proc/swaps> */	  
		$out->bufr = @file_get_contents(Simon::Proc('swaps'));
		if ($out->bufr == 'ERROR'){
			return 0;
		}
		
		/*! \remark get data swap process  on here like process */ 
		$out->swaps = array_map( 'trim', explode("\n", $out->bufr) ); 
		$out->sizeof = sizeof($out->swaps); 
		for ($i= 1; $i<$out->sizeof; $i++)
		{ 
			/*! \remark If data is empty */
			/*! \remark If data is empty */
			
			if (app_func_trim($out->swaps[$i]) == '') {
				continue;
			} 
			
			/*! \remark preg_split <devswap> */
			/*! \remark preg_split <devswap> */
			
			if ($out->ar_buf = preg_split( '/\s+/', $out->swaps[$i], 6))
			{
				$out->ret['devswap'][$i-1] 			 = array();
				$out->ret['devswap'][$i-1]['dev'] 	 = $out->ar_buf[0];
				$out->ret['devswap'][$i-1]['total']  = $out->ar_buf[2];
				$out->ret['devswap'][$i-1]['used'] 	 = $out->ar_buf[3];
				$out->ret['devswap'][$i-1]['free'] 	 = ($out->ret['devswap'][$i - 1]['total'] - $out->ret['devswap'][$i - 1]['used']);
				$out->ret['devswap'][$i-1]['percent']= round(($out->ar_buf[3] * 100) / $out->ar_buf[2], 2);
			}
		}
		
	    /*! \retval return of process on here */ 
	    /*! \retval return of process on here */ 
		
		$out->res['data'] = $out->ret;
		if ( app_func_copy($ret, $out->res) )
		{
			app_func_free($out);
		}
		return $ret;
		
	}
   
   /*!
	* \brief hardisk
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function hardisk(&$ret = 0) 
	{
		/*! \retval app_func_struct */
		app_func_struct($out);
		app_func_alloc($out->ret);
		app_func_alloc($out->res);
		
		// for res 
		app_func_copy($out->res, 'event', 'Hardisk'); 
		app_func_copy($out->res, 'vmid' , app_vendor_id()); 
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		
		/*! \retval df -k */ 
		app_func_free($out->buf);
		exec( Simon::Exec('CMD01'), $out->buf ); 
		if (!is_array($out->buf))
		{
			return 0;
		} 
		
		/*! \retval get hardisk for this section */
		app_func_copy($i,0);  
		foreach ($out->buf as $df_line)
		{
			/* first row skip proc */
			if (!$i)
			{
				app_func_next($i);
				continue;
			}
			
			app_func_copy($q,0);
			if (false !== ($df_row = preg_split('/\s/', $df_line))){
				if (false !== ($hask = preg_split( '/\//', $df_row[0]))){
					app_func_copy($q, end($hask));
				}
				
			}
			
			
			$df_buf1 = preg_split("/(\%\s)/", $df_line, 2);   
			$preg_match = preg_match("/(.*)(\s+)(([0-9]+)(\s+)([0-9]+)(\s+)([0-9]+)(\s+)([0-9]+)$)/", $df_buf1[0], $df_buf2);
			if ($preg_match == 0) 
			{
				app_func_next($i);
				continue;
			}
			
			/*! \remark array push data to storage */
			$df_buf = array
			(
				$df_buf2[1], 
				$df_buf2[4], 
				$df_buf2[6], 
				$df_buf2[8], 
				$df_buf2[10], 
				$df_buf1[1]
			);
			
			/*! \remark rekondition process */
			$out->ret[$q] 		     = array();
			$out->ret[$q]['disk']    = str_replace( "\\$", "\$", $df_buf1[1] );
			$out->ret[$q]['size']    = $df_buf[1];
			$out->ret[$q]['used']    = $df_buf[2];
			$out->ret[$q]['free']    = $df_buf[3];
			$out->ret[$q]['percent'] = round(($out->ret[$q]['used'] * 100) / $out->ret[$q]['size']);
			
			/*! loopback counter */
			app_func_next($i);
		} 
		
		/*! \remark retval */ 
		app_func_copy($out->res, 'data', $out->ret);
		if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		}
		
		return $ret;
	}
	
   /*!
	* \brief Storage
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function storage(&$ret= false)
	{
		Global $smt;
		
		/*! \remark for Pooling Thread */
		app_func_struct($out);
		app_func_alloc($out->res);
		app_func_copy($out->res, 'event', 'Storage');
		app_func_copy($out->res, 'vmid',  app_vendor_id());
		app_func_copy($out->res, 'time',  strtotime('now'));
		app_func_copy($out->res, 'data',  array());	
		
		
		/*! \retval second layer */
		app_func_alloc($stg);
		app_func_copy($stg, 'time', strtotime('now'));
		app_func_copy($stg, 'message', 'Storage Pooling');
		
		if (app_func_copy($out->res, 'data', $stg))
		{
			app_func_free($stg);
			if ( app_func_copy($ret, $out->res) )
			{
				app_func_free($out);
			}
		}
		
		return $ret;
	}
	
	
	/*!
	* \brief service
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function service(&$ret=false)
	{
		Global $smt;
		
		app_func_struct($out);
		app_func_alloc($out->res);
		app_func_copy($out->res, 'event', 'Service');
		app_func_copy($out->res, 'vmid' , app_vendor_id());
		app_func_copy($out->res, 'time' , strtotime('now'));
		app_func_copy($out->res, 'data' , array());
		
		foreach ($smt->AppService as $uid => $svc)
		{
		   /*!
			* \remark Iteration for check Service Status 
			* 
			* Process Pencarian Service Berdasarkan Lokasi PID 
			* Jika file locate tidak di temukan maka langsung saja exit, ubah state menjadi == 0 
			*
			*/
			
			app_func_free($proc);
			
			if (app_func_typdef($svc, $proc))
			{
				if (!file_exists($proc->lock))
				{
					unset($smt->AppService[$proc->name]['pid']);
					
					$smt->AppService[$proc->name]['state'] = 0;  
					
					$out->res['data'][$proc->name] = $smt->AppService[$proc->name];
					unset($out->res['data'][$proc->name]['lock']);
					continue;
				}		
				
				/*! \remark get PID No from this file */
				$proc->pid = file_get_contents($proc->lock);
				app_func_trim($proc->pid);
				if ($proc->pid)
				{
					unset($smt->AppService[$proc->name]['pid']);
					$smt->AppService[$proc->name]['pid'] = $proc->pid;
				}
				
				// next check pid with test 
				// exec("ps -p $proc->pid -o comm=", $prog); 
				
				if (!app_fork_pid($proc->pid))
				{
					unset($smt->AppService[$proc->name]['state']);
					$smt->AppService[$proc->name]['state'] = 0; 
				} 
				else if (app_fork_pid($proc->pid)) 
				{
					unset($smt->AppService[$proc->name]['state']);
					$smt->AppService[$proc->name]['state'] = 1; 
				}
				
				$out->res['data'][$proc->name] = $smt->AppService[$proc->name];
				unset($out->res['data'][$proc->name]['lock']);
			}
		}
		
		if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		}
		
		return $ret;
	}
	
	/*!
	* \brief warning
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function netstat()
	{
		
	}
	
	/*!
	* \brief warning
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	 function ping()
	{
		
	}
	
   /*!
	* \brief warning
	*
	* \note Main Program for Server Monitoring Toolkit 
	* base program Under CLI console .
	*
	* \retval void(0) running KepAlive(0)
	*
	*/
	function warning(&$ret = NULL)
   {
	   
	    app_func_struct($out);
		app_func_alloc($out->res);
		app_func_copy($out->res, 'event', 'warning');
		app_func_copy($out->res, 'vmid' , app_vendor_id());
		app_func_copy($out->res, 'time' , strtotime('now'));
		
	   /*! get All from storage Process */
	    app_func_copy($out->res, 'data' , 
			array( 'proc' => 'Pooling' )
	    );
		
	    if (app_func_copy($ret, $out->res))
		{
			app_func_free($out);
		}
		return $ret;
	}
}

?>