<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \brief <MAX_SESSION_TIMEOUT>
	MAX_SESSION_TIMEOUT = n x 60(s) 
 */
 if (!defined('MAX_SESSION_TIMEOUT')) define( 'MAX_SESSION_TIMEOUT', 1800);
 
/*! \retval app_func_malloc_start */  
 function app_func_malloc_start(&$smt) 
{
	if ( !isset($smt->__MALLOC__))
	{
		$smt->__MALLOC__ = array();
	}
	return 0;
 }
 
 /*! \retval app_func_malloc_register */ 
 function app_func_malloc_register(&$var = NULL, $val = "")
{
	if ( app_func_copy($var, $val) )
	{
		return $var;
	}
	return 0;
	 
 } 
 
/*! \retval app_func_malloc_write */ 
 function app_func_malloc_write(&$smt= NULL, &$SESSION =NULL, $_KEY_SESION= NULL, $_VAL_SESION = '') 
{
	/*! \remark make session local storage */
	if (!is_array($_KEY_SESION))
	{
		$_KEY_SESION = array($_KEY_SESION => $_VAL_SESION);
	}

	/*! \remark add to session */
	foreach ($_KEY_SESION as $SESION_APP_KEY => $SESION_APP_VAL ) 
    {
		if (!isset($smt->__MALLOC__[$SESSION]))
		{
			$smt->__MALLOC__[$SESSION]['SESSION_MAX_TIMEOUT'] = strtotime('now') + MAX_SESSION_TIMEOUT;
			$smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'] = array();
		} 
		
		if ( isset($smt->__MALLOC__[$SESSION]))
		{
			if( !isset($smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$SESION_APP_KEY]))
			{
				$smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$SESION_APP_KEY] = $SESION_APP_VAL; 	
			}
			
			if( isset($smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$SESION_APP_KEY]))
			{
				unset($smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$SESION_APP_KEY]);
				$smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$SESION_APP_KEY] = $SESION_APP_VAL; 
			}
		}
	}
	return 0;
 }
 
/*! \retval app_func_malloc_delete */ 
 function app_func_malloc_delete(&$smt= NULL, &$SESSION = NULL, $_KEY_SESION = NULL) 
{
	if (isset($smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$_KEY_SESION]))
	{
		unset($smt->__MALLOC__[$SESSION]['SESSION_MAX_STORAGE'][$_KEY_SESION]);
	}
	return 0;	
 }
 
/*! \retval app_func_malloc_destroy */ 
 function app_func_malloc_destroy(&$smt) 
{
	if	( is_array($smt->__MALLOC__))
	{ 
		app_func_free($smt->__MALLOC__);
	}
	return 0;
 }
 
 
 
