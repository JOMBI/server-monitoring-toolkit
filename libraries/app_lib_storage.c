<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/** \brief stg_create_stream */ 
 function stg_create_storage(&$smt = false, &$storage = 0)
{
	if (!$storage){
		return 0;
	}		 
	
	/*! \retval Create Storage file */
	$smt->CfgPathStgFile = $smt->CfgPathStg ."/storage.json";
	if (!isset($smt->CfgPathSource))
	{
		$smt->CfgPathSource = @fopen($smt->CfgPathStgFile, "a+");
		if (!$smt->CfgPathSource)
		{
			smt_log_error("Error 'fopen()' Storage. ", __FILE__, __LINE__);	
			return 0;
		}
	}
	
	/*! \remark save data to local object file */
	if (false === @ftruncate($smt->CfgPathSource, 0))
	{
		smt_log_error("Error 'ftruncate()' Storage. ", __FILE__, __LINE__);	
		return 0;
	}
	
	if(false === @fwrite($smt->CfgPathSource, json_encode($storage)))
	{
		smt_log_error("Error 'fwrite()' Storage. ", __FILE__, __LINE__);	
		return 0;
	} 
	
	/*! \retval return by reff */
	return $storage; 
} 
 
 
/** \brief stg_create_stream */ 
 function stg_create_stream(&$smt = false, &$ret = 0)
{	
    /*! \remark create Allocation */
	app_func_struct($out);
	
	/*! \remark default Static for Vendor */
	app_func_alloc($out->vendor);
	app_func_copy($out->vendor,'v_id' , app_vendor_id());
	app_func_copy($out->vendor,'v_name' , app_vendor_name());
	app_func_copy($out->vendor,'v_hostip' , app_vendor_host());
	app_func_copy($out->vendor,'v_hostname', app_vendor_hostname());
	app_func_copy($out->vendor,'v_kernel' , app_vendor_kernel()); 
	
	/*! \remark  default Static for Version */  
	
	app_func_alloc($out->version);
	app_func_copy($out->version,'osi', "");
	app_func_copy($out->version,'org', "");
	app_func_copy($out->version,'gcc', "");
	app_func_copy($out->version,'rev', "");
	
	/*! \remark default Static 
	for Uptime */
	
	app_func_alloc($out->uptime);
	app_func_copy($out->uptime,'day' , "");
	app_func_copy($out->uptime,'hour', "");
	app_func_copy($out->uptime,'min' , "");
	app_func_copy($out->uptime,'sec' , "");
	
	/*! \remark default Static for 
	CPU Info */
	
	app_func_alloc($out->cpuinfo);
	app_func_copy($out->cpuinfo,'vendor_id'		  , '');
	app_func_copy($out->cpuinfo,'cpu_family'	  , '');
	app_func_copy($out->cpuinfo,'model'			  , '');
	app_func_copy($out->cpuinfo,'model_name'	  , '');
	app_func_copy($out->cpuinfo,'stepping'		  , '');
	app_func_copy($out->cpuinfo,'cpu_mhz'		  , '');
	app_func_copy($out->cpuinfo,'cache_size'	  , '');
	app_func_copy($out->cpuinfo,'fpu'			  , '');
	app_func_copy($out->cpuinfo,'fpu_exception'	  , '');
	app_func_copy($out->cpuinfo,'cpuid_level'	  , '');
	app_func_copy($out->cpuinfo,'wp'			  , '');
	app_func_copy($out->cpuinfo,'bogomips'		  , '');
	app_func_copy($out->cpuinfo,'clflush_size'	  , '');
	app_func_copy($out->cpuinfo,'cache_alignment' , '');
	app_func_copy($out->cpuinfo,'address_sizes'	  , '');
	app_func_copy($out->cpuinfo,'power_management', '');
	
	/*! \remark default Dynamic 
	for Uptime */ 
	
	app_func_alloc($out->average);
	app_func_alloc($out->memory);
	app_func_alloc($out->hardisk);
	app_func_alloc($out->network);
	app_func_alloc($out->service);
	 
	/*! \remark  data for 
	storage */
	
	app_func_alloc($out->storage);
	app_func_copy($out->storage,'smt_vm_vendor' ,$out->vendor);
	app_func_copy($out->storage,'smt_vm_version',$out->version);
	app_func_copy($out->storage,'smt_vm_uptime' ,$out->uptime);
	app_func_copy($out->storage,'smt_vm_average',$out->average);
	app_func_copy($out->storage,'smt_vm_memory' ,$out->memory);
	app_func_copy($out->storage,'smt_vm_hardisk',$out->hardisk);
	app_func_copy($out->storage,'smt_vm_network',$out->network);
	app_func_copy($out->storage,'smt_vm_service',$out->service);
	app_func_copy($out->storage,'smt_vm_cpuinfo',$out->cpuinfo);
	
	
	/*! \remark  put header 
	Storage */
	app_func_copy($smt->AppStorage,'smt_vm_id'    ,app_vendor_id());
	app_func_copy($smt->AppStorage,'smt_vm_name'  ,app_vendor_name());
	app_func_copy($smt->AppStorage,'smt_vm_haddr' ,app_vendor_host());
	app_func_copy($smt->AppStorage,'smt_vm_hname' ,app_vendor_hostname());
	app_func_copy($smt->AppStorage,'smt_vm_kernel',app_vendor_kernel());
	app_func_copy($smt->AppStorage,'smt_vm_update',app_vendor_update()); 
	
	
	/*! \remark  Add Body Storage */
	
	app_func_copy($smt->AppStorage, 'vm_data', $out->storage);
	if (app_func_copy($ret, $smt->AppStorage))
	{
		app_func_free($out);
	}
	return $ret;
}


/** \brief stg_read_session */
 function stg_reader_stream(&$ret= NULL, $f= NULL)
{
	Global $smt;
	
	/*! \retval for this  untuk data dynamic di perlukan check process argc */
	if (app_func_struct($out))
	{
		$out->ob_val = func_num_args();
		$out->ob_ret = func_get_args(); 
	}
	
	/*! \retval local data absolute */
	app_func_copy($out->ob_stg, 0); 
	while ($out->ob_val)
	{
		// for Read Level :1 
		if ($out->ob_val == 2)
		{
			$out->ob_key = $out->ob_ret[1];
			if (!isset($smt->AppStorage['vm_data'][$out->ob_key]))
			{
				app_func_free($out);
				return 0;
			}
			
			// dump data to this sections 
			app_func_copy( $out->ob_stg, 
				$smt->AppStorage['vm_data'][$out->ob_key] 
			);
			
			app_func_copy($out->ob_stg, 'item', $out->ob_key);
			if (app_func_copy($ret, app_func_typdef($out->ob_stg) ))
			{
				app_func_free($out);
			}    
			
			// break stop;
			break;
		} 
		
		// for Read Level : 2
		if ($out->ob_val == 3)
		{
			$out->ob_key = $out->ob_ret[1];
			$out->ob_has = $out->ob_ret[2];
			
			$out->ob_arr_lev1 = $smt->AppStorage['vm_data'][$out->ob_key];
			if (!is_array($out->ob_arr_lev1))
			{
				app_func_free($out);
				break;
			}
			
			
			/*! \retval check array(s) */
			$out->ob_arr_lev2 = ( isset($smt->AppStorage['vm_data'][$out->ob_key][$out->ob_has]) ?
								  $smt->AppStorage['vm_data'][$out->ob_key][$out->ob_has] : 0);
				
			if (!$out->ob_arr_lev2)
			{
				$out->ob_arr_lev2 = $smt->AppStorage['vm_data'][$out->ob_key][$out->ob_has] = array();
			}
			
			
			if (is_array($out->ob_arr_lev2) && sizeof($out->ob_arr_lev2)<1)
			{
				app_func_alloc($out->ob_stg);
				app_func_copy($out->ob_stg, 'value', 'dynamic');
				app_func_copy($out->ob_stg, 'item' , $out->ob_key);
				app_func_copy($out->ob_stg, 'hash' , $out->ob_has);
				
				if (app_func_copy($ret, app_func_typdef($out->ob_stg) ))
				{
					app_func_free($out);
				}
				break;
			}
			
			else if (is_array($out->ob_arr_lev2) && sizeof($out->ob_arr_lev2)>0 )
			{
				app_func_copy($out->ob_stg, $out->ob_arr_lev2);
				app_func_copy($out->ob_stg, 'item', $out->ob_key);
				app_func_copy($out->ob_stg, 'hash', $out->ob_has);
				
				if (app_func_copy($ret, app_func_typdef($out->ob_stg) ))
				{
					app_func_free($out);
				}
				
				break;
			} 
		} 
		
		// default Break;
		break;
	}
	
	return $ret; 
}


/** \brief stg_update_item */
 function stg_update_item($p0 = '', $p1 = '', $p2 = '', $p3 = '' )
{
	Global $smt; 
	
	app_func_copy($next, 0);
	if(func_num_args() == 3)
	{
		if (!isset($smt->AppStorage['vm_data'][$p0]) )
		{
			return 0;
		}
		
		/*! \retval update this */
		if ($smt->AppStorage['vm_data'][$p0][$p1] = $p2 )
		{
			app_func_next($nex);
		}
	}
	
	else if (func_num_args() == 4)
	{
		if (!isset($smt->AppStorage['vm_data'][$p0]) )
		{
			return 0;
		}	
		
		if ($smt->AppStorage['vm_data'][$p0][$p1][$p2] = $p3)
		{
			app_func_next($next);
		}
	}
	
	return $next; 
}
 
/** \brief stg_update_version */
 function stg_update_version(&$s= false, &$ret = NULL)
{
	app_func_copy($f, 0);
	app_func_copy($c, 0);
	if (!stg_reader_stream($f, 'smt_vm_version'))
	{
		app_func_free($f);
		return 0;
	}
	 
	/*! \retval update data from base */ 
	stg_update_item($f->item,'osi',$s->osi);
	stg_update_item($f->item,'org',$s->org);
	stg_update_item($f->item,'gcc',$s->gcc);
	stg_update_item($f->item,'rev',$s->rev); 
	
	if ( stg_reader_stream($c, $f->item) )
	{
		app_func_copy($ret, $c);	
	} 
	
	/*! \retval clean Memory Usage */
	app_func_free($f);
	app_func_free($c);
	return $ret;
}
 
/** \brief stg_update_vendor */
 function stg_update_vendor(&$s= false, &$ret = NULL)
{
	app_func_copy($f,0);
	app_func_copy($c,0);
	if (!stg_reader_stream($f, 'smt_vm_vendor'))
	{
		app_func_free($f);
		return 0;
	}
	
	/*! \retval update data from base */ 
	stg_update_item($f->item,'v_id',$s->v_id);
	stg_update_item($f->item,'v_name',$s->v_name);
	stg_update_item($f->item,'v_hostip',$s->v_hostip);
	stg_update_item($f->item,'v_hostname',$s->v_hostname);
	stg_update_item($f->item,'v_kernel',$s->v_kernel);  
	
	if (stg_reader_stream($c, $f->item))
	{
		app_func_copy($ret, $c);	
	} 
	
	/*! \retval clean Memory Usage */
	app_func_free($f);
	app_func_free($c);
	return $ret;
}

/** \brief stg_update_service */
 function stg_update_service($h= NULL, $s= NULL, &$ret = NULL)
{
	app_func_struct($out); 
	app_func_copy($out->h, $h);
	app_func_copy($out->k, 'smt_vm_service');
	
	/*! \retval clean before Assign */
	app_func_free($out->f); 
	app_func_free($out->c);
	
	if ( !stg_reader_stream($out->f, $out->k, $out->h) )
	{
		app_func_free($out);
		return 0;
	}
	
	if (isset($out->f->value)) {
		
		stg_update_item($out->f->item, $out->f->hash, 'name' , $s->name);
		stg_update_item($out->f->item, $out->f->hash, 'pid'  , $s->pid);
		stg_update_item($out->f->item, $out->f->hash, 'state', $s->state); 
		
		if (stg_reader_stream( $out->c, $out->k, $out->h ))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		} 
		
	} else if (!isset($out->f->value)) {
		
		stg_update_item($out->f->item, $out->f->hash, 'name' , $s->name);
		stg_update_item($out->f->item, $out->f->hash, 'pid'  , $s->pid);
		stg_update_item($out->f->item, $out->f->hash, 'state', $s->state); 
		
		if (stg_reader_stream( $out->c, $out->k, $out->h ))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		} 
	} 
	
	app_func_free($out);
	return $ret;
}
 
/** \brief stg_update_hardisk */ 
 function stg_update_hardisk($h= NULL, $s= NULL, &$ret = NULL)
{
	app_func_struct($out);  
	app_func_copy($out->s, $s);
	app_func_copy($out->h, $h);
	app_func_copy($out->k, 'smt_vm_hardisk'); 
	
	/*! clean before Assign */
	app_func_free($out->c);
	app_func_free($out->f);
	
	
	/*! \remark read data is still exist */
	if (!stg_reader_stream($out->f, $out->k, $out->h))
	{
		app_func_free($out);
		return 0;
	} 
	
	/*! \remark Goto Break Action  */
	while (1)
	{
		/*! \remark start empty data */
		if (isset($out->f->value))
		{
			foreach ($out->s as $j => $r) {
				stg_update_item($out->k, $out->h, $j, $r);
			}
			
			if (stg_reader_stream($out->c, $out->k, $out->h))
			{
				if (app_func_copy($ret, $out->c)){	
					app_func_free($out);
				}
			}
			break;
		} 
		
		/*! \remark data still exist then will update content 
			data session from Thread worker */
			
		foreach ($out->s as $j => $r) {
			stg_update_item($out->k, $out->h, $j, $r);
		}
		
		if (stg_reader_stream($out->c, $out->k, $out->h))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		}
		break;
	} 
	
	app_func_free($out);
	return $ret;
} 
 
/** \brief stg_update_average */  
 function stg_update_average($h= NULL, $s= NULL, &$ret = NULL)
{
	app_func_struct($out);  
	app_func_copy($out->s, $s);
	app_func_copy($out->h, $h);
	app_func_copy($out->k, 'smt_vm_average'); 
	
	/*! clean before Assign */
	app_func_free($out->c);
	app_func_free($out->f);
	
	
	/*! \remark read data is still exist */
	if (!stg_reader_stream($out->f, $out->k, $out->h))
	{
		app_func_free($out);
		return 0;
	} 
	
	/*! \remark Goto Break Action  */
	while (1)
	{
		/*! \remark start empty data */
		if (isset($out->f->value))
		{
			foreach ($out->s as $j => $r) {
				stg_update_item($out->k, $out->h, $j, $r);
			}
			
			if (stg_reader_stream($out->c, $out->k, $out->h))
			{
				if (app_func_copy($ret, $out->c)){	
					app_func_free($out);
				}
			}
			break;
		} 
		
		/*! \remark data still exist then will update content 
			data session from Thread worker */
			
		foreach ($out->s as $j => $r) {
			stg_update_item($out->k, $out->h, $j, $r);
		}
		
		if (stg_reader_stream($out->c, $out->k, $out->h))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		}
		break;
	} 
	
	app_func_free($out);
	return $ret;
} 
 
 
/** \brief stg_update_memory */    
 function stg_update_memory($h= NULL, $s= NULL, &$ret = NULL)
{
	app_func_struct($out); 
	
	app_func_copy($out->s, $s);
	app_func_copy($out->h, $h);
	app_func_copy($out->k, 'smt_vm_memory'); 
	
	/*! clean before Assign */
	app_func_free($out->c);
	app_func_free($out->f);
	
	
	/*! \remark read data is still exist */
	if (!stg_reader_stream($out->f, $out->k, $out->h))
	{
		app_func_free($out);
		return 0;
	} 
	
	/*! \remark Goto Break Action  */
	while (1)
	{
		/*! \remark start empty data */
		if (isset($out->f->value))
		{
			foreach ($out->s as $j => $r) {
				stg_update_item($out->k, $out->h, $j, $r);
			}
			
			if (stg_reader_stream($out->c, $out->k, $out->h))
			{
				if (app_func_copy($ret, $out->c)){	
					app_func_free($out);
				}
			}
			break;
		} 
		
		/*! \remark data still exist then will update content 
			data session from Thread worker */
			
		foreach ($out->s as $j => $r) {
			stg_update_item($out->k, $out->h, $j, $r);
		}
		
		if (stg_reader_stream($out->c, $out->k, $out->h))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		}
		break;
	} 
	
	app_func_free($out);
	return $ret;
} 
 
 
/** \brief stg_update_cpuinfo */   
 function stg_update_cpuinfo(&$s= false, &$ret = NULL)
{
	app_func_copy($f, 0);
	app_func_copy($c, 0);
	if (!stg_reader_stream($f, 'smt_vm_cpuinfo'))
	{
		app_func_free($f);
		return 0;
	}
	
	/*! \retval update data from base */
	stg_update_item($f->item,'vendor_id',$s->vendor_id);
	stg_update_item($f->item,'cpu_family',$s->cpu_family);
	stg_update_item($f->item,'model',$s->model);
	stg_update_item($f->item,'model_name',$s->model_name);
	stg_update_item($f->item,'stepping',$s->stepping);
	stg_update_item($f->item,'cpu_mhz',$s->cpu_mhz);
	stg_update_item($f->item,'cache_size',$s->cache_size);
	stg_update_item($f->item,'fpu',$s->fpu);
	stg_update_item($f->item,'fpu_exception',$s->fpu_exception);
	stg_update_item($f->item,'cpuid_level',$s->cpuid_level);
	stg_update_item($f->item,'wp',$s->wp);
	stg_update_item($f->item,'bogomips',$s->bogomips);
	stg_update_item($f->item,'clflush_size',$s->clflush_size);
	stg_update_item($f->item,'cache_alignment',$s->cache_alignment);
	stg_update_item($f->item,'address_sizes',$s->address_sizes);
	stg_update_item($f->item,'power_management',$s->power_management);
	
	if ( stg_reader_stream($c, $f->item) )
	{
		app_func_copy($ret, $c);	
	} 
	
	/*! \retval clean Memory Usage */
	app_func_free($f);
	app_func_free($c);
	return $ret;
} 


/** \brief stg_update_uptime */    
 function stg_update_uptime(&$s = false, &$ret = NULL)
{
	app_func_copy($f, NULL);
	app_func_free($c, NULL);
	
	if (!stg_reader_stream($f,'smt_vm_uptime') )
	{
		app_func_free($f);
		return 0;
	}
	 
	stg_update_item($f->item, 'day', $s->day);
	stg_update_item($f->item, 'hour',$s->hour);
	stg_update_item($f->item, 'min', $s->min);
	stg_update_item($f->item, 'sec', $s->sec);
	
	if (stg_reader_stream($c, $f->item))
	{
		app_func_copy($ret, $c);	
		
	}
	
	/*! \retval clean Memory Usage */
	app_func_free($c);
	app_func_free($f);
	
	return $ret;
} 

 
/** \brief stg_update_network */     
 function stg_update_network($h= NULL, $s= NULL, &$ret = NULL)
{
	app_func_struct($out);  
	app_func_copy($out->s, $s);
	app_func_copy($out->h, $h);
	app_func_copy($out->k, 'smt_vm_network'); 
	
	/*! clean before Assign */
	app_func_free($out->c);
	app_func_free($out->f);
	
	
	/*! \remark read data is still exist */
	if (!stg_reader_stream($out->f, $out->k, $out->h))
	{
		app_func_free($out);
		return 0;
	} 
	
	/*! \remark Goto Break Action  */
	while (1)
	{
		/*! \remark start empty data */
		if (isset($out->f->value))
		{
			foreach ($out->s as $j => $r) {
				stg_update_item($out->k, $out->h, $j, $r);
			}
			
			if (stg_reader_stream($out->c, $out->k, $out->h))
			{
				if (app_func_copy($ret, $out->c)){	
					app_func_free($out);
				}
			}
			break;
		} 
		
		/*! \remark data still exist then will update content 
			data session from Thread worker */
			
		foreach ($out->s as $j => $r) {
			stg_update_item($out->k, $out->h, $j, $r);
		}
		
		if (stg_reader_stream($out->c, $out->k, $out->h))
		{
			if (app_func_copy($ret, $out->c)){	
				app_func_free($out);
			}
		}
		break;
	} 
	
	app_func_free($out);
	return $ret;
} 
 