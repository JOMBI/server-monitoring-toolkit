<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \remark 
 
 void <\ app_signal_register >
 void <\ app_signal_unregister >
 void <\ app_signal_resolver >
 void <\ app_signal_worker >
 
*/
 
/*! \remark app_signal_register */ 
 function app_signal_register(&$smt= NULL, $pid= 0)
{
	if ($pid) 
	{
		$smt->AppThread[$pid] = $pid;
	}
	return $pid;
}

/*! \remark app_signal_unregister */ 
 function app_signal_unregister(&$smt= NULL, $pid= 0 )
{
	unset($smt->AppThread[$pid]);
	return 0;
}

/*! \remark app_signal_resolver */ 
 function app_signal_resolver(&$smt = NULL)
{
	return 0;
}

/*! \remark app_signal_worker */ 
 function app_signal_worker(&$smt = NULL, $event = NULL, $param = NULL, &$ret = 0 )
{
	/*! \remark If Method Not Exist return false */
	if ( !function_exists($event) )
	{
		app_func_free($smt);
		return 0;
	}
	/*! \remark Next with Method is Exist */
	else if ( function_exists($event) )
	{
		/*! \remark If Parameter Is Array */
		app_func_copy($retval, 0); 
		app_func_sizeof($param, $retval);
		
		if ($retval)
		{
			call_user_func_array( $event, $param);
		}
		else if ($retval)
		{
			call_user_func($event, $param);
		}	 
	}
	
	/*! \retval Assign PID */
	$smt->pid = getmypid();
	if (app_func_copy($ret, $smt->pid))
	{
		return $ret;
	}
	return 0;
}