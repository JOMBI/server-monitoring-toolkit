<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \brief app_vendor_id */
 function app_vendor_id(&$ret = 0) 
{
	Global $smt; 
	
	if(isset($smt->VendorId))
	{
		app_func_copy($ret, $smt->VendorId);
	}
	return $ret;
}
 
/*! \brief app_vendor_name */
 function app_vendor_name(&$ret = NULL) 
{
	Global $smt;

	if (isset($smt->VendorName))
	{	
		app_func_copy($ret, $smt->VendorName);
	}
	return $ret;
}

/*! \brief app_vendor_host */
 function app_vendor_host(&$ret = NULL) 
{
	Global $smt;

	if (isset($smt->VendorHost))
	{	
		app_func_copy($ret, $smt->VendorHost);
	}
	return $ret;
}
 
/*! \brief app_vendor_hostname */
 function app_vendor_hostname(&$ret = NULL) 
{
	Global $smt; 
	
	if (!isset($smt->VendorHostname))
	{
		if (is_null($smt->VendorHostname) && $smt->AppProc->hostname($host))
		{
			app_func_copy($smt->VendorHostname,  $host);
			app_func_free($host);
		}
	}
	
	// next process 
	if (app_func_copy($ret, $smt->VendorHostname) )
	{
		return $ret;
	}
	
	return 0;
}
 
/*! \brief app_vendor_kernel */ 
 function app_vendor_kernel(&$ret = NULL) 
{
	Global $smt; 
	
	if (!isset($smt->VendorKernel))
	{
		if (is_null($smt->VendorKernel) && $smt->AppProc->kernel($host))
		{
			app_func_copy($smt->VendorKernel,  $host);
			app_func_free($host);
		}
	}
	
	// next process 
	if (app_func_copy($ret, $smt->VendorKernel) )
	{
		return $ret;
	}
	
	return 0;
}


/*! \brief app_vendor_update */ 
 function app_vendor_update(&$ret = NULL) 
{
	if (app_func_copy($ret, strtotime('now')))
	{
		return $ret;
	}
	return 0;
 }
 
 
 
 