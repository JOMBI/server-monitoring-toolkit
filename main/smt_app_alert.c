<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */


/*! \retval smtAppMainAlert */
 function smtAppMainAlert(&$smt= NULL)
{
	
	app_func_alloc($smt->smtAppAlert);
	app_func_copy($smt->smtAppAlert, 'smtAverage', 'smtAppAlertAverage' );
	app_func_copy($smt->smtAppAlert, 'smtService', 'smtAppAlertService' );
	app_func_copy($smt->smtAppAlert, 'smtHardisk', 'smtAppAlertHardisk' );
	app_func_copy($smt->smtAppAlert, 'smtMemory' , 'smtAppAlertMemory' );
	
	/*! \remark write thread base on smtAppMainWorker */ 
	foreach ($smt->smtAppAlert as $modul => $event)
	{
		/*! \remark check function on dir "alerts" */
		if ( !function_exists($event) ) 
		{
			smt_log_error(sprintf("Alert '%s' Error.", $event), __FILE__, __LINE__ );
			continue;
		}
		
		/*! \remark function alerts is exist on here */
		
		/*! 
		 * \remark Masukan Thread yang Sedang di Monitoring, Untuk Tujuan 
		 *  Memenage Thread Yang sedang di process atau untuk Mencak Apakah Thread jalan Atau Sudah Mati 
		 *  Jika Mati Bisa saja Thread Akan di Hidupkan Kembali , Atau tidak sama sekali 
		 */ 
		 
		app_func_free($pid);
		if (smtAppCreateThread( $smt, $event, array($smt), $pid) )
		{
			$smt->AppAlert[$pid]['modul']  = $modul;
			$smt->AppAlert[$pid]['event']  = $event;
			$smt->AppAlert[$pid]['status'] = $pid;
			$smt->AppAlert[$pid]['time']   = app_func_time(); 
		} 
	}
	
	return 0;
}


/*! \retval smtAppMonitorAlert */
 function smtAppMonitorAlert(&$smt= NULL)
{
	
	/*! \retval Invalid handler */
	if ( !is_array($smt->AppAlert) )
	{
		return 0;
	}
	
	/*! \retval Next for Iterator process */
	foreach ($smt->AppAlert as $pid => $work)
	{
		if (smtAppRetvalAlert($pid, $modul, $event, $status, $time))
		{
			if (!$status &&((time() - $time) >= 2)) 
			{   
				smt_log_info(sprintf("Stop Alert '%s'", $event), 
				__FILE__, __LINE__);
				
				/*! \retval start smtAppStartAlert */
				/*! \retval deleted smtAppStartAlert */		
				if ( smtAppStartAlert($smt, $modul, $event, $newpid) )
				{
					smtAppDeleteAlert($pid); 
				}
			}
		}
	}
	
	return 0;
}
/*! \retval smtAppRetvalAlert */
 function smtAppRetvalAlert($pid= 0, &$modul = NULL, &$event = NULL, &$status= 0, &$time = 0)
{
	Global $smt;
	
	/*! \retval 'app_func_copy' */
	app_func_copy($modul , NULL);
	app_func_copy($event , NULL);
	app_func_copy($status, 0);
	app_func_free($time  , 0);
	
	
	/*! \retval tested process */
	if ( !isset($smt->AppAlert[$pid]) )
		return 0;
	else if ( isset($smt->AppAlert[$pid]) )
	{
		app_func_copy($modul , $smt->AppAlert[$pid]['modul']);
		app_func_copy($event , $smt->AppAlert[$pid]['event']);
		app_func_copy($status, $smt->AppAlert[$pid]['status']);
		app_func_copy($time	 , $smt->AppAlert[$pid]['time']); 
	}
	
	/*! \retval finaly integer */
	return $pid;
}

/*! \retval smtAppDeleteAlert */
 function smtAppDeleteAlert($pid= 0)
{
	Global $smt; 
	
	/*! \remark for process apps */
	app_func_copy($retval, 0);
	if ( isset($smt->AppAlert[$pid]) )
	{ 
		unset($smt->AppAlert[$pid]);
		app_func_next($retval);
	}
	return $retval;
}

/*! \retval smtAppStrtAlert */
 function smtAppStartAlert(&$smt= NULL, $modul= NULL, $event= NULL, &$pid= 0 )
{
   /*! \retval app_func_struct */
	app_func_struct($out);
	app_func_copy($out->modul, $modul);
	app_func_copy($out->event, $event); 
	
	
	/*! \retval 'app_func_time' */
	app_func_copy( $out->time, 
		app_func_time()
	); 
	
	/*! \retval function_exists */
	if (function_exists($out->event)) 
	{
		app_func_free($out->pid);
		if (smtAppCreateThread($smt, $out->event, array($smt), $out->pid))
		{
			$smt->AppAlert[$out->pid]['modul']  = $out->modul;
			$smt->AppAlert[$out->pid]['event']  = $out->event;
			$smt->AppAlert[$out->pid]['status'] = $out->pid; 
			$smt->AppAlert[$out->pid]['time']   = $out->time;
			
			/*! copy PID to retval new process */
			if ( app_func_copy($pid, $out->pid) )
			{
				app_func_free($out);
				return $pid;
			}
		}
	}
	
	/*! \retval return is === false */
	return 0;
}  
