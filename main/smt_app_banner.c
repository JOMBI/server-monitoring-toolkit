<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 function smtAppMainBanner(&$smt = NULL)
{
	/*! \note verbose false then 
	will hide if on Production  arg "v" */	 
	app_func_struct($app);
	app_func_copy($app->scr, "\n");  
	
	if (app_func_sizeof($app->scr))
	{
		app_func_output($app->scr, sprintf("%s (%s) - Version %s\n",$smt->AppProgName,$smt->AppProgUid, $smt->AppProgVer));
		app_func_output($app->scr, sprintf("%s.\n",$smt->AppProgCopy));
		app_func_output($app->scr, sprintf("Auth < %s >\n", $smt->AppProgAuth));
		app_func_output($app->scr, sprintf("Build on %s\n", $smt->AppProgBuild));
		app_func_output($app->scr, "\n"); 
		
		/*! \remark show data to user */
		if ( !smt_log_scr($smt, $app->scr) )
		{
			app_func_free($app);
		} 
	}
	
	return 0;
}