<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 
/*! \retval <smtAppEventChannel> */

 function smtAppEventChannel(&$smt= NULL, $buf= NULL, $events= NULL)
{
	/*! \retval <smtAppEventHandler> */
	/*! call class EventHandler.method */ 
	
	 foreach($events as $c => $event)
	{
		/*! \remark if fail open class */	
		
		if ( !class_exists($c) ){
			smt_log_error(sprintf("Open Class %s Fail.", $c), __FILE__, __LINE__ );
			continue;
		}
		
		if ( !isset($smt->Modul->$c) ){
			app_func_copy( $smt->Modul, $c, new $c() );
		}
		
		/*! \remark if Modul of class stil exist */
		if ( is_object($smt->Modul->$c) )
		{
			/*! \remark get all method by 'method_exists'   */
			/*! \remark for validation process EventHandler */
			
			$populates = get_class_methods($smt->Modul->$c); 
			if ( !in_array('EventHandler', $populates))
			{
				smt_log_error( sprintf("Handler Class.%s->%s()", $c, $event) , __FILE__, __LINE__ );
			}
			else if ( in_array('EventHandler', $populates))
			{
				/*! \remark sent message Modul On here */
				$smt->Modul->$c->EventHandler($smt, strtolower($event), $buf); 
				app_func_free($buf);
			}
		}  
	}
	
	/*! \retval return data is false */ 
	return 0;
} 


/*! \retval <smtAppEventAlerting> */
 function smtAppEventAlerting(&$smt= NULL, $buf= NULL, $events= NULL)
{
	/*! \retval <smtAppEventHandler> */
	/*! call class EventHandler.method */ 
	 foreach($events as $c => $event)
	{
		/*! \remark if fail open class */	
		
		if (!class_exists($c))
		{
			smt_log_error(sprintf("Open Class %s Fail.", $c), __FILE__, __LINE__ );
			continue;
		}
		
		if (!isset($smt->Modul->$c))
		{
			app_func_copy( $smt->Modul, $c, new $c() );
		}
		
		/*! \remark if Modul of class stil exist */
		if (is_object($smt->Modul->$c))
		{
			/*! \remark get all method by 'method_exists'   */
			/*! \remark for validation process EventAlerting */
			
			$populates = get_class_methods($smt->Modul->$c);
			
			if (!in_array('EventAlerting', $populates)) {
				smt_log_error( sprintf("Handler Class.%s->%s()", $c, $event) , __FILE__, __LINE__ );
			}
			else if (in_array('EventAlerting', $populates)) {
				/*! \remark sent message Modul On here */
				$smt->Modul->$c->EventAlerting($smt, strtolower($event), $buf); 
				app_func_free($buf);
			}
		}  
	}
	
	/*! \retval return data is false */ 
	return 0;
} 