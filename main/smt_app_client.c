<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */


/*!
 * \brief  smtAppClientHandler 
 *
 * \remark fungsi yang betugas untuk Mengatur Route Client yang connect ke Server 
 *  memastikan apakah client connect melalui Port WebHook , Atau Connect melalui port 
 *  Khusus untuk Komunikasi 2 Arah , 
 *
 * \param  	Global Object
 *
 * \retval 	int(0) 
 *
 */
 
 function smtAppClientHandler(&$smt = NULL)
{
	/*! \remark check data jika bukan array exit saja */
	if (!app_func_sizeof($smt->AppClient)){
		return 0;
	}
	
	foreach ($smt->AppClient as $i => $f)
	{
		if (app_func_retval($f, $s))
		{
			/*! \remark handler: Jika Connect via socket HTTP Atau Web Hook */
			if (!strcmp($s->type, 'pub') &&(app_func_search($s->id, $smt->AppSocket)))
			{
				smtSockTcpRead($s->sock, $s->nread, 1024);
				if (($s->nread ===  false ) OR ( $s->nread === '' ) OR (strlen($s->nread) < 1)) 
				{
					smt_log_app(sprintf("%s:%06d - Session #%d Client Disconnected .", $s->host, $s->port, $s->id));
					if (app_webhook_client_sighup($smt, $s)){
						app_func_free($s);
					}
					break;
				}
				
			   /**
				* \remark Jika data Ada Isinya maka teruskan Ke Porcess handler selanjutnya 
				* Process Manipulasi dan response akan di lakukan pada modul webhook 
				*
				*/
				else 
				{
				   /*! 
					* \remark Untuk Paralel Process dengan Aksess User Yang Lebih dari satu client sebaiknya 
					* process client webhook di Buat thread sendiri tujuannya agar tidak membuat process 
					* harus Mengantri , Atau Menerapkan Multi thread 
					*
					*/
					
					 if ( app_webhook_client_thread($smt, $s) )
					{
						if ( smtAppClientCollected($smt,$s) )
						{
							app_func_free($s->nread);
						}
					} 
					
					/**
						<DOCUMENTED>
							if (app_webhook_client_handler($smt, $s)) { 
								app_func_free($s->nread);
							}
						</DOCUMENTED>
					*/ 
				}
			}
		}
	}
	
	return 0;
}



/*!
 * \brief  	smtAppClientCollected
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppClientCollected(&$smt= NULL, &$s= NULL) 
{
	if (isset($smt->AppClient[$s->id]))
	{
	   /*! 
		* \remark jika PID dari client belum ada tambahkan ke sini,
		*  Untuk slenjutnya akan di monitor Oleh server WebHook
		*
		*/
		if ( !$smt->AppClient[$s->id]['pid'] )
		{
			$smt->AppClient[$s->id]['pid'] = $s->pid;	
		}
	}
	
	return $s->id;
}

/*!
 * \brief  	smtAppClientDeleted
 * \remark  Destruct / Destroy Client Session from Iterator Process 
 *
 * \param  <smt> Global Parameters  	  	 
 * \param  <s>	Object Client Session  
 *
 * \retval 	int(0) 
 *
 */
 function smtAppClientDeleted( &$smt= NULL, &$s= NULL) 
{
	if (isset($smt->AppClient[$s->id])) 
	{
		unset($smt->AppClient[$s->id]);	
	}
	return $s->id;
 }
 
 
/*!
 * \brief  	smtAppMainClient
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppClientIterator(&$smt = NULL)
{
	if (app_func_sizeof($smt->AppClient)<1)
	{
		return 0;	
	}
	
	foreach ($smt->AppClient as $j => $f)
	{ 
		if ( app_func_retval($f, $s) )
		{
			app_func_copy($smt->AppSocket, $s->id, $s->sock);  
			app_func_free($s);
		} 
	} 
	
	return 0;
}


