<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 


/*!
 * smtAppMainConfig
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 */
 function smtAppMainConfig(&$smt = NULL)
{
	/*! \def App config */
	$smt->AppProgVer   = '1.1.0';									 // Version Program
	$smt->AppProgUid   = 'SMT';										 // UID program 	
	$smt->AppProgName  = 'Server Monitoring Toolkit';			 	 // Name of Program
	$smt->AppProgTitle = 'SMT - Server Monitoring Toolkit';			 // Title Description
	$smt->AppProgCopy  = '(C) 2021 - Peranti Digital Solusindo, PT'; // Copy Right 	
	$smt->AppProgAuth  = 'PDS Developer Team';						 // Program Auth 	
	$smt->AppProgLink  = 'http://www.perantidigital.co.id';			 // Program Auth 	
	$smt->AppProgBuild = '2021-08-26';								 // Program Build 	
	$smt->AppProgPid   = 0;											 // PID program 
	$smt->AppProgCfg   = 0;										 	 // Config file 
	$smt->AppProgLck   = 0;										 	 // Lock file 
	$smt->AppProgRun   = 0;										     // Run Or Not 
	$smt->AppProgScr   = 1;
	$smt->AppProgVar   = NULL;
	$smt->AppProgSpl   = array();									 // Auto Loader  
	
	/*! \def App Config  Sokcet */
	$smt->AppStorage   = array();									 // All Information set on this stream 		
	$smt->AppService   = array();									 // All Service To Monitoring 
	$smt->AppWorker    = array();									 // All Worker PID 
	$smt->AppThread    = array();									 // All PID register 
	$smt->AppSignal    = 0;											 // Signal Not Work ?
	$smt->AppClient    = array();									 // socket Client Udp:port, Tcp:sid Pub:sid
	$smt->AppSocket    = array();									 // socket Master 
	$smt->AppTimout    = 30;										 // Timeout connected Sokcet 
	$smt->AppMaxCon    = 1024;										 // Max Client Connect 
	$smt->AppProc	   = NULL;										 // Open Worker Class Register Proc
	
	
	/*! \def TCP config */
	$smt->TcpHost      = NULL; 
	$smt->TcpPort      = NULL;
	$smt->TcpSign      = NULL;
	$smt->TcpBuff      = 65000;
	$smt->TcpLink      = -1;
	
	/*! \def Udp config */
	$smt->UdpHost      = NULL; 
	$smt->UdpPort      = NULL;
	$smt->UdpSign      = NULL;
	$smt->UdpBuff      = 65000;
	$smt->UdpLink      = -1;
	
	/*! \def Pub config */
	$smt->PubHost      = NULL; 
	$smt->PubPort      = NULL;
	$smt->PubSign      = NULL;
	$smt->PubBuff      = 65000;
	$smt->PubLink      = -1;
	
	/*! \def Config */
	$smt->CfgPathApp   = SMT_APP . '/config';	
	$smt->CfgPathLib   = SMT_APP . '/libraries';						// Global Config for App 
	$smt->CfgPathPub   = SMT_APP . '/public';							// Public Path Directory 
	$smt->CfgPathStg   = SMT_APP . '/storage';                       	// Storage for Public Access 	 
	$smt->CfgPathWkr   = SMT_APP . '/worker';							// Worker Program Service 
	$smt->CfgPathAlt   = SMT_APP . '/alert';							// Alert Program Service
	$smt->CfgPathSck   = SMT_APP . '/socket';							// Soket Libraries 	 
	$smt->CfgPathEvt   = SMT_APP . '/events';							// Modul event Interface 
	$smt->CfgPathHok   = SMT_APP . '/whook';							// Web Hook Embeded Socket
	$smt->CfgPathMod   = SMT_APP . '/modul';							// Channel for sent Message Interface
	$smt->CfgPathCli   = SMT_APP . '/console/cli';						// Channel for sent Message Interface
	
	/*! \def config for Loger */
	$smt->LogPathVerb  = 0;
	$smt->LogPathLink  = -1;  
	$smt->LogPathCfg   = '/var/log/smt';
	$smt->LogPathDts   = date('Ymd');
	$smt->LogPathSize  = 1024;
	$smt->LogPathUid   = 'smt';
	
	
	/*! \def for channels */
	$smt->CfgChannel   = NULL;
	$smt->CfgAlerting  = NULL;
	
	$smt->AppChannel   = array();
	$smt->AppAlerting  = array();
	
	$smt->EvtChannel   = array();
	$smt->EvtAlerting  = array();
	
	/*! \def for Alert & Worker */
	$smt->AppWorker   = NULL;
	$smt->AppAlert 	  = NULL;
	
	
	/*! \def socket timeout */
	$smt->AppSec       = 5;
	$smt->AppUsec	   = 2000;
	
	/*! \def vendor Id */
	$smt->VendorId = 0;
	$smt->VendorName = NULL;
	$smt->VendorHost = NULL;
	$smt->VendorHostname= NULL;
	$smt->VendorKernel = NULL;
	
	/*! Retval */
	return $smt;
	
}


/*!
 * smtAppInitService
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function smtAppInitService(&$smt = NULL)
{
	app_func_free($service);
	if (!app_cfg_header('smt.svc', $service))
	{
		return 0;	
	}
	
	foreach ($service as $name => $lock)
	{
		app_func_alloc($ret);
		app_func_copy($ret, 'pid', 0);
		app_func_copy($ret, 'name', $name);
		app_func_copy($ret, 'lock', $lock);
		app_func_copy($ret, 'state', 0);
		
		if (app_func_copy($smt->AppService, $name, $ret))
		{
			app_func_free($ret);	
		}
	} 
	return 0;
}

/*!
 * \remark smtAppInitAlerting
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function smtAppInitAlerting(&$smt = NULL)
{
	$smt->CfgAlerting = array_map('trim', explode(',', $smt->CfgAlerting));
	 foreach ($smt->CfgAlerting as $cfg)
	{
		/*! \retval cari nama event yang di activekan pada config */
		app_func_free($cfr);
		if ( !app_cfg_header($cfg, $cfr) )
		{
			continue;
		}
		
		/*! \retval masukan data event berdasarka nama event */
		foreach ($cfr as $chan => $event)
		{
			$smt->AppAlerting[$chan][$cfg]= $event; 
		}
	}
	
	return 0;
}

/*!
 * smtAppInitChannel
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function smtAppInitChannel(&$smt = NULL)
{
	$smt->CfgChannel = array_map('trim', explode(',', $smt->CfgChannel));
	 foreach ($smt->CfgChannel as $cfg)
	{
		/*! \retval cari nama event yang di activekan pada config */
		app_func_free($cfr);
		if ( !app_cfg_header($cfg, $cfr) )
		{
			continue;
		}
		
		/*! \retval masukan data event berdasarka nama event */
		foreach ($cfr as $chan => $event)
		{
			$smt->AppChannel[$chan][$cfg]= $event; 
		}
	}
	
	return 0;
}




/*!
 * smtAppInitConfig
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function smtAppInitConfig(&$smt = NULL)
{
	/*! \remark Set Program PID to Local */
	app_func_struct($app);
	app_func_copy($app->cfg, 0);
	
	/*! \remark next Iterator Process */
	if ($smt->AppProgPid = getmypid())
	{
		app_func_copy($smt->AppProgRun, 
			app_func_next($smt->AppProgRun)
		);
	}
	
	/*! \remark while Read Config file program process */
	app_cfg_main($smt);
	
	/*! \remark create fork run */
	app_fork_run($smt);
	
	/*! \remark Open All Configuration */
	if (!app_cfg_parser($smt->AppProgCfg, $app->cfg))
	{
		return 0; 
	}
	
	/*! \remark copy stream to Global ? <SMT> */
	if (!app_func_copy($smt->AppProgVar, $app->cfg))
	{
		return 0;
	}
	
	 
	/*! \retval TCP config */
	app_func_copy($smt->TcpHost, app_cfg_param('smt.tcp','host'));
	app_func_copy($smt->TcpPort, app_cfg_param('smt.tcp','port'));
	app_func_copy($smt->TcpSign, app_cfg_param('smt.tcp','sign'));
	
	/*! \retval Udp config */
	app_func_copy($smt->UdpHost, app_cfg_param('smt.udp','host'));
	app_func_copy($smt->UdpPort, app_cfg_param('smt.udp','port'));
	app_func_copy($smt->UdpSign, app_cfg_param('smt.udp','sign'));
	
	/*! \retval Pub config */
	app_func_copy($smt->PubHost, app_cfg_param('smt.pub','host'));
	app_func_copy($smt->PubPort, app_cfg_param('smt.pub','port'));
	app_func_copy($smt->PubSign, app_cfg_param('smt.pub','sign'));
	
	/*! \retval for storage Application */
	app_func_copy($smt->CfgPathStg, app_cfg_param('smt.cfg','stg'));
	
	
	/*! \retval Get Config Service for Monitoring */
	if (sizeof($smt->AppService)<1)
	{
		smtAppInitService($smt);
	}

	/*! \retval Get Config Channel for All */ 
	app_func_free($smt->CfgChannel);
	if ( app_cfg_param( 'smt.cfg', 'evt', $smt->CfgChannel) ) 
	{
		smtAppInitChannel($smt);
	}
	
	
	/*! \retval Get Config Channel for All */ 
	app_func_free($smt->CfgAlerting);
	if ( app_cfg_param( 'smt.cfg', 'alt', $smt->CfgAlerting) ) 
	{
		smtAppInitAlerting($smt);
	}
	
	/*! finaly return this */ 
	return ( is_array($app->cfg) ? sizeof($app->cfg) : 0 );
}