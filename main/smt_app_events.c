<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 

/*! 
 * \brief	smtAppUdpEventMessage
 *
 * \param 	{smt} Global Oject Application 
 * \param 	{buf} source stream from socket 
 * \param	{cli} current client connected 
 *
 * \retval	{mix} integer or boolean 	 
 *
 */
 function smtAppUdpEventMessage(&$smt = NULL, &$buf = NULL, $cli = NULL)
{
	/*! \retval reset value(s) */
	app_func_copy($s, false);
	app_func_copy($c, $cli);
	
	if (onTraceUdpReceive($buf, $s))
	{
		/*! \retval check key(action) */
		 if (!isset($s->Action))
		{
			app_func_free($buf);
			return 0;
		}
		
	   /*! 
		* \remark Jika process berasal dari ' .CLI '
		* Contoh nya ketika akan melakukan reload atau force process maka tidak perlu connect  
		* ke console degan telnet namun langsung saja ke console base hal ini ter inspirasi 
		* dari Asterisk yang langsung menggunakan command console seperti 'asterisk -rx "command" '
		*
		*/ 
		
		if (onTraceUdpConsole($s, $false))
		{
			/*! \retval for CLI Reload Action */
			if (!strcmp($s->Action, 'Reload'))
			{
				/*! \remark convert data to 'lowwercase' */
				app_func_copy($s->Data, strtoupper($s->Data));	
				switch ($s->Data)
				{
					/*! \retval for <\ Helper > Action Stream */	
					case 'HELPER':
						app_func_event( 'cli_reload_helper', 
							array(&$smt, $s, $c), __FILE__, __LINE__ 
						);	
					break;
					
					/*! \retval for <\ Core > Action Stream */	
					case 'CORE':
						app_func_event( 'cli_reload_core', 
							array(&$smt, $s, $c), __FILE__, __LINE__ 
						);	
					break;
					
					/*! \retval for <\ alerting > Action Stream */
					case 'ALERTING':
						app_func_event( 'cli_reload_alerting', 
							array(&$smt, $s, $c), __FILE__, __LINE__ 
						); 
					break;
					
					/*! \retval for <\ Worker > Action Stream */	
					case 'WORKER':
						app_func_event( 'cli_reload_worker', 
							array(&$smt, $s, $c), __FILE__, __LINE__ 
						);	
					break;
				}	
			} 
			
			/*! \retval stop and break process */
			app_func_free($buf);
			return 0;
		}
		
		/*! \remark jika process berasal dari socket Event  dengan normal */
		/*! \remark jika process berasal dari socket Event  dengan normal */
		
		switch ($s->Action)
		{
			/*! \retval for <\ Vendor > Action Stream */
			case 'Vendor':
				app_func_event( 'udp_event_vendor', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				);
			break;
			
			
			/*! \retval for <\ Version > Action Stream */
			case 'Version':
				app_func_event('udp_event_version', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Uptime > Action Stream */
			case 'Uptime':
				app_func_event('udp_event_uptime', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Average > Action Stream */
			case 'Average':
				app_func_event('udp_event_average', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Memory > Action Stream */
			case 'Memory':
				app_func_event('udp_event_memory', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Network > Action Stream */
			case 'Network':
				app_func_event('udp_event_network', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Hardisk > Action Stream */
			case 'Hardisk':
				app_func_event('udp_event_hardisk', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Cpuinfo > Action Stream */
			case 'Cpuinfo':
				app_func_event('udp_event_cpuinfo', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			
			/*! \retval for <\ Service > Action Stream */
			case 'Service':
				app_func_event('udp_event_service', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break;
			
			/*! \retval for <\ Storage > Action Stream */
			case 'Storage':
				app_func_event('udp_event_storage', 
					array(&$smt, $s, $c), __FILE__, __LINE__ 
				); 
			break; 
		}
	} 
	
	/*! \retval free usage memory */
	app_func_free($buf);  
	return 0;
}

/*! 
 * \brief	smtAppTcpEventMessage
 *
 * \param 	{smt} Global Oject Application 
 * \param 	{buf} source stream from socket 
 * \param	{cli} current client connected 
 *
 * \retval	{mix} integer or boolean 	 
 *
 */
 function smtAppTcpEventMessage(&$smt = NULL, $buf = NULL, $cli = NULL)
{
	// app_parse_tcp_message();
	return 0;
}

/*! 
 * \brief	smtAppPubEventMessage
 *
 * \param 	{smt} Global Oject Application 
 * \param 	{buf} source stream from socket 
 * \param	{cli} current client connected 
 *
 * \retval	{mix} integer or boolean 	 
 *
 */
 
 function smtAppPubEventMessage(&$smt = NULL, $buf = NULL, $cli = NULL)
{
	// smtAppPubParseMessage($buf);
	return 0;
}