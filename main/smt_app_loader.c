<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

 function smtAppAutoclass(&$smt = NULL, $o = NULL, $c = NULL )
{
	
	/*! check apakah object sudah ada */
	if ( !isset($smt->$o) ) 
	{
		$smt->$o = NULL;
	}
	
	/*! create_function object */ 
	if ( !is_object($smt->$o) )
	{
		if (!class_exists($c) )
			return 0;
		else if (class_exists($c))
		{
			$smt->$o = new $c();
			return $smt->$o;
		}
	}
	return 0;
	
}

/*!
 * smtAppAutoloader
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 */
 
 function smtAppAutoloader(&$smt = NULL)
{
	/*! Load All Libraries */
	if(!is_dir($smt->CfgPathLib)){
		exit(0);
	}
	
	smtAppEventLoader($smt, 'Libraries', $smt->CfgPathLib);
	smtAppEventLoader($smt, 'Socket', $smt->CfgPathSck);
	smtAppEventLoader($smt, 'Worker', $smt->CfgPathWkr);
	smtAppEventLoader($smt, 'Alert', $smt->CfgPathAlt);
	smtAppEventLoader($smt, 'WebHook', $smt->CfgPathHok);
	smtAppEventLoader($smt, 'Modul', $smt->CfgPathMod);
	smtAppEventLoader($smt, 'Event', $smt->CfgPathEvt);
	smtAppEventLoader($smt, 'Cli', $smt->CfgPathCli);
	
	/*! \remark Init modul */
	if (!isset($smt->Modul))
	{
		app_func_struct($smt->Modul);
	}
	
	/*! \remark test read Modul Class Open  */
	if (!isset($smt->AutoClass))
	{ 
		$smt->AutoClass = array(
			'AppProc' => 'SMTAppProc'
		);
	}
	
	if (is_array($smt->AutoClass)) foreach ($smt->AutoClass as $name => $object)
	{
		if (!smtAppAutoclass($smt, $name, $object))
		{
			smt_log_error(sprintf("Fail open object class <%s>", $object), __FILE__, __LINE__ );
		}
	}
	
	/*! retval  === false */
	return 0;
}


/*!
 * \brief smtAppEventLoader
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 */
 function smtAppEventLoader(&$smt= NULL, $cfgModul = NULL, $cfgPath = NULL)
{
   /*! \retval load CLI */
	$cfgModulDirectory = scandir($cfgPath);
	foreach ($cfgModulDirectory as $i => $sf)
    {
	   /*! 
	    * \retval 
		* validation if directory is empty will 
		* next iterator porcess  
		*
		*/
		if (!is_dir($sf) && strpos($sf, '.c')){
			$smt->AppProgSpl[$cfgModul][] = sprintf("%s/%s", $cfgPath, $sf);
		} 
	} 
	
   /*! \remark After All file is Included then will load */
	$smtFetchModul = $smt->AppProgSpl[$cfgModul]; 
	foreach ($smtFetchModul as $i => $sf)
	{
		if ( @file_exists($sf) ){
			include_once($sf);
		}
	}
	return $cfgModul;
}

/*!
 * \brief smtAppLogerLoader
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 */ 
 
 function smtAppLogerLoader(&$smt)
{
	if ($smt->AppProgScr)
	{  
		foreach ($smt->AppProgSpl as $event => $sf)
		{
			foreach ($sf as $j => $file)
			{
				smt_log_info( sprintf("smtAppLogerLoader(%s, %s)", $event, basename($file) ), 
				__FILE__, __LINE__ );
			}
		}
	}
}