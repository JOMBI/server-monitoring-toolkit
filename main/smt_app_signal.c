<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT Toolkit >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
  
/**
 * \brief  	smtAppMainSigAlarm
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainSigAlarm()
{
	Global $smt;	
	
	smt_log_app("Caught SIGALRM");
	foreach ($smt->AppThread as $pid)
	{
		posix_kill($pid, SIGINT);
	}
	return 0;
} 

/**
 * \brief  	smtAppMainSigClient
 *
 * \remark  Process Ini Tidak di Perlukana jika process hanya bersifat state Less  
 * 	artinya one request one Response ini di perlukan jiga model transaksi Bersifat Realtime 
 *  semisal dengan websocket atu real time protokol process ini sangat di perlukan karena 
 *  terkadang client tidak me request 'Close' namun karena indikasi lain semisal koneksi 
 *  buruk yang menyebabkan koneksi terputus . 
 *
 * \param  	{smt} 	Global Object Handler 
 * \param  	{pid} 	Client PID Signal Trap  
 *
 * \retval 	int(0) 
 *
 */
 function smtAppMainSigClient(&$smt = NULL, &$pid = NULL)
{
	foreach ($smt->AppClient as $id => $c)
	{
		if ( app_func_retval($c, $s) )
		{
			if ($s->pid &&( !strcmp($s->pid, $pid) )) {
				unset($smt->AppClient[$s->id]);
			}
		}	
		app_func_free($c);	
	}
	return 0;
 }
 
/**
 * \brief  	smtAppMainSigRetval
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainSigRetval(&$smt = NULL, $signo = NULL)
{
	/*! \remark create Local variable */
	app_func_struct($out);
	app_func_copy($out->pid, 0); 
	app_func_copy($out->wait, 0);
	app_func_copy($out->status, 0);
	
	/*! \remark iteration process collected */
	
	while ( ($out->pid = pcntl_wait($out->status, WNOHANG)) >0 )
	{
		/*! \remark stop / state = 0 worker thread Method */
		if ( smtAppRetvalWorker($out->pid, $modul, $event, $flags, $time) )
		{
			$smt->AppWorker[$out->pid]['status'] = 0; 
			$smt->AppWorker[$out->pid]['time'] = app_func_time();
		}
		
		/*! \remark stop / state = 0 worker thread Method */
		if ( smtAppRetvalAlert($out->pid, $modul, $event, $flags, $time) )
		{
			$smt->AppAlert[$out->pid]['status'] = 0; 
			$smt->AppAlert[$out->pid]['time'] = app_func_time();
		}
		
		/*! \remark clean up pids from SMT Global */
		$smt->AppThread = array_diff($smt->AppThread, array($out->pid)); 
		app_func_copy( $out->wait, 	
			pcntl_wifexited($out->status)
		); 
		
	   /*! 
	    * \remark check pid is whohang from process 
	    * like for zombie process on here please set change on this 
		* the next level fro kill 
		*
		* Please Use for debug Olny !
		*
		*/
		if ($smt->AppProgScr)
		{
			if (!$out->wait)
			{ 
				smt_log_info(sprintf("Signal Collected Kill PID(%d)", $out->pid), __FILE__, __LINE__ ); 
			} 
			else if($out->wait)
			{ 
				smt_log_info(sprintf("Signal Collected Exited PID(%d)", $out->pid), __FILE__, __LINE__ ); 
			}
		}
	}
	
	/*! \retval int(0) */
	app_func_free($out);
	return 0;
} 
 
/*!
 * \brief  	smtAppMainSigHandler
 *
 * \remark  Description Of Method 
 * \param  	-
 *
 * \retval 	int(0) 
 *
 */
 function smtAppMainSigHandler($signo = NULL)
{ 
	Global $smt;
	
	switch($signo)
	{
	   /*! \retval SIGTERM : shutdown process: */
		case SIGTERM: 
			smt_log_info("Signal: SIGTERM", __FILE__, __LINE__ );
			exit(0);
		break; 
		
		/*! \retval SIGHUP : signal restarting process from system */
		case SIGHUP:
			smt_log_info("Signal: SIGHUP", __FILE__, __LINE__ );
			exit(0);
		break;
		
		/*! \retval SIGINT: Signal Interupted by Sytem */
		case SIGINT: 
			smt_log_info("Signal: SIGINT", __FILE__, __LINE__ );
			exit(0);
		break; 
		
		/*! \retval SIGQUIT : Quit Signal from Process */
		case SIGQUIT:
			smt_log_info("Signal: SIGQUIT", __FILE__, __LINE__ );
			exit(0);
		break;
		
		/*! \retval SIGPIPE : Broken Pipe Like Socket connected:*/
		case SIGPIPE:
			smt_log_info("Signal: SIGPIPE", __FILE__, __LINE__ ); 
		break;
		
		/*! \retval SIGCHLD : signal from child */
		case SIGCHLD:
			if (function_exists( 'smtAppMainSigRetval' )) {
				smtAppMainSigRetval($smt, SIGCHLD);
			}
		break;
		
		/*! \retval No Signal Define set On here : */ 
		default:  
			smt_log_info("Signal: Default", __FILE__, __LINE__ ); 
		break; 
   } 
   /*! \retval int(0) === false */
   return 0;
}  

 
/*!
 * \brief  	smtAppMainSignal
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainSignal(&$smt = NULL)
{ 	
    /*! \retval if posix methode is not fail */
	if (!function_exists('pcntl_signal'))
	{
		app_func_copy($smt->AppSignal, 0);
		return 0;
	}
	
	/*! \retval Copy Signal If Success */
	if (app_func_copy($smt->AppSignal, 1))
	{
		pcntl_signal(SIGTERM, 'smtAppMainSigHandler'); 
		pcntl_signal(SIGHUP,  'smtAppMainSigHandler');
		pcntl_signal(SIGINT,  'smtAppMainSigHandler');
		pcntl_signal(SIGPIPE, 'smtAppMainSigHandler');
		pcntl_signal(SIGQUIT, 'smtAppMainSigHandler');
		pcntl_signal(SIGCHLD, 'smtAppMainSigHandler'); 
		pcntl_signal(SIGALRM, 'smtAppMainSigAlarm');
	}
	
	/*! \retval signal true */
	
	return app_func_int($smt->AppSignal);
}  