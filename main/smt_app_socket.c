<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*!
 * \brief  	smtAppUdpSocket
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppUdpSocket(&$smt = NULL)
{
	if (smtSockUdpServerCreate($smt->UdpLink, $smt->UdpHost, $smt->UdpPort)<0)
	{
		smt_log_error("Created 'smtAppUdpSocket()' Error", __FILE__, __LINE__);
		return 0;
	}
	
	return $smt->UdpLink;
}

/*!
 * \brief  	smtAppTcpSocket
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppTcpSocket(&$smt = NULL)
{
	if (smtSockTcpServerCreate($smt->TcpLink, $smt->TcpHost, $smt->TcpPort, $smt->TcpSign)<0 )
	{
		smt_log_error("Created 'smtAppTcpSocket()' Error.", __FILE__, __LINE__);
		return 0;
	} 
	return $smt->TcpLink;
}

/*!
 * \brief  	smtAppPubSocket
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppPubSocket(&$smt = NULL)
{
	if (smtSockTcpServerCreate($smt->PubLink, $smt->PubHost, $smt->PubPort, $smt->PubSign)<0 )
	{
		smt_log_error("Created 'smtAppPubSocket()' Error.", __FILE__, __LINE__);
		return 0;
	} 
	return $smt->TcpLink;
}

/*!
 * \brief  	smtAppMainSignal
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainSocket(&$smt = NULL)
{
	/*! \remark RunSock */
	if (!isset($smt->RunSock))
	{
		app_func_copy($smt->RunSock, 0);
	}
	
	/*! \remark smtAppUdpSocket */
	if (smtAppUdpSocket($smt)){
		app_func_next($smt->RunSock);
	}
	
	/*! \remark smtAppTcpSocket */
	if (smtAppTcpSocket($smt))
	{
		app_func_next($smt->RunSock);
		if (function_exists('smtSockTcpSetNonblock')){
			smtSockTcpSetNonblock($smt->TcpLink);
		}
	}
	
	/*! \remark  smtAppPubSocket */
	if (smtAppPubSocket($smt))
	{
		app_func_next($smt->RunSock);
		if (function_exists('smtSockTcpSetNonblock')) {
			smtSockTcpSetNonblock($smt->PubLink);
		}
	}
	
	/*! \retval finally is false */
	return $smt->RunSock;
}