<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */



/*!
 * \brief  	smtAppMainStorage
 * \remark  Description Of Method 
 *
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainStorage(&$smt = false)
{
   /*! \remark 
	*
	* Pada data Storage Ada 2 Jenis Type data 1:Static, 2:Dynamic 
	*
	* 1. Static
	*    data storage CRID yang memiliki nilai default yang kemungkinan struktur data  
	*	 tidak berubah 
	*
	* 2. Dynamic 
	* 	 Data Struktur dapat Berubah-ubah tergantung Perngkat yang sedang di Monitoring 
	* 	 semisal network Bisa Memiliki lebih dari satu 'eth' yang mana ini sangat bergantung  
	*    level configurasi Perangkat itu sendiri 
	*
	*
	*/
	if (sizeof($smt->AppStorage)>0)
	{
		app_func_copy($storage, $smt->AppStorage); 
		if (stg_create_storage($smt, $storage))
		{
			app_func_free($storage);
		}
	}
	
	else if (sizeof($smt->AppStorage)<1)
	{
		app_func_free($storage);
		if (stg_create_stream($smt, $storage))
		{
			/*! \remark create default file for Iteract Every Time will updated */
			if (stg_create_storage($smt, $storage))
			{
				app_func_free($storage);
			}
		}
	}
	
	return 0;
}