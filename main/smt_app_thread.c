<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*!
 * \brief  	smtAppCreateThread
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppCreateThread(&$smt = NULL, $event = NULL, $param = NULL, &$pid = 0 )
{
	/*!\retval ceate multi thread  management pararel process*/
	app_func_struct($out);
	app_func_copy($out->pid, pcntl_fork());
	
	
	/*! \retval if false process then */
	if ($out->pid == -1) 
		die('could not fork');
		
	else if ($out->pid)
	{ 
		/*! \retval detach PID to SMT Application Monitoring */
		app_signal_register($smt, $out->pid);
		
	}
	
	/*! \retval fork child hook  */
	else 
	{
		/*! \retval app_signal_worker success */
		if (app_signal_worker($smt, $event, $param))
		{
			/*! kill process If done Process handler zombie */
			if ($smt->pid &&(function_exists( 'posix_kill' ))){
				if (posix_kill($smt->pid, SIGKILL))
				{
					exit(0);
				}
			}
		}
	} 
	
	/*! \retval Return PID for Global Master */ 
	if (app_func_copy($pid, $out->pid)) 
	{
		app_func_free($out);
	}
	
	return $pid;
} 