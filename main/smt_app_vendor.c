<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*!
 * \brief  	smtAppMainVendor
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function smtAppMainVendor(&$smt = false)
{
	
	/*! \retval vendor ID */  
	if (!$smt->VendorHost)
	{
		app_func_copy($smt->VendorHost, app_cfg_param('smt.vendor','host'));
	}
	
	/*! \retval vendor ID */ 
	if (!$smt->VendorId)
	{
		app_func_copy($smt->VendorId, app_cfg_param('smt.vendor','code'));
	}
	
	/*! \retval vendor Name */
	if (!$smt->VendorName)
	{
		app_func_copy($smt->VendorName, app_cfg_param('smt.vendor','name'));
	}
	
	/*! \retval Vendor Hostname */
	if (!$smt->VendorHostname && app_vendor_hostname($hostname) )
	{
		app_func_copy($smt->VendorHostname, $hostname);
	} 
	
	/*! \retval Vendor Kernel */
	if (!$smt->VendorKernel && app_vendor_kernel($kernel) )
	{
		app_func_copy($smt->VendorKernel, $kernel);
	}  
	
	return $smt;
}