<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */


/*! \remark smtAppMainWorker */
 function smtAppMainWorker(&$smt = NULL ) 
{
	/*! \retval	created thread every Look */	
	
	app_func_alloc($smt->smtAppWorker);
	app_func_copy($smt->smtAppWorker, 'smtVendor' , 'smtAppWorkerVendor');
	app_func_copy($smt->smtAppWorker, 'smtVersion', 'smtAppWorkerVersion');
	app_func_copy($smt->smtAppWorker, 'smtUptime' , 'smtAppWorkerUptime');
	app_func_copy($smt->smtAppWorker, 'smtMemory' , 'smtAppWorkerMemory');
	app_func_copy($smt->smtAppWorker, 'smtAverage', 'smtAppWorkerAverage');
	app_func_copy($smt->smtAppWorker, 'smtCpuinfo', 'smtAppWorkerCpuinfo');
	app_func_copy($smt->smtAppWorker, 'smtHardisk', 'smtAppWorkerHardisk');
	app_func_copy($smt->smtAppWorker, 'smtNetwork', 'smtAppWorkerNetwork');
	app_func_copy($smt->smtAppWorker, 'smtService', 'smtAppWorkerService'); 
	app_func_copy($smt->smtAppWorker, 'smtStorage', 'smtAppWorkerStorage'); 
	// app_func_copy($smt->smtAppWorker, 'smtWarning', 'smtAppWorkerWarning'); 
	
	/*! \remark write thread base on smtAppMainWorker */ 
	foreach ($smt->smtAppWorker as $modul => $event)
	{
		
		if (!function_exists($event)) 
		{
			smt_log_error(sprintf("Worker '%s' Error.", $event), __FILE__, __LINE__ );
			continue;
		}
		
		if (function_exists($event)) 
		{
		   /*! 
		    * \remark Masukan Thread yang Sedang di Monitoring, Untuk Tujuan 
			*  Memenage Thread Yang sedang di process atau untuk Mencak Apakah Thread jalan Atau Sudah Mati 
			*  Jika Mati Bisa saja Thread Akan di Hidupkan Kembali , Atau tidak sama sekali 
			*/ 
			app_func_free($pid);
			if (smtAppCreateThread( $smt, $event, array($smt), $pid))
			{
				$smt->AppWorker[$pid]['modul']  = $modul;
				$smt->AppWorker[$pid]['event']  = $event;
				$smt->AppWorker[$pid]['status'] = $pid;
				$smt->AppWorker[$pid]['time']   = app_func_time(); 
			} 
		}
	}
	return 0;
}


/*! \retval smtAppMonitorWorker */
 function smtAppMonitorWorker(&$smt= NULL)
{
	foreach ($smt->AppWorker as $pid => $work)
	{
		if (smtAppRetvalWorker($pid, $modul, $event, $status, $time))
		{
			if (!$status &&((time() - $time) >= 2)) 
			{   
				smt_log_info(sprintf("Stop Worker '%s'", $event), 
				__FILE__, __LINE__);
				
				/*! \retval start smtAppStartWorker */
				/*! \retval deleted smtAppStartWorker */
				if (smtAppStartWorker($smt, $modul, $event, $newpid))
				{
					smtAppDeleteWorker($pid); 
				}
			}
		}
	} 
	return 0;
}

/*! \remark smtAppRetvalWorker */
 function smtAppRetvalWorker($pid= 0, &$modul = NULL, &$event = NULL, &$status= 0, &$time = 0) 
{
	Global $smt;
	
	/*! \retval 'app_func_copy' */
	app_func_copy($modul,NULL);
	app_func_copy($event, NULL);
	app_func_copy($status, 0);
	app_func_free($time, 0);
	
	
	/*! \retval tested process */
	if ( !isset($smt->AppWorker[$pid]) )
		return 0;
	else if ( isset($smt->AppWorker[$pid]) )
	{
		app_func_copy($modul, $smt->AppWorker[$pid]['modul']);
		app_func_copy($event, $smt->AppWorker[$pid]['event']);
		app_func_copy($status, $smt->AppWorker[$pid]['status']);
		app_func_copy($time, $smt->AppWorker[$pid]['time']); 
	}
	
	/*! \retval finaly integer */
	return $pid;
	
}

/*! \remark smtAppDeleteWorker */
 function smtAppDeleteWorker($pid = 0) 
{
	Global $smt; 
	
	/*! \remark for process apps */
	app_func_copy($retval, 0);
	if ( isset($smt->AppWorker[$pid]) )
	{ 
		unset($smt->AppWorker[$pid]);
		app_func_next($retval);
	}
	return $retval;
}

/*! \remark smtAppStartWorker */
 function smtAppStartWorker(&$smt = NULL, $modul= NULL, $event = NULL, &$pid = 0 ) 
{
	/*! \retval app_func_struct */
	app_func_struct($out);
	app_func_copy($out->modul, $modul);
	app_func_copy($out->event, $event); 
	
	
	/*! \retval 'app_func_time' */
	app_func_copy( $out->time, 
		app_func_time()
	); 
	
	/*! \retval function_exists */
	if ( function_exists($out->event) ) 
	{
		app_func_free($out->pid);
		if ( smtAppCreateThread($smt, $out->event, array($smt), $out->pid) )
		{
			$smt->AppWorker[$out->pid]['modul']  = $out->modul;
			$smt->AppWorker[$out->pid]['event']  = $out->event;
			$smt->AppWorker[$out->pid]['status'] = $out->pid; 
			$smt->AppWorker[$out->pid]['time']   = $out->time;
			
			/*! copy PID to retval new process */
			if ( app_func_copy($pid, $out->pid) )
			{
				app_func_free($out);
				return $pid;
			}
		}
	}
	
	/*! \retval return is === false */
	return 0;
}
