<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */ 
 
 if (!defined('API_CHATID')) define('API_CHATID', -566830220);
 if (!defined('API_TOKEN')) define('API_TOKEN', '1959432303:AAE5r4UCKpjtWmLSXk5k2IoX8aw1uuTHZYo');
 if (!defined('API_HOST')) define('API_HOST', 'https://api.telegram.org/bot');
 
 class Telegram 
{
	var $urlapi  = NULL;
	var $param	 = array();
	
	/*! \retval Telegram(__construct) */
	function __construct(){
		
	}
	
	/*! \retval sent text from SMT to Telegram */
	 function sendAlert(&$simon = NULL) 
	{
	   
	   /*! 
	    * \remark karena Ini menggunakan Process posix_thread , Maka Sebaikanya 
		* Ketika Process Ini sudah selesai jangan di exit tapi di KILL saja 
		*
		*/
		
		app_func_copy($pid, getmypid());
		app_func_copy($url, sprintf("%s%s", API_HOST, API_TOKEN));
		
		while ($pid)
		{   
		   /*! \remark if Invalid str */ 
			if (!$url) 
			{
				break;
			} 
		   
		   /*! \remark use Global function required */
			app_func_copy( $message , 
				(isset($simon['message']) ?  $simon['message']: false)
			);
			
			if (!$message) {
				break;
			}
			
		    /*! \retval Build Message to sent process */
			app_func_alloc($param);
			app_func_copy($param, 'chat_id', API_CHATID);
			app_func_copy($param, 'text', $message);
			
			/*! \retval create_function curl */ 
		    app_func_copy($start, microtime(true));
		    if (app_func_copy($url, $url. '/sendMessage'))
			{
				smt_log_info(sprintf("Send Alert <%s> to '%s' ", $simon['event'], $url),__FILE__, __LINE__ );
			}
			
			if (app_func_copy($ch, curl_init($url)) && is_resource($ch) )
			{
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, ($param));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);  
			}
			
		   /*! \remark close Any Time */
			curl_close($ch);
			break;
		}
		
		/*! \remark close Any Time */
		if ( app_func_copy($maxtime, (microtime(true) - $start)) )
		{
			smt_log_info(sprintf("Send Alert Execute Time (%s) ms.", $maxtime), __FILE__, __LINE__ );
			smt_log_app("\t");
		}
		
		/*! \remark KILL process from here */
		if ($pid &&(function_exists( 'posix_kill' )))
		{
			if ( posix_kill($pid, SIGKILL) ) 
			{
				exit(0);
			}
		}
		
		/*! \retval return === 0 */
		return 0;
	}
	
	/*! \retval Telegram(EventHandler) */
	 function EventHandler(&$smt = NULL, $event = NULL, $data = NULL)
	{
		switch ($event)
		{
			case 'vendor':
				// your code here ...
			break;	
			
			case 'version':
				// your code here ...
			break;
			
			case 'cpuinfo':
				// your code here ...
			break;
			
			case 'average':
				// your code here ...
			break;
				
			case 'network':
				// your code here ...
			break;	
				
			case 'uptime':
				// your code here ...
			break;		
				
			case 'memory':
				// your code here ...
			break;	

			case 'hardisk':
				// your code here ...
			break;		
			
			case 'service':
				// your code here ...
			break;	
		}
		
		return 0;	
	}
	
   /*! \retval Telegram(EventAlerting) */
	 function EventAlerting(&$smt = NULL, $event = NULL, $data = NULL)
	{
		switch ($event)
		{
			// for Average 
			case 'average':
				self::sendAlert($data);
			break;	
			
			// for Hardisk 
			case 'hardisk':
				self::sendAlert($data);
			break;	
			
			// for service
			case 'service':
				self::sendAlert($data);
			break;	
			
			// for memory 
			case 'memory':
				self::sendAlert($data);
			break;
		} 
		return 0;
	}
} 
?>