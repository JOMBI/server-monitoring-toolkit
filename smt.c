#!/usr/bin/php
<?php if(!defined('SMT_APP')) define('SMT_APP', dirname( __FILE__ ));
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT Toolkit >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 // https://lms.onnocenter.or.id/wiki/index.php/Asterisk_Video_Call 
 /*! running
	php -q /opt/kawani/bin/simon/smt.c --cfg=/opt/kawani/bin/simon/config/smt.conf --pid=/var/run/smt/smt.pid --vvv=1
 */
 
 
 /*! create Global Object */
 $smt = new stdClass();
 
 /*! debug Track Error */
 if (function_exists('error_reporting')){
  ini_set("display_errors",1);
  error_reporting(E_ALL);	
}
 
 /*! create signal fork handler */ 
 if (function_exists( 'pcntl_async_signals')) 
	pcntl_async_signals(true);
 else {
	declare(ticks=1);
 }
 
/*! Unlimited process */
 if ( function_exists('set_time_limit') ){
   set_time_limit(0);
   ob_implicit_flush();
   ignore_user_abort(0);
}
 
/*! declare All php Ini Modul */
 if (function_exists('date_default_timezone_set')){
	date_default_timezone_set( 'Asia/Jakarta' );
 }

 
/*! 
 * include Application Header 
 *
 */
 
 include dirname( __FILE__ ) .'/smt.h';
 
/*!
 * ToolkitMainApplictaion
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function ToolkitMainApplictaion()
{
	Global $smt;	 
	
	if (php_sapi_name() != 'cli')
	{ 
		exit(0);	
	}
	
	if (smtAppMainConfig($smt))
	{
		/*! \def Loader & Config */
		smtAppAutoloader($smt);
		if (!smtAppInitConfig($smt))
		{
			exit(0);
		}
		
		/*! \def Iniated Banner & Signal*/
		smtAppMainBanner($smt);  
		if (!smtAppMainSignal($smt))
		{
			smt_log_app("Create Signal Fail");
			smt_log_app("\t");
		}
		
		smt_log_app(sprintf("Starting Application(%d)", $smt->AppProgPid));
		smt_log_app("\t");
		
		
		/*! \def Iniated Vendor & Server Socket Thread */
		
		smtAppLogerLoader($smt);
		smtAppMainVendor($smt);
		
		if (smtAppMainSocket($smt))
		{
			smt_log_app(sprintf("Starting Created Socket(%d)", $smt->RunSock));
			if ($smt->RunSock)
			{
				smtAppMainWorker($smt);
				smtAppMainAlert($smt);
			}
		}
		 
		/*! \def Created Storage for All */
		smtAppMainStorage($smt);
		
		/*! \def Open Class From SPL */
		while(1)
		{
			/*! \retval process on ready */
			app_func_copy($smt->AppSocket, 0, $smt->UdpLink);
			app_func_copy($smt->AppSocket, 1, $smt->TcpLink);
			app_func_copy($smt->AppSocket, 2, $smt->PubLink); 
			
			
			/*! \remark Iterasi untuk semua jenis client yang connect ke socket server */
			if ( function_exists( 'smtAppClientIterator'))
			{
				smtAppClientIterator($smt);
			}  
			
			/*! \retval Iterupted Socket Sent Event */ 
			$smt->AppExcept = $smt->AppWrite = array(); $smt->AppRead = $smt->AppSocket;
			@socket_select($smt->AppRead, $smt->AppWrite, $except, $smt->AppSec, ($smt->AppSec * 1000)); 
			
			if ( @socket_select($smt->AppSocket, $smt->AppWrite, $smt->AppExcept, $smt->AppSec, ($smt->AppSec * 1000))>0) 
		   {
			    /*! \remark for TCP Server Port */
				if (app_func_sock($smt->TcpLink) && (app_func_search($smt->TcpLink, $smt->AppSocket)))
				{
					// var_dump($smt->TcpLink); 
				}
				
				
				/*! \remark for Public Server Port */
				if (app_func_sock($smt->PubLink) &&(app_func_search($smt->PubLink, $smt->AppSocket)))
				{
					/*! \remark ketika Ada yang Connect dari Port Public Lamgsung di iterasi untuk 
					dimasukan ke list socket */
					
					smtSockTcpAccept($smt->PubLink, $sock); 
					if (app_func_sock($sock) &&(smtSockTcpPeer($sock, $s)) )
					{
						$smt->AppClient[$s->from_id]['id']    = $s->from_id;
						$smt->AppClient[$s->from_id]['sock']  = $s->from_sock;
						$smt->AppClient[$s->from_id]['host']  = $s->from_ip;
						$smt->AppClient[$s->from_id]['port']  = $s->from_port;
						$smt->AppClient[$s->from_id]['time']  = $s->from_time;
						$smt->AppClient[$s->from_id]['pid']   = $s->from_pid;
						$smt->AppClient[$s->from_id]['type']  = 'pub'; 
						
						/*! \remark for Iformasi debug */
						smt_log_app(sprintf("%s:%06d - Session #%d Client Connected.", $s->from_ip, $s->from_port, $s->from_id));
					}
				} 
				
			    /*! \remark for UDP Server Port  */
				
				if (app_func_sock($smt->UdpLink) &&(app_func_search($smt->UdpLink, $smt->AppSocket)))
				{
					if (smtSockUdpRead($smt->UdpLink, $buf, $ret)) 
					{
						if (smtAppUdpEventMessage($smt, $buf, $ret)){
							app_func_free($buf);
						}
					}
				} 
				
				
			   /*! \remark Cari Semua Client yang connnect ke Sini */
				if ( function_exists('smtAppClientHandler')){
					smtAppClientHandler($smt);
				}
			}
			
		   /*!
		    * \note Worker Monitoring Event for Iterupted process Up 
			* Contoh Handler, Jika Sebuah Thread Worker Mati Kemudian Akan di Up kembali 
			* Dengan Demikian Selama service running selama itu pula Thread Monitor Jalan 
			*
			*/
			
			if (function_exists( 'smtAppMonitorWorker' )){
				smtAppMonitorWorker($smt);
			}
			
		   /*!
		    * \note Alert Monitoring Event for Iterupted process Up 
			* Contoh Handler, Jika Sebuah Thread Alert Mati Kemudian Akan di Up kembali 
			* Dengan Demikian Selama service running selama itu pula Thread Monitor Jalan 
			*
			*/
			if (function_exists( 'smtAppMonitorAlert' )){
				smtAppMonitorAlert($smt);
			}
			
			// smt_log_core("smt-dump", $smt);  
		}
	}
	/*! retval === 0 */
	app_func_free($smt);
	return 0;
 }

 /*! \retval __ ToolkitMainApplictaion ___*/
 ToolkitMainApplictaion();