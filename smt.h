<?php if(!defined('SMT_APP')) define('SMT_APP', dirname( __FILE__ ));
/**
 * IOT -- An open source Server Utilize toolkit.
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

 include( SMT_APP ."/main/smt_app_config.c");
 include( SMT_APP ."/main/smt_app_signal.c");
 include( SMT_APP ."/main/smt_app_banner.c");
 include( SMT_APP ."/main/smt_app_socket.c");
 include( SMT_APP ."/main/smt_app_thread.c");
 include( SMT_APP ."/main/smt_app_loader.c");
 include( SMT_APP ."/main/smt_app_vendor.c");
 include( SMT_APP ."/main/smt_app_channel.c");
 include( SMT_APP ."/main/smt_app_worker.c");
 include( SMT_APP ."/main/smt_app_alert.c");
 include( SMT_APP ."/main/smt_app_events.c");
 include( SMT_APP ."/main/smt_app_storage.c");
 include( SMT_APP ."/main/smt_app_client.c");