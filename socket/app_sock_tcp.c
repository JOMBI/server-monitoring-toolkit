<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * IOT -- An open source Server Utilize toolkit.
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/**
 * sockTcpDefineSocket
 *
 * Create Socket Recording
 *
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 *
 */
 
 define( 'SOK_EINTR', "\r\n". "\r\n");
 

/**
 * [smtSockTcpServerCreate]
 * @param  [type] $host [description]
 * @param  [type] $port [description]
 * @param  [type] $queue [description]
 *
 * @return [type]  [description]
 */	
 function smtSockTcpServerCreate(&$sock= false, $host =NULL, $port = 0, $sign= 0)
{
	$obj 		 = new stdclass();
	$obj->sock   = NULL;
	$obj->host   = $host;
	$obj->port   = $port;
	$obj->sign   = $sign;
	$obj->sect   = 10; 				// second 
	$obj->usec   = 0; 			// usec timeout 
	$obj->optval = 1; 
  
	// SOL_TCP.sock.socket_create:
	if(false === ( $obj->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) )) {
		smt_log_error("Created 'SocketCreate()' Error.", __FILE__, __LINE__ );
		return -1;
	}
  
  // SOL_TCP.sock.socket_set_option.SO_REUSEADDR 
	if (smtSockTcpSetReuse( $obj->sock, $obj->optval )<0 )
	{
		smt_log_error("Created 'smtSockTcpSetReuse()' Error.",  __FILE__, __LINE__ );
		socket_close($obj->sock); 
		return -1;
	}
  
  // SOL_TCP.sock.socket_set_option.SO_NONBLOCK 
	if (smtSockTcpSetNonblock($obj->sock)<0 )
	{
		smt_log_error("Created 'smtSockTcpSetNonblock()' Error.",  __FILE__, __LINE__ );
		socket_close( $obj->sock ); 
		return -1;
	}	 
  
	// SOL_TCP.sock.SOCKET_BIND
	if (false === @socket_bind( $obj->sock, $obj->host, $obj->port ))
	{
		smt_log_error("Created 'SocketBind()' Error.",  __FILE__, __LINE__ );
		socket_close($obj->sock); 
		return -1;
	}
  
	// SOL_TCP.sock.socket_listen 
	if( !$obj->sign ){
		$obj->sign = 128;
	} 
  
	// SOL_TCP.sock.socket_listen
	if (false === @socket_listen( $obj->sock, $obj->sign ))
	{
		smt_log_error("Created 'SocketListen()' Error.",  __FILE__, __LINE__ );	
		socket_close( $obj->sock ); 
		return -1;
	}

	// SOL_TCP.sock.socket_set_option.SO_SNDTIMEO
	if (smtSockTcpSetSndTimeout( $obj->sock, $obj->sect, $obj->usec )<0 )
	{
		smt_log_app("Created 'smtSockTcpSetSndTimeout()' Error.",  __FILE__, __LINE__ );	
		socket_close( $obj->sock ); 
		return -1;
	} 
  
	// SOL_TCP.sock.socket_set_option.SO_RCVTIMEO
	if (smtSockTcpSetRcvTimeout( $obj->sock, $obj->sect, $obj->usec )<0)
	{
		smt_log_error("Created 'smtSockTcpSetRcvTimeout()' Error.",  __FILE__, __LINE__ );	
		socket_close( $obj->sock ); 
		return -1;
	}
  
	// finaly return(sock): 
	app_func_copy($sock, $obj->sock);
	return $obj->sock;
}
 
 
/**
 * \brief smtSockTcpClientConnect
 *
 * \param  {socket} 	sock [description]
 * \param  {string}		host [description]
 * \param  {integer} 	port [description]
 *
 * \retval [mix]  NULL	 [description]
 *
 */	
 function smtSockTcpClientConnect( $sock = NULL, $host='127.0.0.1', $port = 19000 )
{
 
	/*! Create new object "Struct" */
	$src 	   = new stdclass();
	$src->sock = $sock;
	$src->host = $host;
	$src->port = $port;
	$src->usec = 10; 
	
	/* Validation Of IP */
	if (ip2long($src->host)< 0)
	{
		smt_log_app("ip2long() Fail.");   
	}
	
	/*! SOL_TCP.sock.socket_create: */
	if (!app_func_sock($src->sock)) 
	{
		smt_log_app("Socket() Fail.");  
		return -1; 
	}
	
	/*! SOL_TCP.sock.socket_non_blocking */ 
	if (smtSockTcpSetSndTimeout( $src->sock, 1, 0 )<0)
	{
		smt_log_app("smtSockTcpSetSndTimeout() Fail."); 
		return -1;
	}
	
	/*! connect to new socket server: */
	if (app_func_sock($src->sock) &&(false === @socket_connect($src->sock, $src->host, $src->port))) 
	{
		/*! get Error() from process socket connected */
		$src->code = socket_last_error($src->sock);
		$src->mesg = socket_strerror($src->code);
		
		/*! If Error code More Then Zero */
		if ($src->code){
			smt_log_app("socket_connect() Fail."); 
		} 
		return -1;
	}
	
	/*! SOL_TCP.sock.socket_set_option.SO_KEEPALIVE */
	if (false === @socket_set_option($src->sock, SOL_SOCKET, SO_KEEPALIVE,1))
	{
		smt_log_app("socket_set_option() Fail.");
		return -1;
	}
	
	return 0;
}

/**
 * [smtSockTcpClientCreate]
 * @param  [type] $buffer [description]
 * @return [type]  [description]
 */	
 function smtSockTcpClientCreate($option= 1)
{
	$obj = new stdclass(); $obj->optval = $option;
  
	// SOL_TCP.sock.socket_create.SOCK_STREAM
	if(false === ($obj->sock = socket_create(AF_INET, SOCK_STREAM, 0)))
	{
		return -1;
	} 
   
	// SOL_TCP.sock.socket_set_option.SO_REUSEADDR  
	if( false === socket_set_option($obj->sock, SOL_SOCKET, SO_REUSEADDR, $obj->optval) ){
		socket_close($obj->sock);
		return -1;
	}
  
	// return socket stream: (recsource)
	return $obj->sock;
}

/**
 * [smtSockTcpWrite] sent  sentTcpClientToServer
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function smtSockTcpWrite($sock= NULL, $buff = '', $t_out = 1)
{	
	$obj 		 = new stdclass(); 		// return after all data in buffer sent
	$obj->sent 	 = 0;					// packet sent 
	$obj->sock 	 = $sock;				// socket 
	$obj->buff 	 = $buff;				// buff messafe 
	$obj->length = strlen($obj->buff);	// size of packet
	
	while (($obj->sent!= $obj->length))
	{
		$obj->count = @socket_write($sock, $obj->buff, $obj->length);
		
		if ($obj->count === false) 
			return 0; 
		else if ($obj->count<1)
		{
			return 0;
		}
		
		$obj->sent += $obj->count;
		$obj->buff = substr($obj->buff, $obj->sent);
	}
  // finaly return :
  return (int)$obj->sent;
}	

/**
 * [smtSockTcpClientWrite2]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function smtSockTcpClientWrite2($sock= NULL, $buf = '', $t_out = 1)
{
	$obj 	   = new stdclass();
	$obj->size = strlen($buf);
	$obj->sent = @socket_write($sock, $buf, $obj->size);
	
	if ($obj->sent === false ) 
		return 0;
	else if ($obj->sent < $obj->size) 
	{
		$buf = substr($buf, $obj->sent);
		$obj->size -= $obj->sent;
	}
	
	// return default finnaly :
	return $obj->sent;
}	

/**
 * [smtSockTcpClientWrite]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 function smtSockTcpClientWrite($sock= NULL, $buff = '', $t_out = 1)
{
	// set socket Timeout from user setup :
	
	$obj 					 = new stdclass();
	$obj->timeout 			 = array();
	$obj->timeout['tv_sec']  = (($t_out)/1000); 
	$obj->timeout['tv_usec'] = (($t_out)*1000); 
  
  
	// set socket_set_nonblock socket :
	if ( app_func_sock($sock) )
	{
		smtSockTcpSetNonblock($sock); 
	}
  
	$obj->sockTcpSent = 0;
	while ($buff != '')
	{
        $obj->write = array($sock); $obj->read = $obj->except = NULL; // set timeout to 1s
        $obj->sockTcpTimeout = @socket_select($obj->read, $obj->write, $obj->except, $obj->timeout);
		
		if($obj->sockTcpTimeout<1) 
			return 0;
        else if ($obj->sockTcpTimeout>0) 
		{
            $obj->length = strlen($buff);
            $obj->sockTcpSent = @socket_write($sock, $buff, $obj->length);
			
			if($obj->sockTcpSent === false) 
				return 0;  // failed send data : 
			else if ($obj->sockTcpSent == $obj->length) 
			{  
				// complete sent data :
				$buff = substr($buff, $obj->sockTcpSent);
				$obj->length -= $obj->sockTcpSent; 
				return $obj->sockTcpSent;
			}
			
			// not complete sent data :
			else if ($obj->sockTcpSent<$obj->length) 
			{
				$buff = substr($buff, $obj->sockTcpSent);
				$obj->length -= $obj->sockTcpSent; 
				return $obj->sockTcpSent;
			}  
        }
	}
	
	// finaly Return data process :
	return $obj->sockTcpSent; // return fallback:
}


/**
 * smtSockTcpPeer
 *
 * @param  [sock]  	 recsource of socket 
 * @return [object]  obj[host, port]
 *
 */	 
 function smtSockTcpPeer($sock = NULL, &$ret = NULL)
{ 
	// create new object  
	app_func_free($ret);
	app_func_struct($obj);
	app_func_copy($obj->from_id	  , 0);
	app_func_copy($obj->from_ip	  , 0);
	app_func_copy($obj->from_pid  , 0);
	app_func_copy($obj->from_port , 0);
	app_func_copy($obj->from_sock , 0);
	app_func_copy($obj->from_time , 0); 
	
	// get sock Name : 
	if ( !app_func_sock($sock) ){
		return 0;	
	}
	// get sockpername :
	if ( @socket_getpeername($sock, $host, $port) )
	{ 
		app_func_copy($obj->from_id	  , app_func_sint($sock));
		app_func_copy($obj->from_ip	  ,	app_func_str($host));
		app_func_copy($obj->from_port ,	app_func_int($port));
		app_func_copy($obj->from_sock , $sock); 
		app_func_copy($obj->from_time , time());
	}
  
	// finally return;  
	if( app_func_copy($ret, $obj))
	{
		app_func_free($obj);
	} 
	return $ret;
}

/**
 * [smtSockTcpAccept ]
 * @param  [type] $address [description]
 * @return [type] $port    [description]
 */	 
 function smtSockTcpAccept($sock, &$newsock = NULL)
{ 
	app_func_struct($src);
	app_func_copy($src->sock, $sock); 
	if (false === ($src->client = socket_accept($src->sock)) )
	{
		socket_close($src->sock);
		return -1;
	} 
	
	if ($src->client && app_func_copy($newsock, $src->client))
	{ 
		return $src->client;
	}
	// finally return;
	return -1;  
}

/**
 * [smtSockTcpHost]
 * @param  [type] $address [description]
 * @return [type] $port    [description]
 */	
 function smtSockTcpHost($sock = NULL)
{
	if (socket_getpeername( $sock, $from_ip, $from_port ))
	{
		return $from_ip;
	}
	return -1;  
}

/**
 * [smtSockTcpPort]
 * @param  [type] $address [description]
 * @return [type] $port    [description]
 */	
 function smtSockTcpPort($sock)
{
	if (socket_getpeername( $sock, $from_ip, $from_port ))
	{
		return $from_port;
	}
	return -1;   
}

/**
 * [smtSockTcpSetReuse]
 * @param  [type] $address [description]
 * @return [type] $port    [description]
 */	
 function smtSockTcpSetReuse( $sock = NULL, $reuse = 1 )
{ 
	if (!app_func_sock($sock)) 
		return -1; 
	else if (false === @socket_set_option($sock, SOL_SOCKET, SO_REUSEADDR, $reuse))
	{
		return -1;
	}
	return 0;
}

/*!
 * \brief smtSockTcpSetRcvTimeout
 *
 * \link https://gist.github.com/brianlmoon/442310033bf44565bddd
 *
 * \param  [type] $address [description]
 * \retval [type] $port    [description]
 */	
 function smtSockTcpSetRcvTimeout( $sock = NULL, $sec = 0, $usec = 20)
{
	if (!app_func_sock($sock)) 
		return -1;
	else if (false === @socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, array( 'sec' => $sec, 'usec' => $usec) ))
	{
		return -1;	
	} 
	return 0;
}

/*!
 * \brief smtSockTcpSetSndTimeout
 *
 * \link https://gist.github.com/brianlmoon/442310033bf44565bddd
 *
 * \param  [type] $address [description]
 * \retval [type] $port    [description]
 */	
 function smtSockTcpSetSndTimeout(&$sock = NULL, $sec = 0, $usec = 20)
{
	if ( !app_func_sock($sock) ) 
		return -1;
	else if (false === @socket_set_option( $sock, SOL_SOCKET, SO_SNDTIMEO, array( 'sec' => $sec, 'usec' => $usec )))
	{
		return -1;	
	} 
	return 0;
}

/*!
 * smtSockTcpSetNonblock
 *
 * Sock Tcp non Blocking 
 *
 * @param  [type] $address [description]
 * @return [type] $port    [description]
 */	
 function smtSockTcpSetNonblock( $sock = NULL)
{
	if (!app_func_sock($sock)) 
		return -1;
	else if (false === @socket_set_nonblock($sock))
	{
		return -1;
	}
	return 0;
}

/*!
 * smtSockTcpSetLinger
 *
 * Socket Destroy socket on Wait Time 
 *
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 *
 */
 function smtSockTcpSetLinger($sock = NULL, $option = NULL, $flag = false )
{
	// create new object 
	$obj 		 = new stdClass();
	$obj->sock 	 = $sock;
	$obj->option = $option;
	$obj->flag 	 = $flag;
   
	// check option linger 
	if (!is_array( $obj->option))
	{
		return 0;	  
	}
   
	// check validation socket 
	if (!app_func_sock($obj->sock))
	{
		return -1;
	} 
   
	// set Option NonBlocking Socket 
	if ($obj->flag &&( false === socket_set_option($obj->sock, SOL_SOCKET, SO_LINGER, $obj->option)))
	{
		return -1;
	} 
   
	// finally return(0)
	return 0;
 }	 
 
/*!
 * smtSockTcpClose
 *
 * Socket Tcp close process
 *
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 *
 */
 function smtSockTcpClose($sock= NULL, $wait= 0) 
{
   /**
	* Create Local Objects 
	* default create objects 
	*/
  
	$obj 			= new stdClass(); 
	$obj->sock 		= $sock; 
	$obj->l_linger 	= $wait; 
	$obj->l_onoff  	= 1;
	
   /**
	* Options Linger 
	* default Set Linger if socket true 
	*/ 
	
	$obj->option = array( 
		'l_onoff'  => $obj->l_onoff, 
		'l_linger' => $obj->l_linger 
	);
  
   /**
	* Set Socket Linger options
	* for Set Linger Must Be check socket entity 
	*/ 
	
	smtSockTcpSetNonblock($obj->sock);
	if (!smtSockTcpSetLinger($obj->sock, $obj->option, true))
	{
		@socket_shutdown($obj->sock,2);
		@socket_close($obj->sock); 
	}
	// finally return 0;
	return 0;
}

/*!
 * smtSockTcpRead
 *  
 * Read All Buffer Until End
 * 
 * @param  [type] $sock  [description]
 * @param  [type] $lengh [description]
 * @return [type] $buff  [description]
 */
 
 function smtSockTcpRead(&$sock = NULL, &$buff= NULL, $size = 2048)
{
	$obj 		= new stdClass(); 
	$obj->sock 	= $sock; 
	$obj->buff 	= $buff;
	while($recv = @socket_read($obj->sock, $size)) 
	{
		$obj->buff .= $recv;
		if(strpos($obj->buff, "\n") !== false) 
			break;
	}
	
	// finaly return data EGAIN:
	app_func_copy($buff, $obj->buff);
	return $obj->buff;
}

/*!
 * smtSockTcpReadMtu
 *  
 * Read All Buffer Until End
 * 
 * @param  [type] $sock  [description]
 * @param  [type] $lengh [description]
 * @return [type] $buff  [description]
 */
 
 function smtSockTcpReadMtu(&$ntype= NULL, &$nread = NULL, &$nbyte=0 )
{
	$obj = new stdClass();
	$obj->nread  = $nread; 
	$obj->ntype  = $ntype;
	$obj->length = strlen($obj->nread);
	
	// then parsng signal messafe from this;
	$obj->check = trim( substr($obj->nread, (($obj->length) - 4), $obj->length));
	if ($obj->check != '' )
	{
		$obj->ntype = 'MTU';  
	}
	
	// adding to the this;
	$ntype = $obj->ntype;
	$nbyte = $obj->length;
	
	// finally return;
	return $ntype;
}

/*!
 * smtSockTcpReadDataLength
 *  
 * Read All Buffer Until End
 * 
 * @param  [type] $sock  [description]
 * @param  [type] $lengh [description]
 * @return [type] $buff  [description]
 */
 function smtSockTcpReadDataLength($sock, $len) 
{
	$off = 0;
	$buf = '';
	while ($off < $len)
	{
		if (($data = @socket_read ($sock, $len - $off, PHP_BINARY_READ)) === false) 
		{
			return false;	
		}
		
		$off += strlen ($data);
		$buf .= $data;
	}
	
	return $buf;
}

/*!
 * smtSockTcpReadLineTimeout
 *
 * \param  {s} 	recsource socket type 
 * \param  {q} 	buf string to write 
 * \param  {t}	wait timeout (time)
 * 
 * \retval {r} 	buf of string
 *
 */
 
 function smtSockTcpReadLineTimeout($s= NULL, $q= NULL, $t= 2)
{
	// create new object reference :
	$obj 	 = new stdClass(); 
	$obj->w  = NULL;
	$obj->e  = NULL;
	$obj->s  = $s;  
	$obj->q  = $q;
	$obj->t  = $t;
	$obj->r  = 0;
	
	// open selected socket by timeout if Timeout then return 0; 
	$obj->f = array($obj->s); 
	if ( @socket_select($obj->f, $obj->w, $obj->e, $obj->t,0)<1)
	{
		return 0;
	}
	
	// finally return socket ctiSockTcpReadLine;
	$obj->r = smtSockTcpReadLine( $obj->s, $obj->q ); 
	return $obj->r;
}

/*!
 * smtSockTcpReadLine
 *
 * Read line of data buffer process from socket 
 *
 * @param  [sock] 		$.sock		[recsource]
 * @param  [int] 		$.buff 		[size of buffer]
 * @param  [str] 		$.bufsize 	[length of buffer]
 * @return [str]     	$.r			[description]
 */	
 function smtSockTcpReadLine($sock = NULL, $buf= 1024, $nbuf= NULL)
{
	app_func_struct($obj);
	app_func_copy($obj->s, $sock); 
	app_func_copy($obj->n, $buf); 
	app_func_copy($obj->t, time());
	app_func_copy($obj->r, "");
	app_func_copy($obj->i, 0); 
	
	while ($obj->c = @socket_read($obj->s, $obj->n, PHP_BINARY_READ))
	{
		$obj->r .= $obj->c;  
		if ($obj->c == "\n") 
			break;
		else if ($obj->c == 0) 
		{
			if ($obj->i == 1) 
				return 0;
			else 
				break;
		}
		
		// jika sudah lebih dari 60 detik
		if ( (time()-$obj->t) >= 10 )
		{
			break;
		}
		
		// next Looping data :
		app_func_next($obj->i);
		
		// if slowly process connect to manager will set sleep 1s  
		if ( $obj->i>=5 ) sleep(1); 
	}
	
	return $obj->r; 
}