<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/**
 * [smtSockUdpServerCreate]
 * @param  [type] $host [description]
 * @param  [type] $port [description]
 * @param  [type] $queue [description]
 
 * @return [type]  [description]
 */	
 
 function smtSockUdpServerCreate(&$sock= false, $host =NULL, $port = 0, $sign = 0)
{ 
 // create new object 
 $out 		= new stdClass();
 $out->host = $host;  // default host 	
 $out->port = $port;  // default port 
 $out->opts = 1;	  // sock_options 	
 $out->sock = -1;	  // default sock	
 
 
 // SOL_TCP.sock.socket_set_option.SOCK_DGRAM
 if( false === ($out->sock = @socket_create(AF_INET, SOCK_DGRAM, 0) ) ){
   smt_log_app("Error SocketCreate() UDP.\n");
   return -1;
 }
 
 // SOL_TCP.sock.socket_set_option.SO_REUSEADDR
 if( false === @socket_set_option($out->sock, SOL_SOCKET, SO_REUSEADDR, $out->opts) ){
	smt_log_app("Error SetSockOption() Udp.\n"); 
	return -1;
 }
	
 // SOL_TCP.sock.socket_set_option.BIND 	
 if( false === @socket_bind($out->sock, $out->host, $out->port) ) {
   smt_log_app("Error SocketBind() Udp.\n"); 
   socket_close($out->sock); 
   return -1;
 }
 
 // then will copy from this sock 
 app_func_copy($sock, $out->sock);
 
 // finnaly return of data process is sock != NULL 
 return $out->sock;
}


/**
 * [smtSockUdpClientCreate]
 * @param  [type] $host [description]
 * @param  [type] $port [description]
 * @param  [type] $queue [description]
 
 * @return [type]  [description]
 */	
 function smtSockUdpClientCreate(&$sock = false ) 
{
	
// create new Obejct 
 $out = new stdClass();
 $out->sock = NULL;
  
 // create UDP socket for clients  
 if( false === ( $out->sock = @socket_create( AF_INET, SOCK_DGRAM, 0))) {
   smt_log_app("Error socket_create() UDP.\n");
   return -1;
 }
 
 // finnaly return  == 0;
 app_func_copy($sock, $out->sock);
 
 // finnaly return of data process is sock != NULL 
 return $out->sock;
}

/**
 * smtSockUdpReceive
 *  
 * Read Socket Replay from server Hits By clients Usage for syncronize 
 * process write then wait Until Response 
 *
 * @param  [type] $sock  [description] 
 * @return [type] $buff  [description]
 */ 
 function smtSockUdpReceive( &$sock = NULL, &$rcv = NULL, $size = 1024 )
{  
 if( false === @socket_recv ( $sock , $rcv , $size , MSG_WAITALL ) ){
	smt_log_app("Error socket_recv() UDP.\n"); 
	return 0;
 }
 // finnaly return of data process is sock != NULL 
 return strlen($rcv);
}

/**
 * smtSockUdpRead
 *  
 * Read All Buffer from message client disposition on Udp Socket server 
 * 
 * @param  [type] $sock  	[description]
 * @param  [type] $buff 	[description]
 * @param  [type] $size 	[description]
 */
 
 function smtSockUdpRead( &$sock = NULL, &$buf= NULL, &$ret = NULL, &$size = 0)
{
	Global $smt;	
	
	// create new local object
	app_func_struct($obj);
	app_func_copy($obj->sock, $sock);
	app_func_copy($obj->size, $size);
	app_func_copy($obj->nrcv, 0);
	
	
	// validation of buf size if < 0 get from configuration 
	// server settings 
	if (!$obj->size &&($smt->UdpBuff))
	{
		app_func_copy( $obj->size, 
			app_func_int($smt->UdpBuff)
		);
	}
 
	// validation ret is host & port 'app_func_maloc' */
	if (false !== is_null($ret))
	{
		app_func_maloc( $ret, 
			array(
				'sock' => 0,
				'host' => 0,
				'port' => 0,
				'buf'  => 0,	
				'len'  => 0
			)
		);
	}	 
 
	app_func_copy($ret->len, $obj->nrcv);
	app_func_copy($ret->sock, $obj->sock);
	
	/*! \remark read from udp socket client connected */
	if (($obj->nrcv = @socket_recvfrom($obj->sock, $buf, $obj->size, 0, $ret->host, $ret->port))<0 )
	{
		smt_log_app( "Error socket_recvfrom() UDP.\n");
		return 0;	
	} 
	
	app_func_copy($ret->len, $obj->nrcv);
	if ($obj->nrcv)
	{
		app_func_copy( $ret->buf, $buf );
	}
	
	/*! \remark finaly return is NUMBER */
	app_func_free($obj, NULL);
	return $ret->len;
}

/**
 * smtSockUdpRecv
 *  
 * 
 * 
 * @param  [type] $sock  	[description]
 * @param  [type] $buff 	[description]
 * @param  [type] $size 	[description]
 * @param  [type] $host 	[description]
 * @param  [type] $port 	[description]
 */
 
 function smtSockUdpRecv( &$sock , &$buff, $size = 1024)
{
  // create new local object
  $obj 		= new stdClass(); 
  $obj->sock = $sock; 
  $obj->nrcv = 0;
 
  // parse read from scket 
  if (( $obj->nrcv = @socket_recv($obj->sock, $buff, $size, 0 ))<0)
  {
	smt_log_app("Error socket_recv() UDP.\n");
	return 0;	
  }
  // finaly return is NUMBER 
  return (int)$obj->nrcv;
}

/**
 * smtSockUdpWrite
 *  
 * Read All Buffer Until End
 * 
 * @param  [type] $sock  [description]
 * @param  [type] $lengh [description]
 * @return [type] $buff  [description]
 */

 function smtSockUdpWrite(&$sock = NULL, $buff = NULL, $host = NULL, $port = NULL)
{
	Global $smt;
	
	// create new Struct object;
	app_func_struct($obj);
	app_func_copy($obj->sock, $sock);
	app_func_copy($obj->buff, $buff);
	app_func_copy($obj->host, $host);
	app_func_copy($obj->port, $port);
	app_func_copy($obj->sent, 0);
	app_func_copy($obj->send, 0);
	app_func_copy($obj->len,  0);
	
	// default port && host if null 
	if (is_null($obj->host))
	{
		app_func_copy($obj->host, $smt->UdpHost); 
	}
 
	// default port && host if null 
	if (is_null($obj->port))
	{
		app_func_copy($obj->port, $smt->UdpPort); 
	}
	
	// check of port socket 
	if (!$obj->port){
		return 0;
	}
	
	// check lengh of buffer to send 
	$obj->len  = strlen($obj->buff);
	if (!$obj->len){
	    return 0;
	}
	
	// next to sent via UDP socket 
	while ($obj->buff != '')
	{
		$obj->send = @socket_sendto( $obj->sock, $obj->buff, $obj->len, 0 , $obj->host, $obj->port);
		if ($obj->send === false) 
		{
		   smt_log_app("Error socket_sendto() UDP.\n");
		   break;
		}
		// fi success sent message 
		else if ($obj->send >= $obj->len) 
		{
		  $obj->buff = substr($obj->buff, $obj->send);	
		  $obj->sent += $obj->send;
			break;
		}
	}
	
	// finaly return is NUMBER 
	return $obj->sent;
}


/**
 * smtSockUdpClose
 *  
 * Read All Buffer Until End
 * 
 * @param  [type] $sock  [description] 
 * @return [type] $buff  [description]
 */
 
 function smtSockUdpClose(&$sock = NULL)
{ 
	if (is_resource($sock))
	{
		socket_close($sock);
		app_func_copy($sock, -1);
	}
	
	return $sock;
}

