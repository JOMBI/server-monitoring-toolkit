<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \retval <smt_vm_default> */
function app_webhook_content_default(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
	"<tr>".
		"<th colspan='6'>Information</th>".
	"</tr>" 
	); 
	
	app_func_output( $whook->output, 
		sprintf(
			"<tr>".
			" <td><b>ID</b></td>".
			" <td>:</td>".
			" <td>%s</td>".
			" <td><b>Name</b></td> ".
			" <td>:</td> ".
			" <td>%s</td>".
			"</tr>",
				$smt->AppStorage['smt_vm_id'],
				$smt->AppStorage['smt_vm_name']
		)
	);
	
	app_func_output( $whook->output, 
		sprintf(
			"<tr>".
			" <td><b>Host</b></td>".
			" <td>:</td>".
			" <td>%s</td>".
			" <td><b>Hostname</b></td> ".
			" <td>:</td> ".
			" <td>%s</td>".
			"</tr>",
				$smt->AppStorage['smt_vm_haddr'],
				$smt->AppStorage['smt_vm_hname']	
		)
	);
	
	app_func_output( $whook->output, 
		sprintf(
			"<tr>".
			" <td><b>Kernel</b></td>".
			" <td>:</td>".
			" <td>%s</td>".
			" <td><b>Updated</b></td> ".
			" <td>:</td> ".
			" <td>%s</td>".
			"</tr>",
				$smt->AppStorage['smt_vm_kernel'],
				date('Y-m-d H:i:s', $smt->AppStorage['smt_vm_update'])
		)
	); 
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_default}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	
	return 0;
}

/*! \retval <smt_vm_version> */
function app_webhook_content_version(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
	"<tr>".
		"<th colspan='2'>Operating System</th>".
	"</tr>" 
	); 
	
	$Version = $smt->AppStorage['vm_data']['smt_vm_version'];
	foreach ($Version as $key => $val) 
	{ 
		$key = ( $key == 'osi' ? 'os' : $key);
		app_func_output( $whook->output, 
			sprintf(
				"<tr>".
				"	<td><b>%s</b></td>".
				"	<td>%s</td> ".
				"</tr>", 
				$key, 
				$val 
			)
		);
	} 
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_version}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	return 0;
}


/*! \retval <smt_vm_uptime> */
function app_webhook_content_uptime(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan='4'>Uptime</th>".
		"</tr>" 
	); 
	
	app_func_output( $whook->output, 
		"<tr>".
		"	<td><b>Day</b></td>".
		"	<td><b>Hour</b></td>".
		"	<td><b>Min</b></td>".
		"	<td><b>Sec</b></td>".
		"</tr>"
	); 
	
	$Uptime = $smt->AppStorage['vm_data']['smt_vm_uptime'];
	app_func_output( $whook->output, 
	sprintf( 
			"<tr>".
			"	<td>%s</td>".
			"	<td>%s</td>".
			"	<td>%s</td>".
			"	<td>%s</td>".
			"</tr>",
			
			$Uptime['day'],
			$Uptime['hour'],
			$Uptime['min'],
			$Uptime['sec']
		)
	);  
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_uptime}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	return 0;
}


/*! \retval <smt_vm_average> */
function app_webhook_content_average(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan=5>Load Avg & CPU</th>".
		"</tr>"
	); 
	 
	
	$average = $smt->AppStorage['vm_data']['smt_vm_average'];
	foreach ($average as $dev => $r)
	{
		if ( $dev == 'avg') 
		{  
			app_func_output( $whook->output, 
				sprintf(
					"<tr>".
					"<td><b>Load Avg</b></td>".
					"<td>:</td>".
					"<td>%s</td>".
					"<td>%s</td>".
					"<td>%s</td>".
					"</tr> ",
						$r['vol1'],
						$r['vol2'],
						$r['vol3']
				)
			);
		}
		else {
			app_func_output( $whook->output, 
				sprintf(
					"<tr>".
					"<td><b>CPU</b></td>".
					"<td>:</td> ".
					"<td colspan=3>%s&nbsp;%%</td> ".
					"</tr>",
					round($r['percent'],2)
				)
			);
		}
	} 
	 
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_average}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	return 0;
}

/*! \retval <smt_vm_memory> */
function app_webhook_content_memory(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan=7>Memory</th>".
		"</tr>"
	); 
	 
	
	$memory = $smt->AppStorage['vm_data']['smt_vm_memory'];
	foreach ($memory as $mem => $r)
	{
		if ($mem == 'ram') 
		{ 
			app_func_output( $whook->output, 
				"<tr>".
				"	<td></td>".
				"	<td></td>".
				"	<td><b>Total</b></td>".
				"	<td><b>Used</b></td>".
				"	<td><b>Free</b></td>".
				"	<td><b>Used %</b></td>".
				"	<td><b>Free %</b></td>".
				"</tr>"
			);
			app_func_output( $whook->output, 
			sprintf(
				"<tr>".
				"	<td><b>$mem</b></td>".
				"	<td>:</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s&nbsp;%%</td>".
				"	<td>%s&nbsp;%%</td>".
				"</tr> ", 
					$r['total'],
					$r['t_used'],
					$r['t_free'],
					$r['percent'],
					$r['p_free']
				)
			);	
		} 
		
		if ($mem == 'swap') 
		{
			$s_total = $r['total'];
			$s_free  = $r['free'];
			$s_used  = $r['used'];
			$s_usep  = round( (($s_used/$s_total) *100),0);
			$s_frep  = round( (($s_free/$s_total) *100),0);
			
			app_func_output( $whook->output, 
			sprintf(
				"<tr>".
				"	<td><b>$mem</b></td>".
				"	<td>:</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s&nbsp;%%</td>".
				"	<td>%s&nbsp;%%</td>".
				"</tr> ", 
					$s_total,
					$s_used,
					$s_free,
					$s_usep,
					$s_frep
				)
			);
		} 
	} 
	 
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_memory}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	return 0;
}

/*! \retval <smt_vm_hardisk> */
function app_webhook_content_hardisk(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan=8>Hardisk</th>".
		"</tr>"
	); 
	
	app_func_output($whook->output, 
		"<tr>".
		"<td></td>".
		"<td></td>".
		"<td><b>Disk</b></td>".
		"<td><b>Size</b></td>".
		"<td><b>Used</b></td>".
		"<td><b>Free</b></td>".
		"<td><b>Used %</b></td>".
		"<td><b>Free %</b></td>".
		"</tr>"
	);
	
	$hardisk = $smt->AppStorage['vm_data']['smt_vm_hardisk'];
	foreach ($hardisk as $hdd => $r)
	{
		$r['puse'] =  round( (($r['used']/$r['size']) *100),0);
		$r['pfre'] =  round( (($r['free']/$r['size']) *100),0);
		
		/*! \remark create Body */
		app_func_output($whook->output, 
			sprintf(
			"<tr>".
			"<td><b>%s</b></td>".
			"<td>:</td>".
			"<td>%s</td>".
			"<td>%s</td>".
			"<td>%s</td>".
			"<td>%s</td>". 
			"<td>%s&nbsp;%%</td> ".
			"<td>%s&nbsp;%%</td> ".
			"</tr>",
				$hdd,
				$r['disk'],
				$r['size'],
				$r['used'],
				$r['free'],
				$r['puse'],
				$r['pfre']
			)
		);
	}
			
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_hardisk}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	 
	return 0;	
}

/*! \retval  <smt_vm_network> */
 function app_webhook_content_network(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
		"<th colspan=10>Network</th>".
		"</tr>"
	); 
	
	app_func_output( $whook->output, 
		"<tr>".
		"<td></td>".
		"<td></td>".
		"<td><b>Rx_Bytes</b></td>".
		"<td><b>Rx_Packets</b></td>".
		"<td><b>Rx_Errs</b></td> ".
		"<td><b>Rx_Drop</b></td>".
		"<td><b>Tx_Bytes</b></td>".
		"<td><b>Tx_Packets</b></td>".
		"<td><b>Tx_Errs</b></td>".
		"<td><b>Tx_Drop</b></td> ".
		"</tr>"
	);
	
	
	$networks = $smt->AppStorage['vm_data']['smt_vm_network'];
	foreach ($networks as $network => $r)
	{
		app_func_output( $whook->output, 
			sprintf
			(
				"<tr>".
				"<td><b>%s</b></td>".
				"<td>:</td> ".
				"<td>%s</td>".
				"<td>%s</td>".
				"<td>%s</td> ".
				"<td>%s</td> ".
				"<td>%s</td> ".
				"<td>%s</td> ".
				"<td>%s</td> ".
				"<td>%s</td> ".
				"</tr>",
					$network,
					$r['rx_bytes'],
					$r['rx_packets'],
					$r['rx_errs'],
					$r['rx_drop'],
					$r['tx_bytes'],
					$r['tx_packets'],
					$r['tx_errs'],
					$r['tx_drop']
			)
		);
	} 
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_network}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	  
	return 0;
}

/*! \retval <smt_vm_service>*/
 function app_webhook_content_service(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan=10>Progname & Service</th>".
		"</tr>"
	); 
	
	app_func_output( $whook->output, 
		"<tr>".
		"	<td><b>Prog Name</b></td>".
		"	<td><b>Service</b></td>".
		"	<td><b>PID</b></td>".
		"	<td><b>Status</b></td>".  
		"</tr>" 
	);
	
	
	$service = $smt->AppStorage['vm_data']['smt_vm_service'];
	foreach ($service as $svc => $r)
	{
		$r['prog']  = $svc;
		$r['state'] = ($r['state']? 'Running' : '-'); 
		$r['pid']   = ($r['pid']? $r['pid'] : '-'); 
		
		app_func_output( $whook->output, 
			sprintf(
				"<tr>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".
				"	<td>%s</td>".  		 
				"</tr> ",
				$r['prog'],
				$r['name'],
				$r['pid'],
				$r['state']
			)
		);
	}		
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_service}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_vm_cpuinfo> */
 function app_webhook_content_cpuinfo(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
			"<th colspan=2>CPU Information</th>".
		"</tr>"
	); 
	 
	$cpuinfo = $smt->AppStorage['vm_data']['smt_vm_cpuinfo'];
	foreach ($cpuinfo as $key => $val )
	{
		$val = ( $val ? $val : "...");
		app_func_output( $whook->output, 
			sprintf(
			"<tr>".
				"<td><b>%s</b></td>".
				"<td>%s</td>".
			"</tr>",
				$key,
				$val
			)
		); 
	}		
	  
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_cpuinfo}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_vm_netmon> */
 function app_webhook_content_netmon(&$output = NULL)
{
	Global $smt;
	
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! \retval create Header */
	app_func_output( $whook->output, 
		"<tr>".
		"<th colspan=10>Network AVG / Second </th>".
		"</tr>"
	); 
	
	app_func_output( $whook->output, 
		"<tr>".
		"<td>&nbsp;</td>". 
		"<td><b>Tx_Bytes</b></td>".
		"<td><b>Rx_Bytes</b></td>". 
		"</tr>"
	);
	  
	app_func_struct($o);
	app_func_copy($o->_values , array());
	app_func_copy($o->_result , array());
	
	
	/*! \retval copy data to local storage */
	app_func_copy($o->network, 
		$smt->AppStorage['vm_data']['smt_vm_network']
	);
	
	if( is_array($o->network)) foreach ($o->network as $eth => $r)
	{
		 if ( $o->tx = new SMTAppAverage($o->_values['tx'], 10)) 
		{
			$o->tx->add($r['tx_bytes']);
		}
		
		 if( $o->rx = new SMTAppAverage($o->_values['rx'], 10))
		{
			$o->rx->add($r["rx_bytes"] );
		}
		
		/*! \retval Inject data to local object */ 
		$o->_result[$eth]['tx_bytes'] = $o->tx->average();
		$o->_result[$eth]['rx_bytes'] = $o->rx->average();
	} 
	
	
	foreach ($o->_result as $j => $q)
	{
		$o->avg_tx_byte = round($q['tx_bytes'],3);
		$o->avg_rx_byte = round($q['rx_bytes'],3); 

		app_func_output( $whook->output, 
			sprintf
			(
				"<tr>".
				"<td><b>$j</b></td> ". 
				"<td>%s Kb/s</td> ".
				"<td>%s Kb/s</td> ". 
				"</tr>",
				$o->avg_tx_byte,
				$o->avg_rx_byte
			)
		);
	}
	
	
	/*! \remark repalace content */
	if ( !app_func_replace('{{smt_vm_netmon}}', $output, $whook->output))
	{
		app_func_free($whook);
	}
	
	app_func_free($o);  
	return 0; 
}
