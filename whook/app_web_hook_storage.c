<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 


/*! \retval <smt_stg_default> */
 function app_webhook_storage_default(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	app_func_copy($whook->output, json_encode($smt->AppStorage));
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_default}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_memory> */
 function app_webhook_storage_memory(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->memory($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_memory}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_hardisk> */
 function app_webhook_storage_hardisk(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");

	
   /*! 
	* \remark Agar Lebi Cepat Sebaiknya Ambil ke Local Storage 
	* yang sudah di Update 
	*/
	app_func_alloc($whook->buf); 
	app_func_copy($whook->buf, 'event' , 'Hardisk');
	app_func_copy($whook->buf, 'vmid'  , $smt->AppStorage['smt_vm_id']);
	app_func_copy($whook->buf, 'time'  , $smt->AppStorage['smt_vm_update']);
	app_func_copy($whook->buf, 'data'  , $smt->AppStorage['vm_data']['smt_vm_hardisk']);
	
	if (app_func_copy($whook->output, json_encode($whook->buf)) )
	{
		app_func_free($whook->buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_hardisk}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}
/*! \retval <smt_stg_uptime> */
 function app_webhook_storage_uptime(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->uptime($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_uptime}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_average> */
 function app_webhook_storage_average(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->average($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_average}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_network> */
 function app_webhook_storage_network(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->network($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_network}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}
/*! \retval <smt_stg_service> */
 function app_webhook_storage_service(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->service($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_service}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_cpuinfo> */
 function app_webhook_storage_cpuinfo(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->cpuinfo($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_cpuinfo}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}
/*! \retval <smt_stg_version> */
 function app_webhook_storage_version(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->version($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_version}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}

/*! \retval <smt_stg_vendor> */
 function app_webhook_storage_vendor(&$output = NULL)
{
	Global $smt; 
	
	/*! \remark repalace content */
	app_func_struct($whook);
	app_func_copy($whook->output, "");
	
	/*! Untuk Mendaptkan storage Masing ID */
	if( $smt->AppProc->vendor($buf) )
	{
		app_func_copy($whook->output, json_encode($buf));
		app_func_free($buf);
	}
	
	/*! \remark Binding / Export data to this tag */
	if ( !app_func_replace('{{smt_stg_vendor}}', $output, $whook->output))
	{
		app_func_free($whook);
	} 
	
	return 0;
}


