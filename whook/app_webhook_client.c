<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 
/*! \remark app_webhook_client_sighup <smt, client, pid > */
 function app_webhook_client_sighup(&$smt, &$s = NULL )
{
	if (!smtSockTcpClose($s->sock))
	{
		if (smtAppClientDeleted($smt, $s)){
			unset($smt->AppSocket[$s->id]); 
		}
	} 
	
	return $s->id;
 }
 
 
/*! \remark app_webhook_client_thread <smt, client, pid > */
 function app_webhook_client_thread(&$smt = NULL, &$s= NULL)
{
	if (smtAppCreateThread($smt,'app_webhook_client_handler', array(&$smt, &$s), $s->pid))
	{
		return $s->pid;		
	}		
	
	return 0;
 }
 
 
/*! \remark app_webhook_client_handler */
 function app_webhook_client_handler(&$smt, &$s= NULL )
{
	/*! \remark header status */
	app_webhook_header_ishttp($s);
	if ( !strcmp($s->http, 'off'))
	{
		app_webhook_client_sighup($smt, $s); 
		return $s->id;
	}
	
	/*! \remark Header <parsing> Process valid dan Lamjutkan ke step berikutnya */
	if ( !app_webhook_header_parsing($s))
	{
		app_webhook_client_sighup($smt, $s); 
		return $s->id;
	}
	
	/*! \remark not contribute Allocator */
	if ( !app_webhook_header_param($s,'Allow'))
	{
		app_webhook_client_sighup($smt, $s); 
		return $s->id;
	}
	
	/*! \remark sent to case reserved response */
	app_func_struct($o);
	if (app_webhook_response($smt, $s, $o))
	{ 
		if (smtSockTcpWrite($s->sock, $o->output)) {
			app_func_free($o);
		} 
	}
   /*! \remark karena ini sifatnya state less maka socket langsug 
	*  di destroy atau close saja 
	*/
	smtSockTcpClose($s->sock); 
	return $s->id;				
}