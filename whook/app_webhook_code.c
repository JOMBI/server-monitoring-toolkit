<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 if( !defined( 'DEFAULT_RESPONSE') ) define( 'DEFAULT_RESPONSE', 'UKNOWN' );
 
 function app_webhook_init_code(&$smt = NULL)
{
  
  /*! \note for Aksess Global Data */  
  $smt->WebHookHeaderResponse = array
  (
    // Informational 1xx
    100 => 'Continue',
    101 => 'Switching Protocols',
    // Success 2xx
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',

    // Redirection 3xx
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found', // 1.1
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'is deprecated but reserved',
    307 => 'Temporary Redirect',

    // Client Error 4xx
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',

    // Server Error 5xx
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    509 => 'Bandwidth Limit Exceeded'
 );
}

/*! app_webhook_get_code */
 function app_webhook_get_code($code= 200, &$ret= false)
{
	Global $smt;
	
	/*! \remark crate Local Object */
	
	app_func_struct($out);
	app_func_copy($out->ResponseCode , $code);
	app_func_copy($out->ResponseText , DEFAULT_RESPONSE);
	app_func_copy($out->ResponseProgName , $smt->AppProgUid);
	app_func_copy($out->ResponseProgCopy , $smt->AppProgCopy);
	app_func_copy($out->ResponseProgTitle , $smt->AppProgTitle);
	app_func_copy($out->ResponseProgServer , $smt->AppProgName);
	app_func_copy($out->ResponseProgVersion , $smt->AppProgVer); 
	
	if (isset($smt->WebHookHeaderResponse))
	{
		if (isset($smt->WebHookHeaderResponse[$out->ResponseCode]))
		{
			/*! \retval set response Handler */	
			app_func_copy( $out->ResponseText, 
				$smt->WebHookHeaderResponse[$out->ResponseCode]
			);
		}
	}
	/*! \retval finaly text code */
	if ( app_func_copy($ret, $out) ) 
	{
		return $out->ResponseText;
	}
	return 0;
}

/*! app_webhook_code_header */
 function app_webhook_set_code(&$o= NULL, $c= NULL)
{
	if ( is_object($o) )
	{
		/*! \remark set default */
		app_func_copy($o->ResponseCode,0);
		app_func_copy($o->ResponseText,'UKNOWN');
		
		if (app_webhook_get_code($c, $retval))
		{
			app_func_copy($o->ResponseCode, $retval->ResponseCode);
			app_func_copy($o->ResponseText, $retval->ResponseText);
			app_func_copy($o->ResponseProgName, $retval->ResponseProgName);
			app_func_copy($o->ResponseProgCopy , $retval->ResponseProgCopy);
			app_func_copy($o->ResponseProgTitle, $retval->ResponseProgTitle);
			app_func_copy($o->ResponseProgServer, $retval->ResponseProgServer);
			app_func_copy($o->ResponseProgVersion, $retval->ResponseProgVersion);
		}
	}	
	
	return $o;	
}