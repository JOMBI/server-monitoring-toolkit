<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! app_webhook_header_ishttp */ 
 function app_webhook_header_ishttp(&$s)
{
	app_func_copy($s->http, 'off');
	if ( stristr($s->nread, 'HTTP') )
	{
		app_func_copy($s->http, 'on');
	}
	return;
}


/*! app_webhook_header_param */
 function app_webhook_header_param($s, $q)
{
	/*! \remark object tidak ada dalam header */
	if ( !isset($s->header) ){
		return NULL;
	}		
	
	/*! \remark jika header parameter tidak ada */
	if( !isset($s->header[$q]) )
	{
		return "";
	}
	
	/*! \remark jika header parameter tidak ada */
	return $s->header[$q]; 
}



/*! app_webhook_header_parsing */
 function app_webhook_header_parsing(&$s)
{
	app_func_struct($h);
	app_func_alloc($h->c);
	
	if (app_func_explode($h->q, $s->nread, "\n"))
	{	
		/*! \remark get Allow:type */	
		app_func_copy($h->c, 'Allow', 'html'); 
		if (sscanf($h->q[0],"%s /%s ", $Method, $Allow))
		{
			app_func_copy($h->c, 'Method', $Method); 
			app_func_copy($h->c, 'Allow', $Allow); 
			app_func_copy($h->c, 'File', $Allow);
		}
		
		foreach ($h->q as $j => $r)
		{
			$h->s = array_map('trim', explode(chr(58), $r));
			if (app_func_sizeof($h->s)< 2)
			{
				continue;
			} 
			
			app_func_copy( $h->c, $h->s[0], $h->s[1] );
		}
	}
	
	/*! convert data to object */
	if (app_func_copy($s->header, $h->c))
	{
		app_func_free($s->nread);
		app_func_free($h);
	}
	return $s->header;
}