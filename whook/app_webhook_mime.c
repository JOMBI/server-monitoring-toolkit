<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \retval app_webhook_init_mime */ 
 function app_webhook_init_mime(&$smt = NULL)
{
   $smt->WebHookMimeResponse = array
	(
		'html' => 'text/html charset=utf-8',
		'text' => 'text/html charset=utf-8',
		'envc' => 'application/x-www-form-urlencoded',
		'json' => 'application/json; charset=UTF-8',
		'icon' => 'image/x-icon; charset=UTF-8'
	);
	
	return 0;
}

/*! \retval app_webhook_get_mime */
 function app_webhook_get_mime($type = NULL, &$ret = NULL)
{
	Global $smt;
	if (!is_array($smt->WebHookMimeResponse))
	{
		return 0;
	}
	
	if(isset($smt->WebHookMimeResponse[$type]))
	{
		app_func_copy($ret,$smt->WebHookMimeResponse[$type]);
	} 
	
	return $ret;
}

/*! \retval app_webhook_set_mime */
 function app_webhook_set_mime(&$s = NULL, $type = "")
{
	app_func_copy($s,'text/html charset=utf-8');
	if(app_webhook_get_mime($type, $retval))
	{
		app_func_copy($s,$retval);
		app_func_free($retval);
	}
	
	return $s;
}