<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 if(!defined('HEADER_RESPONSE_200')) define('HEADER_RESPONSE_200', 200);
 if(!defined('HEADER_RESPONSE_403')) define('HEADER_RESPONSE_403', 403);
 if(!defined('HEADER_RESPONSE_404')) define('HEADER_RESPONSE_404', 404);
 
 
/*! app_webhook_response */ 
 function app_webhook_response(&$smt= NULL, &$s = NULL, &$o = NULL)
{
	/*! \remark set Init default code */
	app_webhook_init_mime($smt);
	app_webhook_init_code($smt);
	
	/*! \remark set Init default code */
	if (function_exists( 'app_webhook_set_code' ))
	{ 
		/*! app_webhook_set_code( ref, code) */
		app_webhook_set_code($o, HEADER_RESPONSE_200);
	}
	
	
  /*! 
	* \remark for Response data html 
	* By default set All Argument requested from clients session ,
	* If requested Fail return fail. And Close Session 
	*
	*/
	
	app_func_copy($o->Agent , app_webhook_header_param($s, 'User-Agent'));
	app_func_copy($o->Allow , app_webhook_header_param($s, 'Allow'));  
	app_func_copy($o->File  , app_webhook_header_param($s, 'File'));
	
	while($smt)
	{
		
		/*! \retval dump log Agent request */
		smt_log_app(sprintf("%s:%06d - Session #%d User-Agent: %s", $s->host, $s->port, $s->id, $o->Agent));
		
		/*! \retval set path to process */
		if (!app_webhook_response_path($smt, $o, $s))
		{
			app_webhook_set_code($o, HEADER_RESPONSE_404);
			if (app_webhook_response_error($o))
			{
				smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
				app_func_output($o->output, "\n");	
			} 
			
			/*! \retval hits and stop */
			break;
		}
		
		/*! \retval for Response data svm */
		if (stristr($o->Allow, '.svm'))
		{
			app_webhook_set_code($o, HEADER_RESPONSE_200);
			if (app_webhook_response_svm($o)) 
			{
				smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
				app_func_output($o->output, "\n");	
			}
			
			/*! \retval hits and stop */
			break;
		}
		
		/*! \retval for Response data html */
		if (stristr($o->Allow, '.html'))
		{
			app_webhook_set_code($o, HEADER_RESPONSE_200);
			if (app_webhook_response_html($o)) 
			{
				smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
				app_func_output($o->output, "\n");	
			}
			
			/*! \retval hits and stop */
			break;
		} 
		
		/*! \retval for Response data JSON */
		if (stristr($o->Allow, '.sjs') ) 
		{
			app_webhook_set_code($o, HEADER_RESPONSE_200);
			if(app_webhook_response_sjs($o))
			{
			   smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
			   app_func_output($o->output, "");	
			}
			
			/*! \retval hits and stop */
			break;
		}
		
		/*! \retval for Response data .Ico*/
		if (stristr($o->Allow, '.ico'))
		{
			app_webhook_set_code($o, HEADER_RESPONSE_200);
			if (app_webhook_response_icon($o)) 
			{
				smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
				app_func_output($o->output, "\n");	
			}
			
			/*! \retval hits and stop */
			break;
		} 
		
		
		
		/*! \return Not Allowed By Default */
		app_webhook_set_code($o, HEADER_RESPONSE_404);
		if (app_webhook_response_error($o))
		{
			smt_log_app(sprintf("%s:%06d - Session #%d Response %d %s", $s->host, $s->port, $s->id, $o->ResponseCode, $o->ResponseText)); 
			app_func_output($o->output, "\n");	
		}
		/*! \retval hits and stop */
		break;
	}
	
	/*! \retval sent response */
	return $s->id;
}

/*! \remark app_webhook_response_data */
 function app_webhook_response_data(&$o)
{
	app_webhook_content_default($o->output);
	app_webhook_content_version($o->output);
	app_webhook_content_uptime($o->output);
	app_webhook_content_average($o->output);
	app_webhook_content_memory($o->output);
	app_webhook_content_hardisk($o->output);
	app_webhook_content_network($o->output);
	app_webhook_content_service($o->output);
	app_webhook_content_cpuinfo($o->output);
	app_webhook_content_netmon($o->output);
	
	return $o;
} 


/*! \remark app_webhook_response_user */
 function app_webhook_response_user(&$o, &$content= NULL)
{
	app_func_replace('{{smt_prog_title}}',  $content, $o->ResponseProgTitle);
	app_func_replace('{{smt_prog_name}}',   $content, $o->ResponseProgName);
	app_func_replace('{{smt_prog_version}}',$content, $o->ResponseProgVersion);
	app_func_replace('{{smt_prog_copy}}',   $content, $o->ResponseProgCopy); 	
	return $o;
} 


/*! app_webhook_response_path */ 
 function app_webhook_response_path(&$smt = NULL, &$o= NULL, &$s = NULL)
{
	/*! \remark don't show path aksess */
	smt_log_app(sprintf("%s:%06d - Session #%d Client Request '/%s'", $s->host, $s->port, $s->id, $o->File));  
	
	
	/*! \retval check path to process */
	$o->Path = sprintf("%s/%s", $smt->CfgPathPub, $o->File); 
	if (!file_exists($o->Path))
	{
		smt_log_app(sprintf("%s:%06d - Session #%d Error File '%s' File Not Found", $s->host, $s->port, $s->id, $o->Path));
		return 0;
	}
	
	return $o->Path;	 
 }


/*! app_webhook_response_header */
 function app_webhook_response_header(&$o)
{
	/*! \retval Set default parameter */
	app_func_copy($o->ResponseDate, date('d M Y H:i:s'));
	app_func_copy($o->ResponseConnection,'close');
	app_func_copy($o->ResponseAllowOrigin,'*');
	app_func_copy($o->ResponseTimeout,30);
	 
	/*! \retval Create Output for Header */
	
	app_func_output($o->output, sprintf("HTTP/1.0 %d %s\r\n", $o->ResponseCode, $o->ResponseText));
	app_func_output($o->output, sprintf("Date: %s\r\n", $o->ResponseDate));
	app_func_output($o->output, sprintf("Server: %s/%s\r\n", $o->ResponseProgName, $o->ResponseProgVersion));
	app_func_output($o->output, sprintf("Copyright: %s\r\n", $o->ResponseProgCopy));
	app_func_output($o->output, sprintf("Connection: %s\r\n", $o->ResponseConnection)); 
	app_func_output($o->output, sprintf("Keep-Alive: timeout=%d, max=%d\r\n", $o->ResponseTimeout, $o->ResponseTimeout * 2)); 
	app_func_output($o->output, sprintf("Allow-Origin: %s\r\n", $o->ResponseAllowOrigin));
	app_func_output($o->output, sprintf("Content-Type: %s\r\n", $o->ResponseContentType));
	app_func_output($o->output, "\r\n"); 
	
	/*! \retval Response Code Of Header */
	return $o->ResponseCode;
}


/*! app_webhook_response_error */
 function app_webhook_response_error(&$o)
{
	/*! \retval set Mime Content-Type */
	if( function_exists('app_webhook_set_mime'))
	{
		app_webhook_set_mime($o->ResponseContentType, 'html');
	}
	
	if ( app_webhook_response_header($o) )
	{
		/*! \remark body make content  */
		app_func_output( $o->output, 
			sprintf( "<html> <title>%s</title>\n", 
				$o->ResponseProgTitle
			
			)
		);
		
		/*! \remark body make Atrribute JS OR order  */
		app_func_output( $o->output, 
			"<head>\n".
				"<style>\n".
					"html, body {".
					"font-family: sans-serif;".
					"font-size:13px;".
					"color: #444; ".
					"margin:10%; ".
					"}".
				"</style>".
			"</head>"
		); 
		
		/*! \remark Build Body Content-Type */
		app_func_output( $o->output, 
			sprintf("<body>\n<center>%s Version %s</center>\n<center><h3>%d - %s</h3></center>\n", 
				$o->ResponseProgTitle, $o->ResponseProgVersion, $o->ResponseCode, $o->ResponseText
			)
		);
		
		app_func_output($o->output, "</body>\n"); 
		app_func_output($o->output, "</html>"); 
		
	}
	/*! \retval final Response */
	return $o;
}
 
/*! app_webhook_response_html */
 function app_webhook_response_html(&$o)
{
	/*! \retval set Mime Content-Type */
	if( function_exists('app_webhook_set_mime'))
	{
		app_webhook_set_mime($o->ResponseContentType, 'html');
	}
	
	/*! \retval get Header process */
	if ( app_webhook_response_header($o) )
	{
		$content = file_get_contents($o->Path);
		if (app_webhook_response_user($o, $content))
	    {
		   /*! 
		    * \remark Untuk Sementara tidak di check dulu langsung 
			* di replace maklum kejar tayang 
			*
			*/
			if ( app_func_output($o->output, $content) )
			{
				app_webhook_response_data($o);
			}
			
			/*! \remark hapus jika sudah selesai */
			app_func_free($content);
		}
	}
	
	/*! \retval final Response */
	return $o;
}

/*! app_webhook_response_svm */
 function app_webhook_response_svm(&$o)
{
	/*! \retval set Mime Content-Type */
	if( function_exists('app_webhook_set_mime'))
	{
		app_webhook_set_mime($o->ResponseContentType, 'text');
	}
	
	/*! \retval get Header process */
	if (app_webhook_response_header($o))
	{
		$content = @file_get_contents($o->Path); 
		if (app_webhook_response_user($o, $content))
	    {
		   /*! 
		    * \remark Untuk Sementara tidak di check dulu langsung 
			* di replace maklum kejar tayang 
			*
			*/
			if (app_func_output($o->output, $content))
			{
				app_webhook_response_data($o);
			}
			
			/*! \remark hapus jika sudah selesai */
			app_func_free($content);
		}
	}
	
	/*! \retval final Response */
	return $o;
}
 
/*! \retval app_webhook_response_icon */
function app_webhook_response_icon(&$o)
{   
	/*! \retval set Mime Content-Type */
	if( function_exists('app_webhook_set_mime'))
	{
		app_webhook_set_mime($o->ResponseContentType, 'icon');
	}
	
	
	/*! \retval get Header process */
	if ( app_webhook_response_header($o) )
	{
		$content = @file_get_contents($o->Path); 
		if( app_func_output($o->output, $content) ){
			app_func_free($content);
		}
	}
	
	/*! \retval final Response */
	return $o;
}

/*! app_webhook_response_sjs */
 function app_webhook_response_sjs(&$o)
{
	/*! \retval set Mime Content-Type */
	if( function_exists('app_webhook_set_mime'))
	{
		app_webhook_set_mime($o->ResponseContentType, 'json');
	}
	
	/*! \retval get Header process */
	if ( app_webhook_response_header($o) )
	{
		$content = @file_get_contents($o->Path); 
		if( app_func_output($o->output, $content) )
		{
			app_webhook_storage_default($o->output); 
			app_webhook_storage_version($o->output);
			app_webhook_storage_vendor($o->output);
			app_webhook_storage_uptime($o->output);
			app_webhook_storage_average($o->output);
			app_webhook_storage_network($o->output);
			app_webhook_storage_service($o->output);
			app_webhook_storage_cpuinfo($o->output); 
			app_webhook_storage_memory($o->output);
			app_webhook_storage_hardisk($o->output);
			
			app_func_free($content);
		}
	}
	
	/*! \retval final Response */
	return $o;
	
}