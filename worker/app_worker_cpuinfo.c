<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \brief smtAppCpuinfoSender */ 
 function smtAppCpuinfoSender(&$udp = NULL, &$proc = NULL, &$buf = NULL)
{ 	
    if (app_func_tick($udp->Time) >= $udp->Intv)
	{
		/*! \retval reset Tick Time */
		app_func_copy($udp->Time, time()); 
		if (app_func_encode($proc, $s) && app_func_sock($udp->Sock))
		{ 
			app_func_stream($buf, sprintf("Action:%s", $s->event));
			app_func_stream($buf, sprintf("Data:%s", $s->data));
			
			if (app_func_enter($buf))
			{
				smtSockUdpWrite($udp->Sock, $buf, $udp->Host, $udp->Port);
			}
			
			/*! \remark clear Allocation Memory */
			app_func_free($s);
		}
    }
	
	/*! \retval integer (0) */
	return 0;
}


/*! \brief smtAppWorkerCpuinfo */ 
 function smtAppWorkerCpuinfo($smt = false)
{ 	
	/*! \retval Write PID Process from this section */	
	if ($pid = getmypid())
	{
		smt_log_info(sprintf("Start Worker Thread.WorkerCpuInfo(%d)", $pid), 
		__FILE__, __LINE__ );
	}
	
	/*! 
    * \remark 
    * Definisikan Semua data Untuk Keperluan socket UDP 
	* sock, host, port , time , interval
	*/
	
	app_func_struct($udp);
	app_func_copy($udp->Sock, 0);
	app_func_copy($udp->Intv, 20);
	app_func_copy($udp->Time, time());
	app_func_copy($udp->Host, $smt->UdpHost);
	app_func_copy($udp->Port, $smt->UdpPort);
	
	if (smtSockUdpClientCreate($udp->Sock) < 0)
	{
		smt_log_app("Create 'smtSockUdpClientCreate()' Fail.");
		if (smtSockUdpClose($udp->Sock))
			app_func_copy($udp->Sock,0);
	}
	
	/*! \retval Init for Looping Timer */
	app_func_struct($out);
	app_func_alloc($out->waits);
	
	while($smt)
	{
		/*! if Not found Kill Process */
		usleep(2 * 1000000); 
		if (!app_fork_pid($smt->AppProgPid))
		{
			app_fork_kill( $pid, SIGKILL );
		}
		
		/*! \remark  tested get Uptime process */
		app_func_free($buf);
		if ($smt->AppProc->cpuinfo($buf))
		{
		    /*!
			* \remark sent message to SMT Application 
			*
			* Thread On Periode will sent data to SMT Master Application for 
			* Generate file On Local Storage , for service request by client side 
			* 
			* Sent Message Via Udp Socket 
			*
			* \link https://www.binarytides.com/udp-socket-programming-in-php/
			*
			*/
			
			if ( !smtAppCpuinfoSender($udp, $buf, $udp->buf) )
			{
				app_func_free($udp->buf);
			}
			
		   /*!
			* \retval sent message to UDP channel for 
			* Public Aksess 
			*/
			
			/*! \retval sent to Media channel integrator */
			if (function_exists('smtAppEventChannel'))
			{
				app_func_free($configHandler);
				if (!app_cfg_stream('AppChannel', 'cpuinfo', $configHandler))
				{
				   app_func_free($buf);
				   continue;
				} 
				
				foreach ($configHandler as $event => $eventHandler)
				{
					/*! \remark Parsing Event Handler */
					app_func_free($Handler);
					if (app_cfg_handler($eventHandler, $Handler))
					{
						/*! \remark dump channel events */
						if (!isset($out->waits[$event])) 
						{
							$out->waits[$event]['freq'] = $Handler[0];
							$out->waits[$event]['wait'] = time();
						}
						
						/*! \remark dump wait of time */
						if ((time() - $out->waits[$event]['wait']) >= $out->waits[$event]['freq'])
						{
							/*! \remark Update Time every loop */
							$out->waits[$event]['wait'] = time();
							
							/*! \remark must be implmented */
							smtAppEventChannel($smt, $buf, array($Handler[1] => $Handler[2])); 
							app_func_free($buf);
						} 
					}
				}
			} 
			
			/*! \retval < app_func_free > */
			app_func_free($buf);
		}
	}
	
	return 0;
}
