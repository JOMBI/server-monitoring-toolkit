<?php if(!defined('SMT_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
 
 /*! \brief smtAppStorageSender */ 
 function smtAppStorageSender(&$udp = NULL, &$proc = NULL, &$buf = NULL)
{ 	
	/*! \remark first indikator */
	if (!$udp->Loop && app_func_encode($proc, $s))
	{
		app_func_copy($udp->Time, time()); 
		if (app_func_sock($udp->Sock))
		{
			app_func_stream($buf, sprintf("Action:%s", $s->event));
			app_func_stream($buf, sprintf("Data:%s", $s->data));
			
			if (app_func_enter($buf))
			{
				smtSockUdpWrite($udp->Sock, $buf, $udp->Host, $udp->Port);
			}
			
			app_func_free($s);
		} 
		
		return 0;
	}
	
	/*! \remark second indikator */
    if (app_func_tick($udp->Time) >= $udp->Intv)
	{
		app_func_copy($udp->Time, time()); 
		if (app_func_encode($proc, $s) && app_func_sock($udp->Sock))
		{ 
			app_func_stream($buf, sprintf("Action:%s", $s->event));
			app_func_stream($buf, sprintf("Data:%s", $s->data));
			
			if (app_func_enter($buf))
			{
				smtSockUdpWrite($udp->Sock, $buf, $udp->Host, $udp->Port);
			}
			
			app_func_free($s);
		}
    }
	
	return 0;
} 

/*! \brief smtAppWorkerStorage */ 
 function smtAppWorkerStorage($smt = NULL)
{ 	
	if ($pid = getmypid()) {	
		smt_log_info(sprintf("Start Worker Thread.WorkerStorage(%d)", $pid), 
		__FILE__, __LINE__ );
	}
	
   /*! 
    * \remark 
    * Definisikan Semua data Untuk Keperluan socket UDP 
	* sock, host, port , time , interval
	*/
	
	app_func_struct($udp);
	app_func_copy($udp->Sock, 0);
	app_func_copy($udp->Loop, 0);
	app_func_copy($udp->Intv, 3);
	app_func_copy($udp->Time, time());
	app_func_copy($udp->Host, $smt->UdpHost);
	app_func_copy($udp->Port, $smt->UdpPort);
	
	if (smtSockUdpClientCreate($udp->Sock) < 0)
	{
		smt_log_app("Create 'smtSockUdpClientCreate()' Fail.");
		if (smtSockUdpClose($udp->Sock))
			app_func_copy($udp->Sock,0);
	}
	
   /*! 
    * \remark 
	* Init for Looping Timer 
	* Loop process Live forever 
	*
	*/
	
	app_func_struct($out);
	app_func_alloc($out->waits);
	
	while ($smt)
	{
		/*! if Not found Kill Process */
		usleep(2 * 1000000); 
		if (!app_fork_pid($smt->AppProgPid))
		{
			app_fork_kill( $pid, SIGKILL );
		}
		
		/*! \remark  tested get Uptime process */
		app_func_free($buf);
		if ($smt->AppProc->storage($buf))
		{
		   /*!
			* \remark sent message to SMT Application 
			*
			* Thread On Periode will sent data to SMT Master Application for 
			* Generate file On Local Storage , for service request by client side 
			* 
			* Sent Message Via Udp Socket 
			*
			* \link https://www.binarytides.com/udp-socket-programming-in-php/
			*
			*/
		
			if (!smtAppStorageSender($udp, $buf, $udp->buf))
			{
				app_func_free($udp->buf);
			} 
		}
		
		if (!$udp->Loop)
		{
			app_func_next($udp->Loop);
		}
	}
	
	/*! \retval clear All Process */
	smtSockUdpClose($udp->Sock);
	return 0;
}
